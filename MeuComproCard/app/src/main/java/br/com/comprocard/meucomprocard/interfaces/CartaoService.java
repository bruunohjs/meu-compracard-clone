package br.com.comprocard.meucomprocard.interfaces;

import java.util.List;

import br.com.comprocard.meucomprocard.Model.api.AuthRequest;
import br.com.comprocard.meucomprocard.Model.api.AuthResponse;
import br.com.comprocard.meucomprocard.Model.api.CartaoApi;
import br.com.comprocard.meucomprocard.Model.api.CartaoLoginRequest;
import br.com.comprocard.meucomprocard.Model.api.CartaoLoginResponse;
import br.com.comprocard.meucomprocard.Model.api.CartaoSaldo;
import br.com.comprocard.meucomprocard.Model.api.CartaoSaldosRequest;
import br.com.comprocard.meucomprocard.Model.api.HistoricoRequest;
import br.com.comprocard.meucomprocard.Model.api.HistoricoResponse;
import br.com.comprocard.meucomprocard.Model.api.ListaHistorico;
import br.com.comprocard.meucomprocard.Model.api.PublicidadeResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface CartaoService {

    @POST("account/autentica")
    Call<AuthResponse> autenticar(@Header("Authorization") String token, @Body AuthRequest body);

    @POST("cartao/login")
    Call<CartaoLoginResponse> logarCartao(@Header("Authorization") String token,
                                          @Header("deviceId") String deviceId,
                                          @Body CartaoLoginRequest body);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("cartao/saldos")
    Call<List<CartaoApi>> getCartoes(@Header("Authorization") String token,
                                     @Header("deviceId") String deviceId,
                                     @Body CartaoSaldosRequest body);

    @Headers({
            "Content-Type: application/json"
    })
    @GET
    Call<PublicidadeResponse> getBanners(@Url String url);

}
