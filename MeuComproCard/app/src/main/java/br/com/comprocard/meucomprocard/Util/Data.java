package br.com.comprocard.meucomprocard.Util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import android.util.Log;

public class Data {
	
	private SimpleDateFormat sdf;

	public String brToEn(String pDataStr){
		
		String splitData[] = new String[8];
		splitData = pDataStr.split("/");
		return splitData[2] + "-" +  splitData[1] + "-" + splitData[0];
	}
	
	public String enToBr(String pDataStr){
		String splitData[] = new String[8];
		splitData = pDataStr.split("-");
		return splitData[2] + "-" +  splitData[1] + "-" + splitData[0];
	}

	public Date parseDate(String date, String format)
	{
		Date retorno = null;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(format);
			retorno = formatter.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return retorno;
	}

	public String getDateStrWithDefault(Date date, String format)
	{
		String retorno = null;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat(format);
			if ( date != null ) {
				retorno = new SimpleDateFormat("dd-MM-yyyy").format(date);
			} else {
				retorno = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retorno;
	}

	public Date string2Date(String pDataStr) {
		Date data = null;
		if (!pDataStr.isEmpty()) {
			this.sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			try {
				data = (Date) sdf.parse(pDataStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return data;
	}

	public Date string3Date(String pDataStr) {
		Date data = null;
		if (!pDataStr.isEmpty()) {
			this.sdf = new SimpleDateFormat("dd/MM/yyyy");
			try {
				data = (Date) sdf.parse(pDataStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return data;
	}

	public String dateToBr(Date pData){
		this.sdf = new SimpleDateFormat("dd/MM/yyyy");
		String dataStr = new String();
		dataStr = this.sdf.format(pData);
		return dataStr;
	}
	
	public String dateToEn(Date pData){
		String dataStr = new String();
		if (pData != null) {
			this.sdf = new SimpleDateFormat("yyyy-MM-dd");
			dataStr = this.sdf.format(pData);
		}
		else
		{
			dataStr = "";
		}
		return dataStr;
	}

	public String dateToStringChangeFormat(Date pData) {
		String newDateString = "";
		if (pData != null) {
			final String OLD_FORMAT = "yyyy-MM-dd";
			final String NEW_FORMAT = "dd/MM/yyyy";

			String oldDateString = dateToEn(pData);
			try {
				SimpleDateFormat sdfl = new SimpleDateFormat(OLD_FORMAT);
				Date d = sdfl.parse(oldDateString);
				sdf.applyPattern(NEW_FORMAT);
				newDateString = sdf.format(d);
			} catch (ParseException e) {
				e.printStackTrace();
				newDateString = "Erro ao converter data.";
			}
		} else {
			newDateString = "";
		}
		return newDateString;
	}

	public String converteData(Date dtData){
		SimpleDateFormat formatBra;
		formatBra = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date newData = formatBra.parse(dtData.toString());
			return (formatBra.format(newData));
		} catch (ParseException Ex) {
			return "Erro na conversão da data";
		}
	}


}
