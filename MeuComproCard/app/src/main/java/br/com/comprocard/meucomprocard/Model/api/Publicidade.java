package br.com.comprocard.meucomprocard.Model.api;

import java.io.Serializable;

public class Publicidade implements Serializable{
    private float publicidadeId;
    private String publicidadeImagemUrlDesktop;
    private String publicidadeImagemUrlMobile;


    // Getter Methods

    public float getPublicidadeId() {
        return publicidadeId;
    }

    public String getPublicidadeImagemUrlDesktop() {
        return publicidadeImagemUrlDesktop;
    }

    public String getPublicidadeImagemUrlMobile() {
        return publicidadeImagemUrlMobile;
    }

    // Setter Methods

    public void setPublicidadeId(float publicidadeId) {
        this.publicidadeId = publicidadeId;
    }

    public void setPublicidadeImagemUrlDesktop(String publicidadeImagemUrlDesktop) {
        this.publicidadeImagemUrlDesktop = publicidadeImagemUrlDesktop;
    }

    public void setPublicidadeImagemUrlMobile(String publicidadeImagemUrlMobile) {
        this.publicidadeImagemUrlMobile = publicidadeImagemUrlMobile;
    }
}
