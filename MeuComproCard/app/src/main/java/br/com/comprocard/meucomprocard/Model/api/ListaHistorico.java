package br.com.comprocard.meucomprocard.Model.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ListaHistorico implements Serializable {
    List<HistoricoResponse> historico = new ArrayList<>();

    public List<HistoricoResponse> getHistoricos() {
        return historico;
    }

    public void setHistoricos(List<HistoricoResponse> historico) {
        this.historico = historico;
    }
}
