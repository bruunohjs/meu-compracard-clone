package br.com.comprocard.meucomprocard.Model.api;

import java.io.Serializable;

public class AuthResponse implements Serializable {
    private String token;
    private String dataExpiracao;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDataExpiracao() {
        return dataExpiracao;
    }

    public void setDataExpiracao(String dataExpiracao) {
        this.dataExpiracao = dataExpiracao;
    }
}
