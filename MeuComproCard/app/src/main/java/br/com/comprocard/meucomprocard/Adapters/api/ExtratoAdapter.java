package br.com.comprocard.meucomprocard.Adapters.api;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.comprocard.meucomprocard.Model.Historico;
import br.com.comprocard.meucomprocard.Model.api.HistoricoResponse;
import br.com.comprocard.meucomprocard.R;
import br.com.comprocard.meucomprocard.Util.uiUtils;

public class ExtratoAdapter extends RecyclerView.Adapter<ExtratoAdapter.ExtratoViewHolder>  {

    private Context context;
    private List<HistoricoResponse> items;

    public ExtratoAdapter(Context context, List<HistoricoResponse> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public ExtratoAdapter.ExtratoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.content_extrato_row, parent, false);
        ExtratoAdapter.ExtratoViewHolder viewHolder = new ExtratoAdapter.ExtratoViewHolder(context, view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ExtratoAdapter.ExtratoViewHolder viewHolder, int position) {
        HistoricoResponse _extrato = items.get(position);

        viewHolder._lblDescricao.setText(uiUtils.replaceNull(String.valueOf(_extrato.getEstabelecimento())));
        viewHolder._lblData.setText( uiUtils.replaceNull(String.valueOf(_extrato.getDataFormatada().toLowerCase())));
        //viewHolder._lblParcelas.setText("Parcelas: " + uiUtils.replaceNull(String.valueOf(_extrato.getParcela ())));
        DecimalFormat df = new DecimalFormat("#,###,##0.00");
        viewHolder._lblMarcador.setBackgroundColor(Color.parseColor("#81c784"));
        viewHolder._lblValor.setText("R$ " + df.format(_extrato.getValor()));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ExtratoViewHolder extends RecyclerView.ViewHolder {

        private Context context;
        public TextView _lblDescricao;
        public TextView _lblData;
        public TextView _lblMarcador;
        public TextView _lblValor;
        public RelativeLayout _rl;
        //public ImageView _ivMarcador;

        public ExtratoViewHolder(Context context, View itemView) {
            super(itemView);
            this.context = context;
            _lblDescricao = (TextView) itemView.findViewById(R.id.lblDescricao);
            _lblData = (TextView) itemView.findViewById(R.id.lblData);
            _lblMarcador = (TextView) itemView.findViewById(R.id.lblMarcador);
            _lblValor = (TextView) itemView.findViewById(R.id.lblValor);
            ///_ivMarcador = (ImageView) itemView.findViewById(R.id.ivMarcador);
            _rl = (RelativeLayout) itemView.findViewById(R.id.rltop);
        }
    }
}