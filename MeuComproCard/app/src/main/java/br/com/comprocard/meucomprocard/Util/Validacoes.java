package br.com.comprocard.meucomprocard.Util;

import android.text.TextUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Validacoes {

    public static boolean isValidDate(String inDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(inDate.trim());
        } catch (ParseException pe) {
            return false;
        }
        return true;
    }

    public static String getNumericOnly(String s)
    {
        String numberGen = null;
        if (s == null)
            return numberGen;
        if (s.length() == 0)
            return numberGen;

        numberGen = "";
        for (int i = 0; i < s.length(); i++)
        {
            if (Character.isDigit(s.charAt(i))){
                numberGen += s.charAt(i);
            }
        }
        return numberGen;
    }

    public static boolean validaCPF(String cpf) {
        boolean ret = false;

        if ( cpf.isEmpty() )
            return false;

        String base = "000000000";
        String digitos = "00";
        cpf = getNumericOnly(cpf);

        if ( cpf.length() != 11 )
            return false;

        if (cpf.length() <= 11) {
            if (cpf.length() < 11) {
                cpf = base.substring(0, 11 - cpf.length()) + cpf;
                base = cpf.substring(0, 9);
            }
            base = cpf.substring(0, 9);
            digitos = cpf.substring(9, 11);
            int soma = 0, mult = 11;
            int[] var = new int[11];

            for (int i = 0; i < 9; i++) {
                var[i] = Integer.parseInt("" + cpf.charAt(i));
                if (i < 9)
                    soma += (var[i] * --mult);
            }

            int resto = soma % 11;
            if (resto < 2) {
                var[9] = 0;
            } else {
                var[9] = 11 - resto;
            }

            soma = 0;
            mult = 11;

            for (int i = 0; i < 10; i++)
                soma += var[i] * mult--;

            resto = soma % 11;
            if (resto < 2) {
                var[10] = 0;
            } else {
                var[10] = 11 - resto;
            }
            if ((digitos.substring(0, 1).equalsIgnoreCase(new Integer(var[9])
                    .toString()))
                    && (digitos.substring(1, 2).equalsIgnoreCase(new Integer(
                    var[10]).toString()))) {
                ret = true;
            }
        }

        return ret;
    }

    public static boolean validaCNPJ( String str_cnpj )
    {
        if ( str_cnpj.isEmpty() )
            return false;

        str_cnpj = getNumericOnly(str_cnpj);
        if ( str_cnpj.length() != 14 )
            return false;

        int soma = 0, aux, dig;
        String cnpj_calc = str_cnpj.substring(0,12);


        char[] chr_cnpj = str_cnpj.toCharArray();

       /* Primeira parte */
        for( int i = 0; i < 4; i++ )
            if ( chr_cnpj[i]-48 >=0 && chr_cnpj[i]-48 <=9 )
                soma += (chr_cnpj[i] - 48 ) * (6 - (i + 1)) ;
        for( int i = 0; i < 8; i++ )
            if ( chr_cnpj[i+4]-48 >=0 && chr_cnpj[i+4]-48 <=9 )
                soma += (chr_cnpj[i+4] - 48 ) * (10 - (i + 1)) ;
        dig = 11 - (soma % 11);

        cnpj_calc += ( dig == 10 || dig == 11 ) ?
                "0" : Integer.toString(dig);

       /* Segunda parte */
        soma = 0;
        for ( int i = 0; i < 5; i++ )
            if ( chr_cnpj[i]-48 >=0 && chr_cnpj[i]-48 <=9 )
                soma += (chr_cnpj[i] - 48 ) * (7 - (i + 1)) ;
        for ( int i = 0; i < 8; i++ )
            if ( chr_cnpj[i+5]-48 >=0 && chr_cnpj[i+5]-48 <=9 )
                soma += (chr_cnpj[i+5] - 48 ) * (10 - (i + 1)) ;
        dig = 11 - (soma % 11);
        cnpj_calc += ( dig == 10 || dig == 11 ) ?
                "0" : Integer.toString(dig);

        return str_cnpj.equals(cnpj_calc);
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
}
