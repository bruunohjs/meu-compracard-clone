package br.com.comprocard.meucomprocard.DAO;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import br.com.comprocard.meucomprocard.Model.Indicacao;
import br.com.comprocard.meucomprocard.dbHelper.ConexaoSQLite;

public class indicacaoDAO {
	private ConexaoSQLite sqliteDataBase;
	public indicacaoDAO(ConexaoSQLite pSqliteDataBase) {
		this.sqliteDataBase = pSqliteDataBase;
	}

	public long salvarIndicacaoDAO(Indicacao indicacao) {
		long Id = 0;
		SQLiteDatabase db = this.sqliteDataBase.getWritableDatabase();
		try {
			ContentValues values = new ContentValues();
			values.put("cpf", indicacao.getCPF());
			values.put("Nome", indicacao.getNome());
			values.put("NomeContato", indicacao.getNomeContato());
			values.put("Telefone", indicacao.getTelefone());
			values.put("EmailContato", indicacao.getEmailContato());
			values.put("Latitude", indicacao.getLatitude());
			values.put("Longitude", indicacao.getLongitude());
			Id = db.insert("Indicacao", null, values);
		} catch (Exception e) {
			Log.d("DB_ERROR", "NAO FOI POSSIVEL INSERIR Indicacao");
		} finally {
			db.close();
		}
		return Id;
	}
}