package br.com.comprocard.meucomprocard.Adapters.api;


import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.comprocard.meucomprocard.DataSync.postCartaoLoginWait;
import br.com.comprocard.meucomprocard.ExtratoActivity;
import br.com.comprocard.meucomprocard.Fragments.HomeFragment;
import br.com.comprocard.meucomprocard.HomeActivity;
import br.com.comprocard.meucomprocard.Model.Cartao;
import br.com.comprocard.meucomprocard.R;
import br.com.comprocard.meucomprocard.Util.Global;
import br.com.comprocard.meucomprocard.Util.MCrypt;
import br.com.comprocard.meucomprocard.Util.uiUtils;
import br.com.comprocard.meucomprocard.cartaoDetalhePopup;
import br.com.comprocard.meucomprocard.mainComproCardActivity;

public class CartaoAdapter extends RecyclerView.Adapter<CartaoAdapter.CartaoViewHolder> {

    List<Cartao> mListData;
    private Context mContext;
    //private ArrayList<Cartao> items;

    public CartaoAdapter(Context context, List<Cartao> items) {
        this.mContext = context;
        //this.items = items;
        this.mListData = items;
    }

    @Override
    public CartaoAdapter.CartaoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.cartoes_list_row, parent, false);
        return new CartaoAdapter.CartaoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CartaoAdapter.CartaoViewHolder viewHolder, int position) {
        final Cartao _cartao = mListData.get(position);
        viewHolder._CartaoNumero.setText(uiUtils.replaceNull(String.valueOf(_cartao.getCartaoNumFmt())));
        viewHolder._card_titular.setText(uiUtils.replaceNull(String.valueOf(_cartao.getNome())));
        viewHolder._IdCartao.setText(String.valueOf(_cartao.getCartaoId()));
        viewHolder._Situacao.setText("Situação: " + uiUtils.replaceNull(String.valueOf(_cartao.getSituacao())));
        viewHolder._cartaoType.setText("Cartão: " + uiUtils.replaceNull(String.valueOf(_cartao.getProduto())));
        DecimalFormat df = new DecimalFormat("#,###,##0.00");

        if (Global.getInstance().isConected == true) {
            viewHolder._Saldo.setText("Saldo R$ " + df.format(_cartao.getSaldo()));
            viewHolder._Saldo.setPaintFlags(0);
        } else {
            viewHolder._Saldo.setText("Saldo R$ " + df.format(_cartao.getSaldo()));
            viewHolder._Saldo.setPaintFlags(viewHolder._Saldo.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
        viewHolder._Consulta.setText( uiUtils.replaceNull(String.valueOf(_cartao.getUltimaConsulta())));
        //viewHolder._Limite.setText("Limite: R$ " + df.format(_cartao.getLimite()));
        /*
        viewHolder.btnDetalhes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mContext instanceof HomeActivity) {
                    Intent intent = new Intent(((HomeActivity) mContext), cartaoDetalhePopup.class);
                    intent.setFlags(Intent.FLAG_FROM_BACKGROUND);
                    intent.putExtra("ID_CARTAO", String.valueOf(viewHolder._IdCartao.getText()));
                    ((HomeActivity) mContext).startActivity(intent);
                }
            }
        });*/

        viewHolder._profile_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mContext instanceof HomeActivity) {
                    Intent intent = new Intent(((HomeActivity) mContext), cartaoDetalhePopup.class);
                    intent.setFlags(Intent.FLAG_FROM_BACKGROUND);
                    intent.putExtra("ID_CARTAO", String.valueOf(viewHolder._IdCartao.getText()));
                    ((HomeActivity) mContext).startActivity(intent);
                }
            }
        });
        /*
        viewHolder.btnExtrato.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mContext instanceof HomeActivity) {
                    try {
                        String _cartaoNumero = getCartaoDecript(String.valueOf(_cartao.getNumero()));
                        synchronized (new postCartaoLoginWait(Global.getInstance().apiURLLogin, _cartaoNumero, ((HomeActivity) mContext)).execute().get()) {
                        }
                    } catch (Exception e) {

                    }

                    Intent intent = new Intent(((HomeActivity) mContext), ExtratoActivity.class);
                    intent.setFlags(Intent.FLAG_FROM_BACKGROUND);
                    intent.putExtra("ID_CARTAO", String.valueOf(viewHolder._IdCartao.getText()));
                    ((HomeActivity) mContext).startActivity(intent);
                }
            }
        });*/

        /*String URLbase  = _cartao.getImagemCartao().substring(0, _cartao.getImagemCartao().lastIndexOf("/") + 1 );
        String fileName = _cartao.getImagemCartao().substring(_cartao.getImagemCartao().lastIndexOf("/"), _cartao.getImagemCartao().length());
        String versaoImagem  = mContext.getResources().getString(R.string.versaoImg);
        URLbase += versaoImagem + fileName;*/

        try {
            try {
                //Picasso.with(this.mContext).load("file://" + URLbase ).placeholder(R.drawable.ultimodet).fit().into(_imgCartao);
                viewHolder._profile_pic.setImageResource ( R.drawable.ultimohome);
                /*Picasso
                        .with(this.mContext)
                        .load("https://meucomprocardimg.blob.core.windows.net/cards/cartao_compras_side.png" ) // + _cartao.getImagemCartao())
                        .placeholder(R.drawable.cartao_default)
                        .fit()
                        .into(viewHolder._profile_pic);*/
                //viewHolder._profile_pic.getBackground().setAlpha(50);
                //viewHolder._profile_pic.setAlpha(50);
                //Picasso.with(this.mContext).load("file://" + URLbase ).placeholder(R.drawable.cartao_default).fit().into(viewHolder._profile_pic);
            } catch (Exception e) {

            }
            //decodedBMP.recycle();
            //decodedBMP = null;
            System.gc();
        } catch (Exception e) {

        }
    }

    @Override
    public int getItemCount() {
        return mListData == null ? 0 : mListData.size();
    }

    private String getCartaoDecript(String _numeroCartaoCrip) {
        String result = "";
        try {
            MCrypt mcrypt = new MCrypt();
            result = new String(mcrypt.decrypt(_numeroCartaoCrip));
        } catch (Exception e) {
            Log.d("Erro", e.getMessage());
        }
        return result;
    }

    class CartaoViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        TextView txtPrimeiro;
        TextView txtSegundo;
        //ImageButton btnDetalhes;
        //ImageButton btnExtrato;

        private Context context;
        public TextView _CartaoNumero;
        public TextView _Situacao;
        public TextView _Saldo;
        //public TextView _Limite;
        public TextView _Consulta;
        public TextView _cartaoType;
        public TextView _IdCartao;
        public TextView _card_titular;
        public ImageView _profile_pic;

        public CartaoViewHolder(View itemView) {
            super(itemView);
            //title = (TextView) itemView.findViewById(R.id.listitem_name);
            //txtPrimeiro = (TextView) itemView.findViewById(R.id.summoner);
            txtSegundo = (TextView) itemView.findViewById(R.id.label);
            _card_titular = (TextView) itemView.findViewById(R.id.card_titular);
            //btnDetalhes = (ImageButton) itemView.findViewById(R.id.buttonDetalhe);
            //btnExtrato = (ImageButton) itemView.findViewById(R.id.buttonExtrato);

            _CartaoNumero = (TextView) itemView.findViewById(R.id.lblNumeroCartao);
            _Situacao = (TextView) itemView.findViewById(R.id.lblSituacao);
            _Saldo = (TextView) itemView.findViewById(R.id.lblSaldo);
            //_Limite = (TextView) itemView.findViewById(R.id.lblLimite);
            _Consulta = (TextView) itemView.findViewById(R.id.lblDataConsulta);
            _IdCartao = (TextView) itemView.findViewById(R.id.lblCartaoID);
            _cartaoType = (TextView) itemView.findViewById(R.id.lblDataType);
            _profile_pic = (ImageView) itemView.findViewById(R.id.card_photo);
        }
    }
}