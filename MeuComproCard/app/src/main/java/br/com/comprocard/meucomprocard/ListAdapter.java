package br.com.comprocard.meucomprocard;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.*;

/**
 * Created by @vitovalov on 30/9/15.
 */
public class ListAdapter {

}
/*
    extends
} RecyclerView.Adapter<ListAdapter.MyViewHolder> {

    List<String> mListData;
    private Context mContext;

    public ListAdapter(List<String> mListData, Context context) {
        this.mListData = mListData;
        this.mContext=context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cartoes_list_row,
                viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, int i) {
        myViewHolder.title.setText(mListData.get(i));
        myViewHolder.txtPrimeiro.setText(mListData.get(i));
        myViewHolder.txtSegundo.setText(mListData.get(i));

        myViewHolder.btnDetalhes.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                if(mContext instanceof MainActivity) {
                    Intent intent = new Intent(((MainActivity) mContext), detalheCartaoActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    intent.putExtra("ID_CARTAO", "125125");
                    ((MainActivity) mContext).startActivity(intent);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mListData == null ? 0 : mListData.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        TextView txtPrimeiro;
        TextView txtSegundo;
        ImageButton btnDetalhes;

        public MyViewHolder(View itemView) {
            super(itemView);

            //title = (TextView) itemView.findViewById(R.id.listitem_name);
            //txtPrimeiro = (TextView) itemView.findViewById(R.id.summoner);
            txtSegundo = (TextView) itemView.findViewById(R.id.label);
            //btnDetalhes = (ImageButton) itemView.findViewById(R.id.button);
        }
    }

}

*/