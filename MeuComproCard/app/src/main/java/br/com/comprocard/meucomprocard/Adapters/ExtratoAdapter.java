package br.com.comprocard.meucomprocard.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.text.DecimalFormat;
import java.util.ArrayList;
import br.com.comprocard.meucomprocard.Model.Historico;
import br.com.comprocard.meucomprocard.R;
import br.com.comprocard.meucomprocard.Util.Global;
import br.com.comprocard.meucomprocard.Util.uiUtils;

/**
 * Created by Andre on 06/12/2016.
 */

public class ExtratoAdapter extends RecyclerView.Adapter<ExtratoAdapter.ExtratoViewHolder>  {

    private Context context;
    private ArrayList<Historico> items;

    public ExtratoAdapter(Context context, ArrayList<Historico> items) {
        this.context = context;
        this.items = items;
    }

    /*@NonNull
    @Override
    public String getSectionName(int position) {
        //return String.valueOf(position);
        try {
            return items.get(position).getDescricao().substring(0, 1).toUpperCase();
        } catch  ( Exception e ) {
            return " ";
        }
    }*/

    // Create new views (invoked by the layout manager)
    @Override
    public ExtratoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.content_extrato_row, parent, false);
        ExtratoViewHolder viewHolder = new ExtratoViewHolder(context, view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ExtratoViewHolder viewHolder, int position) {
        Historico _extrato = items.get(position);
        if (_extrato.getTipo().equals("C") ) {
            viewHolder._lblDescricao.setText(uiUtils.replaceNull(String.valueOf(_extrato.getDescricao())));
            viewHolder._lblData.setText( uiUtils.replaceNull(String.valueOf(_extrato.getDataHora().toLowerCase())));
            //viewHolder._lblParcelas.setText("Parcelas: " + uiUtils.replaceNull(String.valueOf(_extrato.getParcela ())));
            DecimalFormat df = new DecimalFormat("#,###,##0.00");
            viewHolder._lblMarcador.setBackgroundColor(Color.parseColor("#81c784"));
            viewHolder._lblValor.setText("R$ " + df.format(_extrato.getValor()));
            //viewHolder._lblValor.setTextColor(Color.parseColor("#009966"));
        } else {
            viewHolder._lblDescricao.setText(uiUtils.replaceNull(String.valueOf(_extrato.getDescricao())));

            /*if (_extrato.getValor().signum() < 0 ) {
                //viewHolder._lblDescricao.setPaintFlags(viewHolder._lblDescricao.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                //viewHolder._lblValor.setPaintFlags(viewHolder._lblDescricao.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                //viewHolder._lblValor.setTextColor(Color.parseColor("#e57373"));
            } else {
                viewHolder._lblDescricao.setPaintFlags(0);
                viewHolder._lblValor.setPaintFlags(0);
                //viewHolder._lblValor.setTextColor(Color.parseColor("#f44336"));
                viewHolder._lblMarcador.setBackgroundColor(Color.parseColor("#e57373"));
            }*/

            viewHolder._lblDescricao.setPaintFlags(0);
            viewHolder._lblValor.setPaintFlags(0);
            //viewHolder._lblValor.setTextColor(Color.parseColor("#f44336"));
            viewHolder._lblMarcador.setBackgroundColor(Color.parseColor("#e57373"));



            viewHolder._lblData.setText( uiUtils.replaceNull(String.valueOf(_extrato.getDataHora().toLowerCase())));
            //viewHolder._lblParcelas.setText("Parcelas: " + uiUtils.replaceNull(String.valueOf(_extrato.getParcela ())));
            DecimalFormat df = new DecimalFormat("#,###,##0.00");
            viewHolder._lblValor.setText("R$ " + df.format(_extrato.getValor()));
        }

        /*if ( position == _findFirstCompletelyVisibleItemPosition )
            viewHolder._rl.setBackgroundResource(R.drawable.time_line_t);

        if(position == _findLastCompletelyVisibleItemPosition)
            viewHolder._rl.setBackgroundResource(R.drawable.time_line_b);*/
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ExtratoViewHolder extends RecyclerView.ViewHolder {

        private Context context;
        public TextView _lblDescricao;
        public TextView _lblData;
        public TextView _lblMarcador;
        public TextView _lblValor;
        public RelativeLayout _rl;
         //public ImageView _ivMarcador;

        public ExtratoViewHolder(Context context, View itemView) {
            super(itemView);
            this.context = context;
            _lblDescricao = (TextView) itemView.findViewById(R.id.lblDescricao);
            _lblData = (TextView) itemView.findViewById(R.id.lblData);
            _lblMarcador = (TextView) itemView.findViewById(R.id.lblMarcador);
            _lblValor = (TextView) itemView.findViewById(R.id.lblValor);
            ///_ivMarcador = (ImageView) itemView.findViewById(R.id.ivMarcador);
            _rl = (RelativeLayout) itemView.findViewById(R.id.rltop);
         }
    }
}