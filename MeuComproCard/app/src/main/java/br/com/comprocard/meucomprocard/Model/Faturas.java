package br.com.comprocard.meucomprocard.Model;

import java.math.BigDecimal;

/**
 * Created by andre.cardoso on 23/11/2016.
 */

public class Faturas {
    private int ID;
    private int cartaoID;
    private String MesAnoReferencia;
    private BigDecimal Valor;
    private String Situacao;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getCartaoID() {
        return cartaoID;
    }

    public void setCartaoID(int cartaoID) {
        this.cartaoID = cartaoID;
    }

    public String getMesAnoReferencia() {
        return MesAnoReferencia;
    }

    public void setMesAnoReferencia(String mesAnoReferencia) {
        MesAnoReferencia = mesAnoReferencia;
    }

    public BigDecimal getValor() {
        return Valor;
    }

    public void setValor(BigDecimal valor) {
        Valor = valor;
    }

    public String getSituacao() {
        return Situacao;
    }

    public void setSituacao(String situacao) {
        Situacao = situacao;
    }

    @Override
    public String toString() {
        return "Faturas[ID=" + ID +", MesAnoReferencia=" + MesAnoReferencia + ", Valor=" + Valor +", Situacao=" + Situacao +"]";
    }
}
