package br.com.comprocard.meucomprocard;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONObject;
import java.net.HttpURLConnection;
import br.com.comprocard.meucomprocard.Controller.PerfilCtrl;
import br.com.comprocard.meucomprocard.DataSync.ResultsListener;
import br.com.comprocard.meucomprocard.DataSync.postPerfilRegister;
import br.com.comprocard.meucomprocard.DataSync.resServer;
import br.com.comprocard.meucomprocard.Model.Perfil;
import br.com.comprocard.meucomprocard.Util.CpfCnpjMask;
import br.com.comprocard.meucomprocard.Util.Global;
import br.com.comprocard.meucomprocard.Util.MCrypt;
import br.com.comprocard.meucomprocard.Util.Mascara;
import br.com.comprocard.meucomprocard.Util.Validacoes;
import br.com.comprocard.meucomprocard.dbHelper.ConexaoSQLite;

import static android.widget.Toast.LENGTH_LONG;

public class showPerfil extends AppCompatActivity implements ResultsListener {

    private ConexaoSQLite _banco;
    private TextWatcher cpfCnpjMask;
    private EditText _edtNome;
    private EditText _edtEmail;
    private EditText _edtCPF;
    private EditText _edtDataNasc;
    private EditText _edtTelefone;
    private TextView _txtFrase1;
    private TextView _txtFrase2;
    private TextView _btnEnviar;
    private TextView _txtSenha;
    private TextView _txtSenhaConf;
    private PerfilCtrl _perfilCTRL;
    private Perfil _perfilPreenchido;
    private Perfil _perfil;
    InputMethodManager imm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_perfil);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Perfil Meu ComproCard");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        _banco = ConexaoSQLite.getInstancia(this);
        _edtNome  = (EditText) findViewById(R.id.edtNome);
        _edtEmail = (EditText) findViewById(R.id.edtEmail);
        _edtDataNasc = (EditText) findViewById(R.id.edtDataNascimento);
        _edtCPF = (EditText) findViewById(R.id.edtCPF);
        _edtTelefone = (EditText) findViewById(R.id.edtTelefone);
        _txtSenha = (EditText) findViewById(R.id.edtSenha);
        _txtSenhaConf = (EditText) findViewById(R.id.edtSenhaConfirm);
        _txtFrase1 = (TextView) findViewById(R.id.txtFrase1);
        _txtFrase2 = (TextView) findViewById(R.id.txtFrase2);
        _btnEnviar = (TextView) findViewById(R.id.btnEnviar);

        _txtFrase1 = (TextView) findViewById(R.id.txtFrase1);
        _txtFrase2 = (TextView) findViewById(R.id.txtFrase2);
        isConnected();
        _perfilCTRL = new PerfilCtrl(_banco);

        _perfil = _perfilCTRL.getPerfilCtrl(1);
        _txtFrase1.setText("Perfil - Alterar dados");
        _txtFrase2.setText("Dados básicos do portador do cartão ComproCard.");
        _edtNome.setText(_perfil.getNome().toString());
        _edtEmail.setText(_perfil.getEmail().toString());
        _edtDataNasc.setText(_perfil.getDataNascimento().toString());
        _edtCPF.setText( _perfil.getCPF().toString().substring(0,7) + ".***-**" );
        _edtCPF.setBackgroundColor(Color.parseColor("#dddddd"));
        _edtTelefone.setText(_perfil.getTelefone().toString());
        _txtSenha.setText(_perfil.getSenha().toString());
        _txtSenhaConf.setText(_perfil.getSenha().toString());
        //_edtNome.setEnabled(false);
        //_edtEmail.setEnabled(false);
        //_edtDataNasc.setEnabled(false);
        _edtCPF.setEnabled(false);
        //_edtTelefone.setEnabled(false);
        _txtSenha.setEnabled(false);
        _txtSenha.setBackgroundColor(Color.parseColor("#dddddd"));
        _txtSenhaConf.setEnabled(false);
        _txtSenhaConf.setBackgroundColor(Color.parseColor("#dddddd"));
        _btnEnviar.setText("Salvar");
        Global.getInstance().isEditPerfil = true;

        _edtTelefone.addTextChangedListener(Mascara.insert("(##)#####-####", _edtTelefone));
        _edtDataNasc.addTextChangedListener(Mascara.insert("##/##/####", _edtDataNasc));

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        _edtDataNasc.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus &&  _edtDataNasc.length() > 0) {
                    if (!Validacoes.isValidDate(_edtDataNasc.getText().toString())) {
                        Toast.makeText(getApplicationContext(), "Data inválida!", Toast.LENGTH_LONG).show();
                        _edtDataNasc.setTextColor(Color.RED);
                    } else {
                        _edtDataNasc.setTextColor(Color.BLACK);
                    }
                }
            }
        });

        _edtEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus && _edtEmail.length() > 0) {
                    if (!Validacoes.isValidEmail( _edtEmail.getText().toString() )) {
                        Toast.makeText(getApplicationContext(), "E-mail inválido!", Toast.LENGTH_LONG).show();
                        _edtEmail.setTextColor(Color.RED);
                    } else {
                        _edtEmail.setTextColor(Color.BLACK);
                    }
                }
            }
        });
    }

    public void putPerfil(View paramView) {
        if (isConnected() == true) {
            if (validaFormulario()) {
                _perfilPreenchido = getDadosPerfilFromFormulario();
                _perfilPreenchido.setIMEI(getDeviceUniqueID(this));
                //_perfilPreenchido.setSenha("9999"); //<-- Não é usado por enquanto
                postPerfilRegister _taskPostPerfil = new postPerfilRegister(Global.getInstance().apiURLRegister, _perfilPreenchido, showPerfil.this);
                _taskPostPerfil.setOnResultsListener(this);
                _taskPostPerfil.execute();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home ) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void onResultsSucceeded(resServer result) {
        int httpStatus;
        JSONObject _json;
        String _mensagem = "";

        try {
            httpStatus = result.getHttpStatus();
            _json = result.getJsonBody();

            if (httpStatus == HttpURLConnection.HTTP_OK) {
                _perfilPreenchido.setToken(_json.getString("token"));
                Global.getInstance().apiTokenPerfil = _json.getString("token");
                Global.getInstance().apiDeviceID    = _perfilPreenchido.getIMEI();
                _perfilPreenchido.setAtivo(1);

                if (Global.getInstance().isEditPerfil == true) {
                    _perfilPreenchido.setCPF(String.valueOf(_perfil.getCPF()));
                    _perfilPreenchido.setSenha(String.valueOf(_perfil.getSenha()));
                }

                salvarPerfil(_perfilPreenchido);
                finish();
            } else {
                _mensagem = _json.getString("mensagem");
                if (_mensagem != "") {
                    Toast.makeText(showPerfil.this, _mensagem, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(showPerfil.this, "Tente novamente mais tarde", Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            Toast.makeText(showPerfil.this, "Falha na conexão de internet", LENGTH_LONG).show();
            return;
        }
    }

    public void salvarPerfil(Perfil _perfil) {
        _perfilCTRL.salvarPerfilCtrl(_perfil, true);
    }

    private boolean validaFormulario() {

        if (this._edtNome.getText().toString().trim().length() == 0) {
            if (imm != null) {
                imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
            }
            Toast.makeText(getApplicationContext(), "Campo Nome deve ser preenchido!", Toast.LENGTH_LONG).show();
            this._edtNome.requestFocus();
            return false;
        }

        if (this._edtTelefone.getText().toString().trim().length() == 0) {
            if (imm != null) {
                imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
            }
            Toast.makeText(getApplicationContext(), "Campo Telefone deve ser preenchido!", Toast.LENGTH_LONG).show();
            this._edtTelefone.requestFocus();
            return false;
        }

        if (this._edtEmail.getText().toString().trim().length() == 0) {
            if (imm != null) {
                imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
            }
            Toast.makeText(getApplicationContext(), "Campo E-Mail deve ser preenchido!", Toast.LENGTH_LONG).show();
            this._edtEmail.requestFocus();
            return false;
        }

        if (this._edtEmail.getText().toString().length() > 0) {
            if (!Validacoes.isValidEmail(this._edtEmail.getText().toString())) {
                Toast.makeText(getApplicationContext(), "E-Mail inválido!", Toast.LENGTH_LONG).show();
                this._edtEmail.requestFocus();
                if (imm != null) {
                    imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }
                return false;
            }
        }

        if (this._edtDataNasc.getText().toString().trim().length() == 0) {
            if (imm != null) {
                imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
            }
            Toast.makeText(getApplicationContext(), "Campo Data de Nascimento deve ser preenchido!", Toast.LENGTH_LONG).show();
            this._edtDataNasc.requestFocus();
            return false;
        }

        if (this._edtDataNasc.getText().toString().length() > 0) {
            if (!Validacoes.isValidDate(this._edtDataNasc.getText().toString())) {
                Toast.makeText(getApplicationContext(), "Data de Nascimento inválida!", Toast.LENGTH_LONG).show();
                this._edtDataNasc.requestFocus();
                if (imm != null) {
                    imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }
                return false;
            }
        }

        return true;
    }

    private Perfil getDadosPerfilFromFormulario() {
        Perfil _perfil = new Perfil();
        _perfil.setID( _banco.ID_PERFIL );
        _perfil.setNome( this._edtNome.getText().toString() );
        _perfil.setTelefone( this._edtTelefone.getText().toString() );
        _perfil.setEmail( this._edtEmail.getText().toString() );
        //_perfil.setCPF( this._edtCPF.getText().toString() );
        _perfil.setDataNascimento( this._edtDataNasc.getText().toString() );
        //_perfil.setSenha( getSenhaCript(this._txtSenha.getText().toString()) );
        return  _perfil;
    }

    public String getDeviceUniqueID(Activity activity){
        String device_unique_id;
        try {
            device_unique_id = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
        } catch (Exception e) {
            Log.d("Erro", e.getMessage());
            return "";
        }
        return device_unique_id;
    }

    public boolean isConnected()
    {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(meuCartaoActivity.CONNECTIVITY_SERVICE);
        NetworkInfo net = cm.getActiveNetworkInfo();
        if (net!=null && net.isAvailable() && net.isConnected()) {
            Global.getInstance().isConected = true;
            return true;
        } else {
            Global.getInstance().isConected = false;
            Toast.makeText(getApplicationContext(), "Por favor conecte-se a internet", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    private String getSenhaCript(String _SenhaPerfil) {
        String encrypted = _SenhaPerfil;
        try {
            MCrypt mcrypt = new MCrypt();
            encrypted = mcrypt.bytesToHex(mcrypt.encrypt(_SenhaPerfil));
        } catch (Exception e) {
            Log.d("Erro", e.getMessage());
            encrypted = "";
        }
        return encrypted;
    }
}

/*

import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import br.com.comprocard.meucomprocard.Controller.PerfilCtrl;
import br.com.comprocard.meucomprocard.Model.Perfil;
import br.com.comprocard.meucomprocard.Util.CpfCnpjMask;
import br.com.comprocard.meucomprocard.Util.Global;
import br.com.comprocard.meucomprocard.Util.Mascara;
import br.com.comprocard.meucomprocard.Util.Validacoes;
import br.com.comprocard.meucomprocard.dbHelper.ConexaoSQLite;

public class showPerfil extends AppCompatActivity {

    private ConexaoSQLite _banco;
    private EditText _edtNome;
    private EditText _edtEmail;
    private EditText _edtCPF;
    private EditText _edtDataNasc;
    private EditText _edtTelefone;
    private TextView _txtFrase1;
    private TextView _txtFrase2;
    private TextView _txtSenha;
    private TextView _txtSenhaConf;
    private PerfilCtrl _perfilCTRL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_perfil);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Perfil Meu ComproCard");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        _banco = ConexaoSQLite.getInstancia(this);
        _edtNome  = (EditText) findViewById(R.id.edtNome);
        _edtEmail = (EditText) findViewById(R.id.edtEmail);
        _edtDataNasc = (EditText) findViewById(R.id.edtDataNascimento);
        _edtCPF = (EditText) findViewById(R.id.edtCPF);
        _edtTelefone = (EditText) findViewById(R.id.edtTelefone);
        _txtSenha = (EditText) findViewById(R.id.edtSenha);
        _txtSenhaConf = (EditText) findViewById(R.id.edtSenhaConfirm);
        _txtFrase1 = (TextView) findViewById(R.id.txtFrase1);
        _txtFrase2 = (TextView) findViewById(R.id.txtFrase2);

        _perfilCTRL = new PerfilCtrl(_banco);
        Perfil _perfil = _perfilCTRL.getPerfilCtrl(1);
        _txtFrase1.setText("Perfil - Meu ComproCard");
        _txtFrase2.setText("Dados básicos do portador do cartão ComproCard.");
        _edtNome.setText("Nome: "+_perfil.getNome().toString());
        _edtEmail.setText("E-Mail: "+_perfil.getEmail().toString());
        _edtDataNasc.setText("Nascimento: "+_perfil.getDataNascimento().toString());
        _edtCPF.setText("CPF/CNPJ: *******************" );
        _edtTelefone.setText("Telefone:"+_perfil.getTelefone().toString());
        _txtSenha.setText(_perfil.getSenha().toString());
        _txtSenhaConf.setText(_perfil.getSenha().toString());
        _edtNome.setEnabled(false);
        _edtEmail.setEnabled(false);
        _edtDataNasc.setEnabled(false);
        _edtCPF.setEnabled(false);
        _edtTelefone.setEnabled(false);
        _txtSenha.setEnabled(false);
        _txtSenhaConf.setEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home ) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
*/