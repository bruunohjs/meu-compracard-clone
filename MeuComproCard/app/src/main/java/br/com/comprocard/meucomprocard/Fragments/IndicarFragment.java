package br.com.comprocard.meucomprocard.Fragments;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import br.com.comprocard.meucomprocard.Controller.PerfilCtrl;
import br.com.comprocard.meucomprocard.DAO.indicacaoDAO;
import br.com.comprocard.meucomprocard.DataSync.ResultsListener;
import br.com.comprocard.meucomprocard.DataSync.postAccountIndicacao;
import br.com.comprocard.meucomprocard.DataSync.resServer;
import br.com.comprocard.meucomprocard.Model.Indicacao;
import br.com.comprocard.meucomprocard.Model.Perfil;
import br.com.comprocard.meucomprocard.R;
import br.com.comprocard.meucomprocard.Util.GPSTracker;
import br.com.comprocard.meucomprocard.Util.Global;
import br.com.comprocard.meucomprocard.Util.Mascara;
import br.com.comprocard.meucomprocard.dbHelper.ConexaoSQLite;
import br.com.comprocard.meucomprocard.indicacaoActivity;

public class IndicarFragment extends Fragment implements ResultsListener {

    private ConexaoSQLite _banco;
    private TextWatcher _Mask;
    private EditText _edtNome;
    private EditText _edtEmail;
    private EditText _edtNomeContato;
    private EditText _edtTelefone;
    private TextView _btnEnviar;
    double latitude;
    double longitude;
    private final int MY_PERMISSIONS_REQUEST = 1;
    InputMethodManager imm;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.frag_indicar, container, false);


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (null != getView()) {
            init(getView());
        }
    }

    @Override
    public void onPause() {
        super.onPause();

//        if (!isGPSAtivado()) return;
    }

    private void init(View view) {

//        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
//        toolbar.setTitle("Indicar Estabelecimento");

        _banco = ConexaoSQLite.getInstancia(getActivity());
        _edtNome = (EditText) view.findViewById(R.id.edtNomeInd);
        _edtNomeContato = (EditText) view.findViewById(R.id.edtNomeContatoInd);
        _edtEmail = (EditText)  view.findViewById(R.id.edtEmailInd);
        _edtTelefone = (EditText)  view.findViewById(R.id.edtTelefoneInd);
        _edtTelefone.addTextChangedListener(Mascara.insert("(##)#####-####", _edtTelefone));
        _btnEnviar = (TextView)  view.findViewById(R.id.btnEnviar);
        _btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                putIndicacao();
            }
        });
        imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

    }

    private boolean isGPSAtivado() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET}, 10);
            }
            return false;
        } else {
            final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View titleView = inflater.inflate(R.layout.custom_dialog, null);
                ((TextView) titleView.findViewById(R.id.partName)).setText(" Atenção");
                builder.setCustomTitle(titleView);
                builder.setMessage("O GPS não está ativo, é necessário ativar para continuar, ativar agora?");
                builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        int RESULT_GPS = 0;
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivityForResult(intent, RESULT_GPS);
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                android.support.v7.app.AlertDialog alert = builder.create();
                alert.show();
            } else {
                return true;
            }
        }
        return false;
    }


    private boolean validaFormulario() {

        if ( null != this._edtNome.getText().toString() ) {
            if (this._edtNome.getText().toString().trim().length() == 0) {
                if (imm != null) {
                    imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }
                Toast.makeText(getActivity(), "Campo [Nome] deve ser preenchido!", Toast.LENGTH_LONG).show();
                this._edtNome.requestFocus();
                return false;
            }
        } else {
            if (imm != null) {
                imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
            }
            Toast.makeText(getActivity(), "Campo [Nome] deve ser preenchido!", Toast.LENGTH_LONG).show();
            this._edtNome.requestFocus();
            return false;
        }

        return true;
    }

    private Indicacao getDadosIndicacaoFromFormulario() {
        Indicacao _indicacao = new Indicacao();
        _indicacao.setNome( _edtNome.getText().toString() );
        _indicacao.setNomeContato( _edtNomeContato.getText().toString() );
        _indicacao.setTelefone( _edtTelefone.getText().toString() );
        _indicacao.setEmailContato( _edtEmail.getText().toString() );
        return _indicacao;
    }

    public void putIndicacao() {
        if (isGPSAtivado()) {
            GPSTracker gps = new GPSTracker(getActivity());
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
               /*try {
                //latitude = location.getLatitude();
                //longitude = location.getLongitude();
                latitude = gps.getLatitude();
                longitude = gps.getLongitude();

                Toast.makeText(getApplicationContext(), "LAT:" + String.valueOf(latitude) + " LOG:" + String.valueOf(longitude), Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Erro ao tentar ler o GPS", Toast.LENGTH_LONG).show();
            }*/
            if (validaFormulario()) {
                try {
                    Indicacao _indicacaoPreenchida = getDadosIndicacaoFromFormulario();
                    PerfilCtrl _perCTRL = new PerfilCtrl(_banco);
                    Perfil _perfil = _perCTRL.getPerfilCtrl(1);
                    _indicacaoPreenchida.setCPF(_perfil.getCPF().toString());
                    //GPSTracker gps = new GPSTracker(indicacaoActivity.this);
                    //latitude = location.getLatitude(); //gps.getLatitude();
                    //longitude = location.getLongitude(); //gps.getLongitude();
                    _indicacaoPreenchida.setLatitude(String.valueOf(latitude));
                    _indicacaoPreenchida.setLongitude(String.valueOf(longitude));
                    indicacaoDAO _dao = new indicacaoDAO(_banco);
                    _dao.salvarIndicacaoDAO(_indicacaoPreenchida);
                    postAccountIndicacao _taskPostIndicacao = new postAccountIndicacao(Global.getInstance().apiURLIndicacao, _indicacaoPreenchida, getActivity());
                    _taskPostIndicacao.setOnResultsListener(this);
                    _taskPostIndicacao.execute();
                } catch (Exception e) {
                    Log.d("Erro", e.getMessage());
                }
            }
        }
    }

    public void onResultsSucceeded(resServer result) {
        if ( result.getHttpStatus() == 200) {
            Toast.makeText(getActivity(), "Agradecemos sua indicação!", Toast.LENGTH_LONG).show();
            //getActivity().finish();
        } else {
            Toast.makeText(getActivity(), "Não foi possível enviar indicação, tente novamente mais tarde!", Toast.LENGTH_LONG).show();
        }
    }

}
