package br.com.comprocard.meucomprocard.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

import br.com.comprocard.meucomprocard.Model.Historico;
import br.com.comprocard.meucomprocard.Model.guiaCompras;
import br.com.comprocard.meucomprocard.R;
import br.com.comprocard.meucomprocard.Util.uiUtils;

/**
 * Created by Andre on 06/12/2016.
 */

public class guiaComprasAdapter extends RecyclerView.Adapter<guiaComprasAdapter.guiaComprasViewHolder>  {

    private Context context;
    private ArrayList<guiaCompras> items;

    public guiaComprasAdapter(Context context, ArrayList<guiaCompras> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public guiaComprasViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.content_guiacompras_row, parent, false);
        guiaComprasViewHolder viewHolder = new guiaComprasViewHolder(context, view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(guiaComprasViewHolder viewHolder, int position) {
        guiaCompras _guia = items.get(position);
        viewHolder._lblDescricao.setText(_guia.getEstabelecimento());
        viewHolder._lblRamo.setText("Ramo: "+_guia.getAtividade() );
        viewHolder._lblTelefone.setText("Tel: "+_guia.getTeletone());
        viewHolder._lblBairro.setText(_guia.getBairro());
        viewHolder._lblCidade.setText(_guia.getCidade());
        viewHolder._lblUF.setText(_guia.getUF());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class guiaComprasViewHolder extends RecyclerView.ViewHolder {

        private Context context;

        public TextView _lblDescricao;
        public TextView _lblRamo;
        public TextView _lblTelefone;
        public TextView _lblBairro;
        public TextView _lblCidade;
        public TextView _lblUF;

        public guiaComprasViewHolder(Context context, View itemView) {
            super(itemView);
            this.context = context;

            _lblDescricao = (TextView) itemView.findViewById(R.id.lblDescricao);
            _lblRamo = (TextView) itemView.findViewById(R.id.lblRamo);
            _lblTelefone = (TextView) itemView.findViewById(R.id.lblTelefone);
            _lblBairro = (TextView) itemView.findViewById(R.id.lblBairro);
            _lblCidade = (TextView) itemView.findViewById(R.id.lblCidade);
            _lblUF = (TextView) itemView.findViewById(R.id.lblUF);
         }
    }
}