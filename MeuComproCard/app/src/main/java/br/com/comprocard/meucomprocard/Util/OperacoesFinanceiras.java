package br.com.comprocard.meucomprocard.Util;

import java.math.BigDecimal;

public class OperacoesFinanceiras {

	public BigDecimal somar(BigDecimal pNum1, BigDecimal pNum2){
		return pNum1.add(pNum2).setScale(2, BigDecimal.ROUND_HALF_EVEN);
	}

	public BigDecimal subtrair(BigDecimal pNum1, BigDecimal pNum2){
		return pNum1.subtract(pNum2).setScale(2, BigDecimal.ROUND_HALF_EVEN);
	}

	public BigDecimal multiplicar(BigDecimal pNum1, BigDecimal pNum2){
		return pNum1.multiply(pNum2).setScale(2, BigDecimal.ROUND_HALF_EVEN);
	}

	public BigDecimal dividir(BigDecimal pNum1, BigDecimal pNum2){
		return pNum1.divide(pNum2, BigDecimal.ROUND_HALF_EVEN).setScale(2);
	}
}
