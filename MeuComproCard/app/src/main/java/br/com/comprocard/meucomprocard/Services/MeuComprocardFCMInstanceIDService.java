package br.com.comprocard.meucomprocard.Services;


import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import br.com.comprocard.meucomprocard.Controller.PerfilCtrl;
import br.com.comprocard.meucomprocard.DataSync.postSendToken;
import br.com.comprocard.meucomprocard.Model.Perfil;
import br.com.comprocard.meucomprocard.Model.TokenComproCard;
import br.com.comprocard.meucomprocard.Util.Global;
import br.com.comprocard.meucomprocard.dbHelper.ConexaoSQLite;

/**
 * Created by esgama on 07/06/2017.
 */

public class MeuComprocardFCMInstanceIDService extends FirebaseInstanceIdService  {
    private static String TAG = "FCMInstance";

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String novoToken = FirebaseInstanceId.getInstance().getToken();
        atualizaToken(novoToken);
    }

    private void atualizaToken(String novoToken) {
        Log.d("FCM", novoToken);
        // TODO: Atualizar token no server
        // Varificar se o perfil está cadastrado antes de tentar cadastrar...
        // Caso ocorra isso, deixar para chamar esse metodo lá FirebaseInstanceId.getInstance().getToken();
        // 1) Chamar a API que atualiza o token no servidor:
        // POST https://sistemas.comprocard.com.br/meucomprocard/api/account/atualizaPushToken
        /*
        { "cpf": [perfil.cpf],
	      "deviceId": [perfil.deviceId],
	      "token": novoToken
	     }
         */
        // 2) Se Status = 200, Atualizar a informação no DB do aplicativo

        ConexaoSQLite _banco;
        _banco = ConexaoSQLite.getInstancia(this);

        Perfil _perfil;
        PerfilCtrl _perCTRL;
        _perCTRL = new PerfilCtrl(_banco);
        _perfil = _perCTRL.getPerfilCtrl(1);

        if (null != _perfil) {
            if (_perfil.getID() == 1) {
                Global.getInstance().apiDeviceID = _perfil.getIMEI();
                //Global.getInstance().apiTokenPerfil = _perfil.getToken();

                TokenComproCard _token = new TokenComproCard();
                _token.setCPF(_perfil.getCPF());
                _token.setIMEI(_perfil.getIMEI());
                _token.setToken(novoToken);

                postSendToken _taskToken = new postSendToken(Global.getInstance().apiURLPushToken, _token, getApplicationContext(), false);
                _taskToken.execute();
            }
        }
    }
}
