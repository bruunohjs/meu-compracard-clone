package br.com.comprocard.meucomprocard.Controller;

import java.util.ArrayList;

import br.com.comprocard.meucomprocard.DAO.HistoricoDAO;
import br.com.comprocard.meucomprocard.dbHelper.ConexaoSQLite;
import br.com.comprocard.meucomprocard.Model.Historico;

public class HistoricoCtrl {
	private HistoricoDAO daoHistorico;

	public HistoricoCtrl() {
	} 

	public HistoricoCtrl(ConexaoSQLite pSQLite) {
		this.daoHistorico = new HistoricoDAO(pSQLite);
	} 

	public long salvarHistoricoCtrl(Historico pHistoric) {
		return this.daoHistorico.salvarHistoricoDAO(pHistoric);
	} 

	public Historico getHistoricoCtrl(int pId) {
		return this.daoHistorico.getHistoricoDAO(pId);
	} 

	public ArrayList<Historico> getListaHistoricoCtrl() {
		return this.daoHistorico.getListaHistoricoDAO();
	}

	public ArrayList<Historico> getListaHistoricoCtrlByIDCartao(String _IDCartao) {
		return this.daoHistorico.getListaHistoricoDAOByCartaoID( _IDCartao );
	}

	public ArrayList<Historico> getListaTestHistoricoCtrl() {
		return this.daoHistorico.getListaHistoricoTestDAO();
	}

	public boolean excluirHistoricoCtrl() {
		return this.daoHistorico.excluirHistoricoDAO();
	}

	public boolean excluirHistoricoCtrlByIDCartao( String _IDCartao ) {
		return this.daoHistorico.excluirHistoricoDAOByCartaoID( _IDCartao );
	}

}