package br.com.comprocard.meucomprocard;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.squareup.picasso.Picasso;
import org.json.JSONObject;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.text.DecimalFormat;
import br.com.comprocard.meucomprocard.Controller.CartaoCtrl;
import br.com.comprocard.meucomprocard.Controller.PerfilCtrl;
import br.com.comprocard.meucomprocard.DataSync.ResultsBloqDesbloq;
import br.com.comprocard.meucomprocard.DataSync.ResultsListener;
import br.com.comprocard.meucomprocard.DataSync.ResultsListenerRefresh;
import br.com.comprocard.meucomprocard.DataSync.postAutentica;
import br.com.comprocard.meucomprocard.DataSync.postCartaoBloqDesbq;
import br.com.comprocard.meucomprocard.DataSync.postCartaoLogin;
import br.com.comprocard.meucomprocard.DataSync.resServer;
import br.com.comprocard.meucomprocard.Model.Cartao;
import br.com.comprocard.meucomprocard.Model.Perfil;
import br.com.comprocard.meucomprocard.Util.Global;
import br.com.comprocard.meucomprocard.Util.MCrypt;
import br.com.comprocard.meucomprocard.dbHelper.ConexaoSQLite;

import static android.widget.Toast.LENGTH_LONG;


public class detalheCartaoActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, ResultsListener, ResultsListenerRefresh, ResultsBloqDesbloq {

    private ConexaoSQLite _banco;
    private int _idCartao;
    private TextView _CartaoNumero;
    private TextView _CartaoNome;
    private TextView _Saldo;
    private CartaoCtrl _carCTRL;
    private Cartao _cartao;
    private Perfil _perfil;
    private PerfilCtrl _perCTRL;
    private DrawerLayout mDrawerLayout;
    private ImageView _imgCartao;
    private TextView _UltimaConsulta;
    private TextView _Situacao;
    private String _acaoCartao;
    private TextView _tvEmpresa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_detalhe_cartao);

        _banco = ConexaoSQLite.getInstancia(this);
        Bundle extras = getIntent().getExtras();
        _idCartao = Integer.parseInt(extras.getString("ID_CARTAO"));
        _carCTRL = new CartaoCtrl(_banco);
        _cartao = _carCTRL.getCartaoCtrlByCartaoId( _idCartao );

        _CartaoNumero = (TextView) findViewById(R.id.tvNumCartao);
        _CartaoNome = (TextView) findViewById(R.id.tvTitular);
        _Saldo = (TextView) findViewById(R.id.tvSaldoValor);
        _imgCartao = (ImageView) findViewById(R.id.imageCard);
        _UltimaConsulta = (TextView) findViewById(R.id.tvConsultaD);
        _Situacao = (TextView) findViewById(R.id.tvSituacaoD);
        _tvEmpresa = (TextView) findViewById(R.id.tvEmpresa);

        _banco = ConexaoSQLite.getInstancia(this);
        _perCTRL = new PerfilCtrl(_banco);
        _perfil = _perCTRL.getPerfilCtrl(1);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View hView =  navigationView.getHeaderView(0);
        hView.bringToFront();
        loadCartaoTela();
    }

    private void initToolbar ( Toolbar mToolbar ) {
        mToolbar.setTitleTextColor ( getResources ().getColor ( R.color.branco ) );
        setSupportActionBar ( mToolbar );
        ActionBar actionbar = getSupportActionBar ();
        actionbar.setDisplayHomeAsUpEnabled ( true );
        actionbar.setHomeAsUpIndicator ( R.drawable.ic_dehaze_white_24dp );
    }

    private void loadCartaoTela() {
        if (null != _cartao) {

            if (_cartao.getSituacao().toUpperCase().equals("ATIVO")) {
                ImageView _btnBloqueio = (ImageView) findViewById(R.id.imgbloqueio);
                _btnBloqueio.setBackgroundResource(R.drawable.bloqueio_bg);
            } else {
                ImageView _btnBloqueio = (ImageView) findViewById(R.id.imgbloqueio);
                _btnBloqueio.setBackgroundResource(R.drawable.desbloqueio_bg);
            }

            try {
                _CartaoNumero.setText(_cartao.getCartaoNumFmt());
                _CartaoNome.setText(_cartao.getNome());
                _UltimaConsulta.setText("Consulta: "+_cartao.getUltimaConsulta());
                _Situacao.setText("Situação: "+_cartao.getSituacao());
                _tvEmpresa.setText("Empresa: "+_cartao.getEmpresa());
            } catch (Exception e) {

            }
            /*
            String URLbase  = _cartao.getImagemCartao().substring(0, _cartao.getImagemCartao().lastIndexOf("/") + 1 ); //"https://s3-us-west-2.amazonaws.com/cdn.comprocard/imagens/cartao_alimentacao.png";
            String fileName = _cartao.getImagemCartao().substring(_cartao.getImagemCartao().lastIndexOf("/"), _cartao.getImagemCartao().length());
            String versaoImagem  = getResources().getString(R.string.sistemaURLFaturas);
            URLbase += versaoImagem + fileName;*/

            try {
                //Picasso.with(detalheCartaoActivity.this).load("file://" + _cartao.getImagemCartao() ).placeholder(R.drawable.cartao_default).fit().into(_imgCartao);
                _imgCartao.setImageResource ( R.drawable.ultimodet);
                //Picasso.with(detalheCartaoActivity.this).load("file://" + URLbase ).placeholder(R.drawable.ultimodet).fit().into(_imgCartao);
                //_imgCartao.getBackground().setAlpha(50);
                //_imgCartao.setAlpha(50);
            } catch (Exception e) {

            }

            System.gc();
            DecimalFormat df = new DecimalFormat("#,###,##0.00");
            _Saldo.setText("Saldo R$ " + df.format(_cartao.getSaldo()));
            if (Global.getInstance().isConected == true) {
                _Saldo.setPaintFlags(0);
            } else {
                _Saldo.setPaintFlags(_Saldo.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }
        } else {
            Toast.makeText(detalheCartaoActivity.this, "Não foi possível exibir detalhes deste cartão no momento.", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    /*@SuppressLint("NewApi")
    public static Bitmap blurRenderScript(Context context,Bitmap smallBitmap, int radius) {
        try {
            smallBitmap = RGB565toARGB888(smallBitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Bitmap bitmap = Bitmap.createBitmap(
                smallBitmap.getWidth(), smallBitmap.getHeight(),
                Bitmap.Config.ARGB_8888);

        RenderScript renderScript = RenderScript.create(context);

        Allocation blurInput = Allocation.createFromBitmap(renderScript, smallBitmap);
        Allocation blurOutput = Allocation.createFromBitmap(renderScript, bitmap);

        ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(renderScript,
                Element.U8_4(renderScript));
        blur.setInput(blurInput);
        blur.setRadius(radius); // radius must be 0 < r <= 25
        blur.forEach(blurOutput);

        blurOutput.copyTo(bitmap);
        renderScript.destroy();

        return bitmap;

    }

    private static Bitmap RGB565toARGB888(Bitmap img) throws Exception {
        int numPixels = img.getWidth() * img.getHeight();
        int[] pixels = new int[numPixels];

        //Get JPEG pixels.  Each int is the color values for one pixel.
        img.getPixels(pixels, 0, img.getWidth(), 0, 0, img.getWidth(), img.getHeight());

        //Create a Bitmap of the appropriate format.
        Bitmap result = Bitmap.createBitmap(img.getWidth(), img.getHeight(), Bitmap.Config.ARGB_8888);

        //Set RGB pixels.
        result.setPixels(pixels, 0, result.getWidth(), 0, 0, result.getWidth(), result.getHeight());
        return result;
    }*/

    @Override
    protected void onResume() {
        super.onResume();
        if (isConnected()) {
            postAutentica _taskAutentica = new postAutentica(Global.getInstance().apiURLAutenticacao, _perfil, detalheCartaoActivity.this, false);
            _taskAutentica.setOnResultsListener(detalheCartaoActivity.this);
            _taskAutentica.execute();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            finish();
        } else if (id == R.id.nav_camera) {
            Global.getInstance().isEditPerfil = true;
            Intent intent = new Intent(getApplicationContext(), perfilActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
        } else if (id == R.id.nav_gallery) {
            Intent intent = new Intent(getApplicationContext(),  incluirCartaoActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
        } else if (id == R.id.nav_slideshow) {
            Intent intent = new Intent(getApplicationContext(),  indicacaoActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
        } else if (id == R.id.nav_share) {
            Intent intent = new Intent(getApplicationContext(),  ajudaActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
        } else if (id == R.id.nav_send) {
            Intent intent = new Intent(getApplicationContext(),  sobreActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
        }

        //DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_detalhe_cartao);
        //drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    public void fechar(View paramView) {
       finish();
    }

    public void BloqDesbloq(View paramView) {
        if (_cartao.getSituacao().toUpperCase().equals("ATIVO")) {
            _acaoCartao = "BLOQ";
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)detalheCartaoActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View titleView = inflater.inflate(R.layout.custom_dialog, null);
            ((TextView) titleView.findViewById(R.id.partName)).setText(" Meu ComproCard");
            builder.setCustomTitle(titleView);
            builder.setMessage("Bloquear cartão " + _cartao.getCartaoNumFmt() + "?");
            builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                postCartaoBloqDesbq _taskGetCartaoBloqDesbq = new postCartaoBloqDesbq(Global.getInstance().apiURLBloquear, detalheCartaoActivity.this);
                _taskGetCartaoBloqDesbq.setOnResultsListener(detalheCartaoActivity.this);
                _taskGetCartaoBloqDesbq.execute();
                }
            });
            builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        } else {
            _acaoCartao = "DESBLOQ";

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)detalheCartaoActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View titleView = inflater.inflate(R.layout.custom_dialog, null);
            ((TextView) titleView.findViewById(R.id.partName)).setText(" Meu ComproCard");
            builder.setCustomTitle(titleView);
            builder.setMessage("Desbloquear cartão " + _cartao.getCartaoNumFmt() + "?");
            builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    postCartaoBloqDesbq _taskGetCartaoBloqDesbq = new postCartaoBloqDesbq(Global.getInstance().apiURLDesbloquear, detalheCartaoActivity.this);
                    _taskGetCartaoBloqDesbq.setOnResultsListener(detalheCartaoActivity.this);
                    _taskGetCartaoBloqDesbq.execute();
                }
            });
            builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    public void abreExtrato(View paramView) {
        Intent intent = new Intent(getApplicationContext(), ExtratoActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.putExtra("ID_CARTAO", String.valueOf(_idCartao));
        startActivity(intent);
    }

    public void removerCartao(View paramView) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater)detalheCartaoActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View titleView = inflater.inflate(R.layout.custom_dialog, null);
        ((TextView) titleView.findViewById(R.id.partName)).setText(" Meu ComproCard");
        builder.setCustomTitle(titleView);
        builder.setMessage("Remover o cartão " + _cartao.getCartaoNumFmt() + " da sua lista de cartões?");
        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (_carCTRL.excluirCartaoCtrl( _idCartao )){
                    Toast.makeText(detalheCartaoActivity.this, "Cartão removido!", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(detalheCartaoActivity.this, "Não foi possível remover este cartão no momento.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void onResultsSucceeded(resServer result) {
        int httpStatus;
        JSONObject _json;
        String _mensagem = "";

        try {
            httpStatus = result.getHttpStatus();
            _json = result.getJsonBody();

            if (httpStatus == HttpURLConnection.HTTP_OK) {
                Global.getInstance().apiTokenPerfil = _json.getString("token");
                String _cartaoNumero = getCartaoDecript(String.valueOf(_cartao.getNumero()));
                postCartaoLogin _taskGetCartaoLogin = new postCartaoLogin(Global.getInstance().apiURLLogin, _cartaoNumero, detalheCartaoActivity.this);
                _taskGetCartaoLogin.setOnResultsListener(this);
                _taskGetCartaoLogin.execute();
            } else {
                _mensagem = _json.getString("mensagem");
                if (_mensagem != "") {
                    Toast.makeText(detalheCartaoActivity.this, _mensagem, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(detalheCartaoActivity.this, "Tente novamente mais tarde", Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            Toast.makeText(detalheCartaoActivity.this, "Falha na conexão de rede.", LENGTH_LONG).show(); //+ e.getMessage()
            System.gc();
            return;
        }
    }

    public void onResultsSucceededRefresh(resServer result) {
        int httpStatus;
        JSONObject _json;
        JSONObject _jsonCartao;
        String _jsonToken;
        String _jsonExpLoginCartao;
        String _mensagem = "";

        try {
            httpStatus = result.getHttpStatus();
            _json = result.getJsonBody();

            if (httpStatus == HttpURLConnection.HTTP_OK) {
                Cartao _cartaoServer = new Cartao();
                _jsonCartao = _json.getJSONObject("cartao");
                _jsonToken  = _json.getString("token");
                _jsonExpLoginCartao = _json.getString("dataExpiracao").replaceAll("T","");
                Global.getInstance().apiTokenCartao = _jsonToken;
                _cartaoServer.setCartaoId(Integer.parseInt(_jsonCartao.getString("CartaoId")));
                _cartaoServer.setBandeira("ComproCard");
                _cartaoServer.setProduto(_jsonCartao.getString("ProdutoDesc"));
                _cartaoServer.setEmpresa(_jsonCartao.getString("EmpresaNome"));
                _cartaoServer.setNome(_jsonCartao.getString("CartaoNome"));
                _cartaoServer.setNumero(getCartaoCript(_jsonCartao.getString("CartaoNumero").toString()));
                _cartaoServer.setCartaoNumFmt(_jsonCartao.getString("CartaoNumFmt").toString());
                _cartaoServer.setUltimaConsulta(_jsonCartao.getString("dataUltimaConsulta"));
                _cartaoServer.setValidade("31/05/2095");
                _cartaoServer.setCVV("999");
                _cartaoServer.setSaldo(new BigDecimal(_jsonCartao.getString("CartaoSaldo")));
                _cartaoServer.setLimite(new BigDecimal(_jsonCartao.getString("CartaoLimite")));
                _cartaoServer.setDiaVencimento(Integer.parseInt(_jsonCartao.getString("CartaoVencto")));
                _cartaoServer.setSituacao(_jsonCartao.getString("SituacaoDescr"));
                _cartaoServer.setSenha(_jsonCartao.getString("exigeSenha"));
                _cartaoServer.setToken(Global.getInstance().apiTokenCartao);
                _cartaoServer.setUltimaConsulta(_jsonCartao.getString("dataUltimaConsulta"));
                //_cartaoServer.setImagemCartao(_jsonCartao.getString("ImagemCartao"));
                salvarCartao(_cartaoServer);
                loadCartaoTela();
            } else {
                _mensagem = _json.getString("mensagem");
                if (_mensagem != "") {
                    Toast.makeText(detalheCartaoActivity.this, _mensagem, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(detalheCartaoActivity.this, "Tente novamente mais tarde", Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            Toast.makeText(detalheCartaoActivity.this, "Falha na conexão de rede.", LENGTH_LONG).show(); //+ e.getMessage()
            System.gc();
            return;
        }
    }

    public void onResultsBloqDesbloq(resServer result) {
        int httpStatus;
        JSONObject _json;
        String _mensagem = "";

        try {
            httpStatus = result.getHttpStatus();
            _json = result.getJsonBody();

            if (httpStatus == HttpURLConnection.HTTP_OK) {
                Cartao _cartaoAtual = _carCTRL.getCartaoCtrlByCartaoId( _idCartao );
                String _mensagemb = "";
                if (_acaoCartao.toUpperCase().equals("BLOQ")){
                    //_cartao.setSituacao("BLOQUEADO");
                    _mensagemb = "Cartão bloqueado!";
                    _cartaoAtual.setSituacao("BLOQUEIO TEMPORÁRIO");
                    ImageView _btnBloqueio = (ImageView) findViewById(R.id.imgbloqueio);
                    _btnBloqueio.setBackgroundResource(R.drawable.desbloqueio_bg);
                } else {
                    //_cartao.setSituacao("ATIVO");
                    _mensagemb = "Cartão desbloqueado!";
                    _cartaoAtual.setSituacao("ATIVO");
                    ImageView _btnBloqueio = (ImageView) findViewById(R.id.imgbloqueio);
                    _btnBloqueio.setBackgroundResource(R.drawable.desbloqueio_bg);
                }
                salvarCartao(_cartaoAtual);
                Toast.makeText(detalheCartaoActivity.this, _mensagemb, Toast.LENGTH_SHORT).show();
                _cartao = _carCTRL.getCartaoCtrlByCartaoId( _idCartao );
                loadCartaoTela();
            } else {
                _mensagem = _json.getString("mensagem");
                if (_mensagem != "") {
                    Toast.makeText(detalheCartaoActivity.this, _mensagem, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(detalheCartaoActivity.this, "Tente novamente mais tarde", Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            Toast.makeText(detalheCartaoActivity.this, "Falha na conexão de rede.", LENGTH_LONG).show(); //+ e.getMessage()
            System.gc();
            return;
        }
    }


    public void salvarCartao(Cartao _cartao) {
        CartaoCtrl _cartaoCTRL = new CartaoCtrl(_banco);
        _cartaoCTRL.salvarCartaoCtrl(_cartao, String.valueOf(_cartao.getCartaoId()));
    }

    private String getCartaoDecript(String _numeroCartaoCrip) {
        String result = "";
        try {
            MCrypt mcrypt = new MCrypt();
            result = new String( mcrypt.decrypt( _numeroCartaoCrip ) );
        } catch (Exception e) {
            Log.d("Erro", e.getMessage());
        }
        return result;
    }

    private String getCartaoCript(String _numeroCartao) {
        String encrypted = _numeroCartao;
        try {
            MCrypt mcrypt = new MCrypt();
            encrypted = mcrypt.bytesToHex(mcrypt.encrypt(_numeroCartao));
        } catch (Exception e) {
            Log.d("Erro", e.getMessage());
        }
        return encrypted;
    }

    public boolean isConnected()
    {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(meuCartaoActivity.CONNECTIVITY_SERVICE);
        NetworkInfo net = cm.getActiveNetworkInfo();
        if (net!=null && net.isAvailable() && net.isConnected()) {
            Global.getInstance().isConected = true;
            return true;
        } else {
            Global.getInstance().isConected = false;
            Toast.makeText(getApplicationContext(), "Por favor conecte-se a internet", Toast.LENGTH_LONG).show();
            return false;
        }
    }


}
