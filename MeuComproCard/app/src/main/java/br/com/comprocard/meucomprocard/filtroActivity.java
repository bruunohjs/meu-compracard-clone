package br.com.comprocard.meucomprocard;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Arrays;

import br.com.comprocard.meucomprocard.DataSync.ResultsListener;
import br.com.comprocard.meucomprocard.DataSync.getGuiaCompras;
import br.com.comprocard.meucomprocard.DataSync.getGuiaComprasBairros;
import br.com.comprocard.meucomprocard.DataSync.getGuiaComprasCidades;
import br.com.comprocard.meucomprocard.DataSync.getGuiaComprasRamos;
import br.com.comprocard.meucomprocard.DataSync.resServer;
import br.com.comprocard.meucomprocard.Model.guiaCompras;
import br.com.comprocard.meucomprocard.Util.Global;

import static android.widget.Toast.LENGTH_LONG;


public class filtroActivity extends AppCompatActivity implements ResultsListener {
    //private Spinner spGuiUF;
    //private TextView labelUFSel;
    //private ConexaoSQLite _banco;
    private TextView filterText;
    private Boolean _searchCity = false;
    ArrayAdapter<String> adapterCidade = null;
    InputMethodManager imm;
    String _tipoPesquisa = "";
    ArrayList<String> _cidadeList;
    ArrayList<String> _bairroList;
    ArrayList<String> _ramoList;
    ArrayList<guiaCompras> _resultadoPesquisa;
    String _uf;
    String _cidade;
    String _bairro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filtro);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Filtro - Guia de Compras");
        toolbar.setTitleTextColor(getResources().getColor(R.color.branco));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeAsUpIndicator(getColoredArrow());

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        getCidadeByUF("ES");
    }

    private Drawable getColoredArrow() {
        Drawable arrowDrawable = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        Drawable wrapped = DrawableCompat.wrap(arrowDrawable);

        if (arrowDrawable != null && wrapped != null) {
            arrowDrawable.mutate();
            DrawableCompat.setTint(wrapped, Color.WHITE);
        }

        return wrapped;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void getUF(View paramView) {
        final TextView _txtUF = (TextView) findViewById(R.id.labelUFSel);
        final Dialog ufs_dialog;
        final ArrayList<String> ufsArrayList = new ArrayList<String>();
        final ListView ufs_listView;

        Global.getInstance().idSelected = -1;
        ufs_dialog = new Dialog(filtroActivity.this);
        ufs_dialog.setContentView(R.layout.dialog_uf_frag);
        ufs_dialog.setTitle("Estados");
        ufsArrayList.addAll(Arrays.asList(getResources().getStringArray(R.array.uf_arrays)));
        ArrayAdapter<String> adapterUFs = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, ufsArrayList);
        ufs_listView = (ListView) ufs_dialog.findViewById(R.id.listUfs);
        ufs_listView.setAdapter(adapterUFs);
        ufs_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                String _ufSelect = (String) ufs_listView.getItemAtPosition(position);
                //Toast.makeText(getApplicationContext(), _ufSelect, Toast.LENGTH_LONG).show();
                TextView _txtCidade = (TextView) findViewById(R.id.labelCidadeSelected);
                if (_txtCidade.getText().toString().equals("Pesquisar uma cidade") == false) {
                    _txtCidade.setText("Pesquisar uma cidade");
                    TextView _txtBairro = (TextView) findViewById(R.id.labelBairroSel);
                    _txtBairro.setText("Pesquisar um bairro");
                    TextView _txtRamo = (TextView) findViewById(R.id.labelRamoSel);
                    _txtRamo.setText("Pesquisar um ramo");
                }
                _txtUF.setText(_ufSelect);
                getCidadeByUF(_ufSelect);
                ufs_dialog.dismiss();
            }
        });
        ufs_dialog.show();
    }

    public void getCidade(View paramView) {
        final ListView cidades_listView;
        final TextView _txtCidade = (TextView) findViewById(R.id.labelCidadeSelected);
        EditText filterText = null;

        final Dialog cidades_dialog = new Dialog(filtroActivity.this);
        cidades_dialog.setContentView(R.layout.dialog_cidades_list);
        cidades_dialog.setTitle("Cidades");
        filterText = (EditText) cidades_dialog.findViewById(R.id.edtPesquisaCidade);
        filterText.addTextChangedListener(filterTextWatcher);
        adapterCidade = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, _cidadeList);
        cidades_listView = (ListView) cidades_dialog.findViewById(R.id.listCidades);
        cidades_listView.setAdapter(adapterCidade);
        cidades_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                String _cidadeSelect = (String) cidades_listView.getItemAtPosition(position);
                //Toast.makeText(filtroActivity.this, _cidadeSelect, Toast.LENGTH_LONG).show();
                _txtCidade.setText(_cidadeSelect);
                TextView _txtUF = (TextView) findViewById(R.id.labelUFSel);
                final TextView _txtBairro = (TextView) findViewById(R.id.labelBairroSel);
                _txtBairro.setText("Pesquisar um bairro");
                getBairroByUfCidade(_txtUF.getText().toString(), _cidadeSelect);
                _uf = _txtUF.getText().toString();
                _cidade = _cidadeSelect;
                imm = (InputMethodManager) getSystemService(cidades_dialog.getContext().INPUT_METHOD_SERVICE);
                enableBairro();
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                cidades_dialog.dismiss();

                if (_searchCity == false) {
                    Thread timer = new Thread() {
                        public void run() {
                            try {
                                sleep(200);
                                if (imm != null) {
                                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                                    _searchCity = false;
                                }
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    timer.start();
                }
            }
        });
        cidades_dialog.show();
    }

    public void getBairro(View paramView) {
        final TextView _txtBairro = (TextView) findViewById(R.id.labelBairroSel);
        final Dialog bairros_dialog;
        final ArrayList<String> bairrosArrayList = new ArrayList<String>();
        final ListView bairros_listView;

        Global.getInstance().idSelected = -1;
        bairros_dialog = new Dialog(filtroActivity.this);
        bairros_dialog.setContentView(R.layout.dialog_bairro_frag);
        bairros_dialog.setTitle("Estados");
        bairrosArrayList.addAll(_bairroList);
        ArrayAdapter<String> adapterBairros = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, bairrosArrayList);
        bairros_listView = (ListView) bairros_dialog.findViewById(R.id.listBairro);
        bairros_listView.setAdapter(adapterBairros);
        bairros_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                _bairro = (String) bairros_listView.getItemAtPosition(position);
                //Toast.makeText(getApplicationContext(), _bairroSelect, Toast.LENGTH_LONG).show();
                _txtBairro.setText(_bairro);
                bairros_dialog.dismiss();
                getRamoDeAtividade(_uf, _cidade, _bairro);
            }
        });
        bairros_dialog.show();
    }

    public void getRamo(View paramView) {
        final TextView _txtRamo = (TextView) findViewById(R.id.labelRamoSel);
        final Dialog ramo_dialog;
        final ArrayList<String> ramoArrayList = new ArrayList<String>();
        final ListView ramo_listView;

        Global.getInstance().idSelected = -1;
        ramo_dialog = new Dialog(filtroActivity.this);
        ramo_dialog.setContentView(R.layout.dialog_ramo_frag);
        ramo_dialog.setTitle("Ramos");
        ramoArrayList.addAll(_ramoList);
        ArrayAdapter<String> adapterRamos = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, ramoArrayList);
        ramo_listView = (ListView) ramo_dialog.findViewById(R.id.listRamos);
        ramo_listView.setAdapter(adapterRamos);
        ramo_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                String _ramoSelect = (String) ramo_listView.getItemAtPosition(position);
                //Toast.makeText(getApplicationContext(), _ramoSelect, Toast.LENGTH_LONG).show();
                _txtRamo.setText(_ramoSelect);
                ramo_dialog.dismiss();
            }
        });
        ramo_dialog.show();
    }

    private void getCidadeByUF(String _UF) {
        if (isConnected()) {
            _tipoPesquisa = "CIDADE";
            getGuiaComprasCidades _taskgetGuia = new getGuiaComprasCidades(Global.getInstance().apiURLguiaCompras + "/cidades", _UF, filtroActivity.this);
            _taskgetGuia.setOnResultsListener(filtroActivity.this);
            _taskgetGuia.execute();
        }
    }

    private void getBairroByUfCidade(String _UF, String _Cidade) {
        if (isConnected()) {
            String _json = "{ \"uf\": \"" + _UF + "\", \"cidade\" : \"" + _Cidade + "\", \"bairro\": \"\" }";
            _tipoPesquisa = "BAIRRO";
            getGuiaComprasBairros _taskgetGuia = new getGuiaComprasBairros(Global.getInstance().apiURLguiaCompras + "/bairros", _json, filtroActivity.this);
            _taskgetGuia.setOnResultsListener(filtroActivity.this);
            _taskgetGuia.execute();
        }
    }

    private void getRamoDeAtividade(String _UF, String _Cidade, String _Bairro) {
        if (isConnected()) {
            String _json = "";
            if (_Bairro == null) {
                _json = "{ \"uf\": \"" + _UF +
                        "\", \"cidade\" : \"" + _Cidade +
                        "\", \"bairro\" : " + _Bairro +
                        ", \"ramo\": \"\", \"estabelecimento\": \"\" }";
            } else {
                _json = "{ \"uf\": \"" + _UF +
                        "\", \"cidade\" : \"" + _Cidade +
                        "\", \"bairro\" : \"" + _Bairro +
                        "\", \"ramo\": \"\", \"estabelecimento\": \"\" }";
            }
            _tipoPesquisa = "RAMO";
            getGuiaComprasRamos _taskgetGuia = new getGuiaComprasRamos(Global.getInstance().apiURLguiaCompras + "/ramos", _json, filtroActivity.this);
            _taskgetGuia.setOnResultsListener(filtroActivity.this);
            _taskgetGuia.execute();
        }
    }

    private void enableBairro() {
        RelativeLayout _rlBairro = (RelativeLayout) findViewById(R.id.rlBairro);
        _rlBairro.setBackground(getResources().getDrawable(R.drawable.layout_bg));
        _rlBairro.setPadding(12, 12, 12, 12);
        ImageView imgBusca = (ImageView) findViewById(R.id.btnImagem);
        imgBusca.setBackground(getResources().getDrawable(R.drawable.ic_search_black_24dp));
        TextView _labelBairroCap = (TextView) findViewById(R.id.labelIdBairro);
        _labelBairroCap.setTextColor(getResources().getColor(R.color.cinzaescuro));
        TextView _labelBairroSel = (TextView) findViewById(R.id.labelBairroSel);
        _labelBairroSel.setTextColor(Color.BLACK);
        _rlBairro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBairro(v);
            }
        });
    }

    private void enableRamo() {
        RelativeLayout _rlRamo = (RelativeLayout) findViewById(R.id.rlRamo);
        _rlRamo.setBackground(getResources().getDrawable(R.drawable.layout_bg));
        _rlRamo.setPadding(12, 12, 12, 12);
        ImageView imgBuscaRamo = (ImageView) findViewById(R.id.btnPesqRamo);
        imgBuscaRamo.setBackground(getResources().getDrawable(R.drawable.ic_search_black_24dp));
        TextView _labelRamoCap = (TextView) findViewById(R.id.labelIdRamo);
        _labelRamoCap.setTextColor(getResources().getColor(R.color.cinzaescuro));
        TextView _labelRamo = (TextView) findViewById(R.id.labelRamoSel);
        _labelRamo.setTextColor(Color.BLACK);
        _rlRamo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getRamo(v);
            }
        });
    }

    private TextWatcher filterTextWatcher = new TextWatcher() {
        public void afterTextChanged(Editable s) {

        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            adapterCidade.getFilter().filter(s);
            _searchCity = true;
        }
    };

    @Override
    public void onStop() {
        super.onStop();
        //filterText.removeTextChangedListener(filterTextWatcher);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private ArrayList<String> listaCidades(resServer result) {
        int httpStatus;
        JSONArray _jsonResp;
        try {
            httpStatus = result.getHttpStatus();
            _jsonResp = result.getJsonArray();
            ArrayList<String> _result = new ArrayList<String>();
            _result.clear();
            if (httpStatus == HttpURLConnection.HTTP_OK) {
                try {
                    JSONArray array = _jsonResp;
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject _jsonItem = array.getJSONObject(i);
                        _result.add(_jsonItem.getString("Cidade"));
                    }
                    _cidadeList = _result;
                } catch (JSONException e) {
                    e.printStackTrace();
                    _cidadeList = null;
                }

            } else {
                Toast.makeText(filtroActivity.this, "Falha de conexão, tente novamente mais tarde.", LENGTH_LONG).show(); //+ e.getMessage()
                System.gc();
                finish();
            }
        } catch (Exception e) {
            Toast.makeText(filtroActivity.this, "Falha na conexão de rede.", LENGTH_LONG).show(); //+ e.getMessage()
            System.gc();
            _cidadeList = null;
        }
        return _cidadeList;
    }

    private ArrayList<String> listaBairros(resServer result) {
        int httpStatus;
        JSONArray _jsonResp;
        try {
            httpStatus = result.getHttpStatus();
            _jsonResp = result.getJsonArray();
            ArrayList<String> _result = new ArrayList<String>();
            _result.clear();
            if (httpStatus == HttpURLConnection.HTTP_OK) {
                try {
                    JSONArray array = _jsonResp;
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject _jsonItem = array.getJSONObject(i);
                        _result.add(_jsonItem.getString("Bairro"));
                    }
                    _bairroList = _result;
                } catch (JSONException e) {
                    e.printStackTrace();
                    _bairroList = null;
                }

            } else {
                Toast.makeText(filtroActivity.this, "Falha de conexão, tente novamente mais tarde.", LENGTH_LONG).show();
                System.gc();
                finish();
            }
        } catch (Exception e) {
            Toast.makeText(filtroActivity.this, "Falha na conexão de rede.", LENGTH_LONG).show(); //+ e.getMessage()
            System.gc();
            _bairroList = null;
        }
        return _bairroList;
    }

    private ArrayList<String> listaRamos(resServer result) {
        int httpStatus;
        JSONArray _jsonResp;
        try {
            httpStatus = result.getHttpStatus();
            _jsonResp = result.getJsonArray();
            ArrayList<String> _result = new ArrayList<String>();
            _result.clear();
            if (httpStatus == HttpURLConnection.HTTP_OK) {
                try {
                    JSONArray array = _jsonResp;
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject _jsonItem = array.getJSONObject(i);
                        _result.add(_jsonItem.getString("RamoAtividade"));
                    }
                    _ramoList = _result;
                } catch (JSONException e) {
                    e.printStackTrace();
                    _ramoList = null;
                }

            } else {
                Toast.makeText(filtroActivity.this, "Falha de conexão, tente novamente mais tarde.", LENGTH_LONG).show();
                System.gc();
                //finish();
            }
        } catch (Exception e) {
            Toast.makeText(filtroActivity.this, "Falha na conexão de rede.", LENGTH_LONG).show(); //+ e.getMessage()
            System.gc();
            _ramoList = null;
        }
        return _ramoList;
    }

    private ArrayList<guiaCompras> listaResultadoFinal(resServer result) {
        int httpStatus;
        JSONArray _jsonResp;
        try {
            httpStatus = result.getHttpStatus();
            _jsonResp = result.getJsonArray();

            ArrayList<guiaCompras> _guiaCompras = new ArrayList<guiaCompras>();

            if (httpStatus == HttpURLConnection.HTTP_OK) {
                try {
                    JSONArray array = _jsonResp;
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject _jsonItem = array.getJSONObject(i);
                        guiaCompras _itemGuia = new guiaCompras();
                        _itemGuia.setEstabelecimento(_jsonItem.getString("Estabelecimento"));
                        _itemGuia.setAtividade(_jsonItem.getString("Atividade"));
                        _itemGuia.setBairro(_jsonItem.getString("Bairro"));
                        _itemGuia.setCidade(_jsonItem.getString("Cidade"));
                        _itemGuia.setUF(_jsonItem.getString("UF"));
                        _itemGuia.setTeletone(_jsonItem.getString("Telefone"));
                        _itemGuia.setGeo_Latitude(_jsonItem.getString("geo_Latitude"));
                        _itemGuia.setGeo_Longitude(_jsonItem.getString("geo_Longitude"));
                        _guiaCompras.add(_itemGuia);
                    }
                    _resultadoPesquisa = _guiaCompras;
                } catch (JSONException e) {
                    e.printStackTrace();
                    _resultadoPesquisa = null;
                }

            } else {
                Toast.makeText(filtroActivity.this, "Falha de conexão, tente novamente mais tarde.", LENGTH_LONG).show();
                System.gc();
                finish();
            }
        } catch (Exception e) {
            Toast.makeText(filtroActivity.this, "Falha na conexão de rede.", LENGTH_LONG).show(); //+ e.getMessage()
            System.gc();
            _resultadoPesquisa = null;
        }
        return _resultadoPesquisa;
    }

    public boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(filtroActivity.CONNECTIVITY_SERVICE);
        NetworkInfo net = cm.getActiveNetworkInfo();
        if (net != null && net.isAvailable() && net.isConnected()) {
            Global.getInstance().isConected = true;
            return true;
        } else {
            Global.getInstance().isConected = false;
            Toast.makeText(getApplicationContext(), "Por favor conecte-se a internet", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public void onResultsSucceeded(resServer result) {

        switch (_tipoPesquisa) {
            case "CIDADE":
                _cidadeList = listaCidades(result);
                break;

            case "BAIRRO":
                _bairroList = listaBairros(result);
                getRamoDeAtividade(_uf, _cidade, _bairro);
                break;

            case "RAMO":
                _ramoList = listaRamos(result);
                enableRamo();
                break;

            case "FINAL":
                _resultadoPesquisa = listaResultadoFinal(result);
                Global.getInstance().guiaComprasList = _resultadoPesquisa;
                Intent myIntent = new Intent(getApplicationContext(), EstabelecimentosActivity.class);
                startActivityForResult(myIntent, 0);
                break;
        }
    }

    public void getLojas(View param) {
        String _estado;
        String _cidade;
        String _bairro;
        String _ramo;
        String _loja;

        TextView _txtUF = (TextView) findViewById(R.id.labelUFSel);
        TextView _txtCidade = (TextView) findViewById(R.id.labelCidadeSelected);
        TextView _txtBairro = (TextView) findViewById(R.id.labelBairroSel);
        TextView _txtRamo = (TextView) findViewById(R.id.labelRamoSel);
        EditText _txtPesquisa = (EditText) findViewById(R.id.labelnomeEstabSel);

        _estado = _txtUF.getText().toString();
        _cidade = _txtCidade.getText().toString();
        _bairro = _txtBairro.getText().toString();
        _ramo = _txtRamo.getText().toString();
        _loja = _txtPesquisa.getText().toString();

        if (_cidade.equals("Pesquisar uma cidade")) {
            _cidade = "";
        }

        if (_bairro.equals("Pesquisar um bairro")) {
            _bairro = "";
        }

        if (_ramo.equals("Pesquisar um ramo")) {
            _ramo = "";
        }

        if (_loja.equals("(Digite aqui...)")) {
            _ramo = "";
        }

        String _JsonPesquisaFinal = "{ \"uf\": \""
                + _estado + "\", \"cidade\" : \""
                + _cidade + "\", \"bairro\":\""
                + _bairro + "\", \"ramo\" : \""
                + _ramo + "\", \"estabelecimento\": \""
                + _loja + "\" }";

        if (isConnected()) {
            _tipoPesquisa = "FINAL";
            getGuiaCompras _taskgetGuia = new getGuiaCompras(Global.getInstance().apiURLguiaCompras + "/estabelecimentos", _JsonPesquisaFinal, filtroActivity.this);
            _taskgetGuia.setOnResultsListener(filtroActivity.this);
            _taskgetGuia.execute();
        }
    }

    public void getLojasByLocation(double latitude, double longitude) {
        String _estado = "";
        String _cidade = "";
        String _bairro = "";
        String _ramo = "";
        String _loja = "";

        String _JsonPesquisaFinal = "{ \"uf\": \""
                + _estado + "\", \"cidade\" : \""
                + _cidade + "\", \"bairro\":\""
                + _bairro + "\", \"ramo\" : \""
                + _ramo + "\", \"latitude\": \""
                + latitude + "\", \"longitude\": \""
                + longitude + "\" }";

        if (isConnected()) {
            _tipoPesquisa = "FINAL";
            getGuiaCompras _taskgetGuia = new getGuiaCompras(Global.getInstance().apiURLguiaCompras + "/estabelecimentos", _JsonPesquisaFinal, filtroActivity.this);
            _taskgetGuia.setOnResultsListener(filtroActivity.this);
            _taskgetGuia.execute();
        }
    }

}
