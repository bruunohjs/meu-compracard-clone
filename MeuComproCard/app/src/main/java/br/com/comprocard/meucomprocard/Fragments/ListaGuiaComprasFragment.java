package br.com.comprocard.meucomprocard.Fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import br.com.comprocard.meucomprocard.Adapters.guiaComprasAdapter;
import br.com.comprocard.meucomprocard.Model.guiaCompras;
import br.com.comprocard.meucomprocard.R;
import br.com.comprocard.meucomprocard.Util.Global;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListaGuiaComprasFragment extends Fragment {

    public ArrayList<guiaCompras> _guiaComprasArray = new ArrayList<>();
    private guiaComprasAdapter _guiaAdapter;
    private RecyclerView _guiaRecyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_lista_guia_compras, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        _guiaRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_guia);

        criarPopularGuiaComprasArray(Global.getInstance().guiaComprasList);

    }

    private void criarPopularGuiaComprasArray(ArrayList<guiaCompras> _resultadoBusca) {
        _guiaComprasArray = _resultadoBusca;
        guiaoCarregaLista();
    }

    private void guiaoCarregaLista() {
        _guiaAdapter = new guiaComprasAdapter(getActivity(), _guiaComprasArray);
        _guiaRecyclerView.setAdapter(_guiaAdapter);
        _guiaRecyclerView.setItemAnimator(new DefaultItemAnimator());
        _guiaRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

}
