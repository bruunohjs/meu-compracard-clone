package br.com.comprocard.meucomprocard.Controller;

import java.util.ArrayList;

import br.com.comprocard.meucomprocard.DAO.CartaoDAO;
import br.com.comprocard.meucomprocard.dbHelper.ConexaoSQLite;
import br.com.comprocard.meucomprocard.Model.Cartao;

public class CartaoCtrl {
	private CartaoDAO daoCartao;

	public CartaoCtrl() {
	} 

	public CartaoCtrl(ConexaoSQLite pSQLite) {
		this.daoCartao = new CartaoDAO(pSQLite);
	} 

	public long salvarCartaoCtrl(Cartao pCarta, String CartaoId) {
		return this.daoCartao.salvarCartaoDAO(pCarta, CartaoId);
	} 

	public Cartao getCartaoCtrlByCartaoId(int pId) {
		return this.daoCartao.getCartaoDAOByCartaoId(pId);
	} 

	public ArrayList<Cartao> getListaCartaoCtrl() {
		return this.daoCartao.getListaCartaoDAO();
	} 

	public boolean excluirCartaoCtrl(long pId) {
		return this.daoCartao.excluirCartaoDAO(pId);
	} 

}