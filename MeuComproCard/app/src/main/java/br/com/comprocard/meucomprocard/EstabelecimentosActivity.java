package br.com.comprocard.meucomprocard;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import java.util.ArrayList;

import br.com.comprocard.meucomprocard.Adapters.tablayout.TabAdapter;
import br.com.comprocard.meucomprocard.Fragments.ListaGuiaComprasFragment;
import br.com.comprocard.meucomprocard.Fragments.MapsFragment;
import br.com.comprocard.meucomprocard.Model.guiaCompras;
import br.com.comprocard.meucomprocard.Util.Global;

public class EstabelecimentosActivity extends AppCompatActivity {

    private TabAdapter adapter;
    private TabLayout tab;
    private ViewPager viewPager;

    private Toolbar toolbar;
    public ArrayList<guiaCompras> _guiaComprasArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estabelecimentos);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarc);
        toolbar.setTitle("Estabelecimentos");
        toolbar.setTitleTextColor(getResources().getColor(R.color.branco));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar = findViewById(R.id.toolbarc);
        toolbar.setTitle(getString(R.string.app_name));

        setSupportActionBar(toolbar);


        criarPopularGuiaComprasArray(Global.getInstance().guiaComprasList);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void criarPopularGuiaComprasArray(ArrayList<guiaCompras> _resultadoBusca) {
        _guiaComprasArray = _resultadoBusca;

        if (null == _guiaComprasArray || _guiaComprasArray.isEmpty()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Atenção");
            builder.setMessage("Nenhum estabelecimento encontrado próximo a sua localização!");
            builder.setCancelable(false);
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });
            builder.create()
                    .show();
        } else {
            configuraTabBar();
        }
    }

    private void configuraTabBar() {
        tab = findViewById(R.id.tab);
        viewPager = findViewById(R.id.viewpager);

        adapter = new TabAdapter(getSupportFragmentManager());
        adapter.addFragment(new ListaGuiaComprasFragment(), "Lista");
        adapter.addFragment(new MapsFragment(), "Mapa");

        viewPager.setAdapter(adapter);
        tab.setupWithViewPager(viewPager);
    }
}
