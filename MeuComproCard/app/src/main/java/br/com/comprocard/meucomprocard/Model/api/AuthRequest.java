package br.com.comprocard.meucomprocard.Model.api;

import java.io.Serializable;

public class AuthRequest{
    private String cpf;
    private String deviceId;

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
