package br.com.comprocard.meucomprocard.Model;

import java.math.BigDecimal;

/**
 * Created by andre.cardoso on 23/11/2016.
 */

public class Historico {
    private int ID;
    private int cartaoID;
    private String DataHora;
    private BigDecimal Valor;
    private String Tipo;
    private String Descricao;
    private int Parcela;

    public int getParcela() {
        return Parcela;
    }

    public void setParcela(int parcela) {
        Parcela = parcela;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getCartaoID() {
        return cartaoID;
    }

    public void setCartaoID(int cartaoID) {
        this.cartaoID = cartaoID;
    }

    public String getDataHora() {
        return DataHora;
    }

    public void setDataHora(String dataHora) {
        DataHora = dataHora;
    }

    public BigDecimal getValor() {
        return Valor;
    }

    public void setValor(BigDecimal valor) {
        Valor = valor;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String tipo) {
        Tipo = tipo;
    }

    public String getDescricao() {
        return Descricao;
    }

    public void setDescricao(String descricao) {
        Descricao = descricao;
    }

    @Override
    public String toString() {
        return "Historico [ID="+ID+", cartaoID="+cartaoID+", DataHora="+DataHora+", Valor="+Valor+", Tipo="+Tipo+", Descricao="+Descricao+"]";
    }
}
