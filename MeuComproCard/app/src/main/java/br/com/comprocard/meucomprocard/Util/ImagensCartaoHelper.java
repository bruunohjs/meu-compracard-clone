package br.com.comprocard.meucomprocard.Util;

import android.os.Environment;

import java.io.File;

/**
 * Created by andre.cardoso on 11/01/2017.
 */

public class ImagensCartaoHelper {

    private final String CAMINHO_IMAGENS_CARTOES = Environment
            .getExternalStorageDirectory()
            + File.separator
            + "MeuComproCard"
            + File.separator
            + "Cards";

    public String getCaminhoImagensCartoes() {

        final File dir = new File(this.CAMINHO_IMAGENS_CARTOES);

        if (!dir.exists()) {
            dir.mkdirs();
        }

        return this.CAMINHO_IMAGENS_CARTOES;
    }

    public String getCaminhoImagenCartaoByURL(String URL) {

        final File dir = new File(this.CAMINHO_IMAGENS_CARTOES);

        if (!dir.exists()) {
            dir.mkdirs();
        }

        String nome = URL.substring(URL.lastIndexOf('/') + 1);

        return this.CAMINHO_IMAGENS_CARTOES + File.separator + nome ;
    }

}
