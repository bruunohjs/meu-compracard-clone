package br.com.comprocard.meucomprocard.Model.api;

import java.util.ArrayList;
import java.util.List;

public class PublicidadeResponse {
    List< Publicidade > data = new ArrayList<>();
    private float statusCode;
    private String message;

    // Getter Methods
    public float getStatusCode() {
        return statusCode;
    }

    public String getMessage() {
        return message;
    }

    // Setter Methods
    public void setStatusCode(float statusCode) {
        this.statusCode = statusCode;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Publicidade> getData() {
        return data;
    }

    public void setData(List<Publicidade> data) {
        this.data = data;
    }
}