package br.com.comprocard.meucomprocard.Fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import br.com.comprocard.meucomprocard.Model.guiaCompras;
import br.com.comprocard.meucomprocard.R;
import br.com.comprocard.meucomprocard.Util.Global;

public class MapsFragment extends Fragment implements OnMapReadyCallback {

    private static final int MAP_PADDING_BOUNDS = 30;
    private GoogleMap mMap;
    public ArrayList<guiaCompras> _guiaComprasArray;
    public LatLngBounds.Builder builder = new LatLngBounds.Builder();

    private TextView tvStoreName, tvStoreAddress, tvStoreFone;
    private ImageView ivStoreRoute;
    private CardView cardView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_mapa, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        cardView = view.findViewById(R.id.map_store_card);
        tvStoreName = view.findViewById(R.id.map_store_name);
        tvStoreAddress = view.findViewById(R.id.map_store_address);
        tvStoreFone = view.findViewById(R.id.map_store_phone);
        ivStoreRoute = view.findViewById(R.id.map_store_route);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                cardView.setVisibility(View.VISIBLE);
                guiaCompras gc = (guiaCompras) marker.getTag();
                populaDadosLoja(gc);
                return false;
            }
        });
        criarPopularGuiaComprasArray(Global.getInstance().guiaComprasList);
    }

    private void criarPopularGuiaComprasArray(ArrayList<guiaCompras> _resultadoBusca) {
        _guiaComprasArray = _resultadoBusca;
        carregarMarkers();
        centralizaCamera();
    }

    private void populaDadosLoja(final guiaCompras gc) {
        tvStoreName.setText(gc.getEstabelecimento());
        tvStoreAddress.setText(String.format("%s %s", gc.getBairro(), gc.getCidade()));
        tvStoreFone.setText(gc.getTeletone());
        ivStoreRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strUri = "http://maps.google.com/maps?q=loc:" +
                        gc.getGeo_Latitude() + "," + gc.getGeo_Longitude();
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(strUri));
                intent.setClassName("com.google.android.apps.maps",
                        "com.google.android.maps.MapsActivity");
                requireContext().startActivity(intent);

            }
        });
    }

    private void carregarMarkers() {
        try {
            for (guiaCompras gc : _guiaComprasArray) {
                if (!(gc.getGeo_Latitude().equals("")) || !(gc.getGeo_Longitude().equals(""))) {
                    double latitude = Double.parseDouble(gc.getGeo_Latitude());
                    double longitude = Double.parseDouble(gc.getGeo_Longitude());
                    LatLng position = new LatLng(latitude, longitude);

                    if (mMap != null) {
                        Marker marker = mMap.addMarker(new MarkerOptions().position(position).title(gc.getEstabelecimento()));
                        marker.setTag(gc);
                    }

                    builder.include(position);
                }
            }
        } catch (Exception e) {
        }
    }

    private void centralizaCamera() {
        LatLngBounds bounds = builder.build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, MAP_PADDING_BOUNDS);
        mMap.animateCamera(cameraUpdate);
    }
}
