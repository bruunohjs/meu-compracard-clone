package br.com.comprocard.meucomprocard.Model.api;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PopupResponse implements Serializable {
    //     "MensagemPrincipal": "Vem aí um super App da Comprocard para você!",
    //    "TextoBotao": "Entendi",
    //    "ExecutaBotao": false,
    //    "LinkBotao": null,
    //    "LinkBackground": "https://www.comprocard.com.br/assets/img/bandeira.png?h=6883478ecfca96ea0a734cb9b7d4c63f"
    @SerializedName("MensagemPrincipal")
    private String mensagemPrincipal;
    @SerializedName("TextoBotao")
    private String textoBotao;
    @SerializedName("ExecutaBotao")
    private Boolean executaBotao;
    @SerializedName("LinkBotao")
    private String linkBotao;
    @SerializedName("LinkBackground")
    private String linkBackground;

    public String getMensagemPrincipal() {
        return mensagemPrincipal;
    }

    public void setMensagemPrincipal(String mensagemPrincipal) {
        this.mensagemPrincipal = mensagemPrincipal;
    }

    public String getTextoBotao() {
        return textoBotao;
    }

    public void setTextoBotao(String textoBotao) {
        this.textoBotao = textoBotao;
    }

    public Boolean getExecutaBotao() {
        return executaBotao;
    }

    public void setExecutaBotao(Boolean executaBotao) {
        this.executaBotao = executaBotao;
    }

    public String getLinkBotao() {
        return linkBotao;
    }

    public void setLinkBotao(String linkBotao) {
        this.linkBotao = linkBotao;
    }

    public String getLinkBackground() {
        return linkBackground;
    }

    public void setLinkBackground(String linkBackground) {
        this.linkBackground = linkBackground;
    }
}
