package br.com.comprocard.meucomprocard.DataSync;

/**
 * Created by Andre on 06/12/2016.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import br.com.comprocard.meucomprocard.Controller.PerfilCtrl;
import br.com.comprocard.meucomprocard.Model.Perfil;
import br.com.comprocard.meucomprocard.Util.Data;
import br.com.comprocard.meucomprocard.Util.Global;
import br.com.comprocard.meucomprocard.Util.MeuComproCardProgressDialog;
import br.com.comprocard.meucomprocard.dbHelper.ConexaoSQLite;

public class getCartaoExtrato extends AsyncTask<String, Void, resServer> {

    private ConexaoSQLite sqliteDataBase;
    private String _localURL;

    private Context mContext;
    private Data datas;
    protected ProgressDialog mProgressDialog;
    ResultsListener listener;

    public getCartaoExtrato(String _url, Context context) {
        this.mContext = context;
        this._localURL = _url;
        this.datas = new Data();
    }

    public void setOnResultsListener(ResultsListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mProgressDialog = MeuComproCardProgressDialog.ctor(mContext);
        mProgressDialog.show();
    }

    @Override
    protected void onPostExecute(resServer result) {
        if (mProgressDialog != null && mProgressDialog.isShowing() && mProgressDialog.getWindow() != null) {
            try {
                mProgressDialog.dismiss();
            } catch ( IllegalArgumentException ignore ) { ; }
        }
        listener.onResultsSucceeded(result);
        super.onPostExecute(result);
    }

    protected resServer doInBackground(final String... args) {
        Map<String, String> mapExtrato = new HashMap<String, String>();

        SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        GregorianCalendar gc = new GregorianCalendar();
        gc.add( Calendar.DATE, -90 );
        String DataHoraInicial = localSimpleDateFormat.format( gc.getTime() );
        gc.add( Calendar.DATE, + 91 );
        String DataHoraFinal = localSimpleDateFormat.format( gc.getTime() );
        mapExtrato.put("dataInicial", DataHoraInicial);
        mapExtrato.put("dataFinal", DataHoraFinal);

        String _jsonExtratoSync = new GsonBuilder().create().toJson(mapExtrato, Map.class);
        resServer _res;
        try {
            _res = uploadToServer( this._localURL, _jsonExtratoSync);
        } catch (IOException e) {
            e.printStackTrace();
            return new resServer();
        } catch (JSONException e) {
            e.printStackTrace();
            return new resServer();
        }
        return _res;
    }

    private resServer uploadToServer(String query, String json ) throws IOException, JSONException {
        ///JSONArray jsonArray = null;
        JSONObject _json = null;
        URL url = new URL(query);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        try {
            conn.setConnectTimeout(5000);
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", "Basic " + Global.getInstance().apiTokenCartao );
            conn.setRequestProperty("deviceId", Global.getInstance().apiDeviceID);

            OutputStream os = conn.getOutputStream();
            os.write(json.getBytes("UTF-8"));
            os.close();

            InputStream in = new BufferedInputStream(conn.getInputStream());
            byte[] bytes = new byte[1000];

            StringBuilder _reader = new StringBuilder();
            int numRead = 0;
            while ((numRead = in.read(bytes)) >= 0) {
                _reader.append(new String(bytes, 0, numRead));
            }
            _json     = new JSONObject(_reader.toString());
            //jsonArray = new JSONArray(_json. );
            in.close();
            conn.disconnect();

        } catch (Exception e) {
            e.getMessage();
        } finally {
            resServer _resServer = new resServer();
            _resServer.setHttpStatus(conn.getResponseCode());

            InputStream is = null;
            if (_resServer.getHttpStatus() != 200) {
                if (_resServer.getHttpStatus() > 200 && _resServer.getHttpStatus() < 400) {
                    is = conn.getInputStream();
                } else {
                    is = conn.getErrorStream();
                }

                try {
                    _json = new JSONObject(Global.getInstance().convertStreamToString(is));
                } catch (Exception e) {
                    _json = null;
                }

                if (null == _json) {
                    _json = new JSONObject("{\n" +
                            "  \"codigo\": -1,\n" +
                            "  \"mensagem\": \"Erro de autenticação.\"\n" +
                            "}");
                }
            }
            _resServer.setJsonBody( _json );
            return _resServer;
        }
    }

}
