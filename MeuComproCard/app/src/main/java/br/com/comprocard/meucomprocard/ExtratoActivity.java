package br.com.comprocard.meucomprocard;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.comprocard.meucomprocard.Adapters.DividerItemDecoration;
import br.com.comprocard.meucomprocard.Adapters.ExtratoAdapter;
import br.com.comprocard.meucomprocard.Controller.CartaoCtrl;
import br.com.comprocard.meucomprocard.Controller.HistoricoCtrl;
import br.com.comprocard.meucomprocard.Controller.PerfilCtrl;
import br.com.comprocard.meucomprocard.DataSync.ResultsListener;
import br.com.comprocard.meucomprocard.DataSync.getCartaoExtrato;
import br.com.comprocard.meucomprocard.DataSync.postAutentica;
import br.com.comprocard.meucomprocard.DataSync.postAutenticaGlobal;
import br.com.comprocard.meucomprocard.DataSync.postCartaoLogin;
import br.com.comprocard.meucomprocard.DataSync.postCartaoLoginWait;
import br.com.comprocard.meucomprocard.DataSync.resServer;
import br.com.comprocard.meucomprocard.Model.Cartao;
import br.com.comprocard.meucomprocard.Model.Historico;
import br.com.comprocard.meucomprocard.Model.Perfil;
import br.com.comprocard.meucomprocard.Util.Global;
import br.com.comprocard.meucomprocard.Util.MCrypt;
import br.com.comprocard.meucomprocard.dbHelper.ConexaoSQLite;

import static android.widget.Toast.LENGTH_LONG;

public class ExtratoActivity extends AppCompatActivity implements ResultsListener {

    public ArrayList<Historico> _extratoArray;
    public ArrayList<Historico> _extratoArraySorted;
    private ExtratoAdapter _extratoAdapter;
    private RecyclerView _extratoRecyclerView;
    private HistoricoCtrl _hisCTRL;
    private ConexaoSQLite _banco;
    private int _idCartao;
    private CartaoCtrl _carCTRL;
    private Cartao _cartao;
    private ImageButton _btnPesquisaVozCli;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_extrato);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Extrato do cartão");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        _banco = ConexaoSQLite.getInstancia(this);
        _hisCTRL = new HistoricoCtrl(_banco);
        Bundle extras = getIntent().getExtras();
        _idCartao = Integer.parseInt(extras.getString("ID_CARTAO"));

        _carCTRL = new CartaoCtrl(_banco);
        _cartao = _carCTRL.getCartaoCtrlByCartaoId(_idCartao);
        String _cartaoNumerofmt = _cartao.getCartaoNumFmt();
        TextView _txtNumCartao = (TextView) findViewById(R.id.txtNumCartaoftm);
        _txtNumCartao.setText(_cartaoNumerofmt);

        /*if ((null == Global.getInstance().apiTokenCartao) || (Global.getInstance().apiTokenCartao.equals(""))) {
            if (isConnected()) {
                try {
                    String _cartaoNumero = getCartaoDecript(String.valueOf(_cartao.getNumero()));
                    synchronized (new postCartaoLoginWait(Global.getInstance().apiURLLogin, _cartaoNumero, ExtratoActivity.this).execute().get()) {}
                } catch (Exception e) {

                }
            }
        }*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home ) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        cartaoCarregaLista( String.valueOf(_idCartao) );
        if (isConnected()) {
            getCartaoExtrato _taskgetExtrato = new getCartaoExtrato( Global.getInstance().apiURLHistoricos, ExtratoActivity.this );
            _taskgetExtrato.setOnResultsListener( ExtratoActivity.this );
            _taskgetExtrato.execute();
        }
    }

    private void cartaoCarregaLista( String _cartaoID ) {
        criarPopularHistoricoArray( _cartaoID );
        _extratoAdapter = new ExtratoAdapter(ExtratoActivity.this, _extratoArraySorted);
        _extratoRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_extrato);
        _extratoRecyclerView.setAdapter(_extratoAdapter);
        _extratoRecyclerView.setItemAnimator(new DefaultItemAnimator());
        _extratoRecyclerView.setLayoutManager(new LinearLayoutManager(ExtratoActivity.this));
    }

    private ArrayList<Historico> criarPopularHistoricoArray(String _cartaoID) {
        _extratoArray = null;
        _extratoArray = new ArrayList<Historico>();
        _extratoArray = _hisCTRL.getListaHistoricoCtrlByIDCartao( _cartaoID );

        Collections.sort(_extratoArray, new Comparator<Historico>() {
            DateFormat f = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
            @Override
            public int compare(Historico o1, Historico o2) {
                try {
                    return f.parse(o1.getDataHora()).compareTo(f.parse(o2.getDataHora()));
                } catch (ParseException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        });

        _extratoArraySorted = new ArrayList<Historico>(_extratoArray);
        Collections.reverse(_extratoArraySorted);
        return _extratoArraySorted;
    }

    public void fechar(View paramView) {
        finish();
    }

    public boolean isConnected()
    {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(meuCartaoActivity.CONNECTIVITY_SERVICE);
        NetworkInfo net = cm.getActiveNetworkInfo();
        if (net!=null && net.isAvailable() && net.isConnected()) {
            Global.getInstance().isConected = true;
            return true;
        } else {
            Global.getInstance().isConected = false;
            Toast.makeText(getApplicationContext(), "Por favor conecte-se a internet", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public void onResultsSucceeded(resServer result) {
        int httpStatus;
        JSONObject _json;
        JSONObject _jsonResp;
        JSONArray _jsonHistorico;
        String _mensagem = "";

        try {
            httpStatus = result.getHttpStatus();
            _jsonResp = result.getJsonBody();
            _jsonHistorico = _jsonResp.getJSONArray("historico");

            if (httpStatus == HttpURLConnection.HTTP_OK) {
                int n = _jsonHistorico.length();
                try {
                    _extratoArray = new ArrayList<Historico>();
                    for (int i = 0; i < n; i++) {
                        Historico _historico = new Historico();
                        _json = _jsonHistorico.getJSONObject(i);
                        _historico.setCartaoID(_idCartao);
                        _historico.setDataHora(_json.getString("dataFormatada"));
                        _historico.setDescricao(_json.getString("estabelecimento"));
                        _historico.setParcela(Integer.parseInt(_json.getString("parcelas")));
                        BigDecimal _valor = new BigDecimal(_json.getString("valor"));

                        if (_valor.signum() < 0){
                            if (_json.getString("estornada").equals("N"))
                                _valor = _valor.multiply(new BigDecimal("-1"));
                            _historico.setTipo("D");
                        } else {
                            _historico.setTipo("C");
                        }
                        _historico.setValor(_valor);
                        _extratoArray.add(_historico);
                    }
                } catch(JSONException e){
                        e.printStackTrace();
                }
                try {
                    _hisCTRL.excluirHistoricoCtrlByIDCartao( String.valueOf(_idCartao) );
                    for (int i = 0; i < _extratoArray.size(); i++) {
                        Historico _historico = new Historico();
                        _historico.setCartaoID(_idCartao);
                        _historico.setDataHora( _extratoArray.get(i).getDataHora() );
                        _historico.setDescricao( _extratoArray.get(i).getDescricao() );
                        _historico.setParcela( _extratoArray.get(i).getParcela() );
                        _historico.setValor( _extratoArray.get(i).getValor() );
                        _historico.setTipo( _extratoArray.get(i).getTipo() );
                        _hisCTRL.salvarHistoricoCtrl(_historico);
                    }
                    cartaoCarregaLista( String.valueOf(_idCartao) );
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                _mensagem = _jsonResp.getString("mensagem");
                if (_mensagem != "") {
                    Toast.makeText(ExtratoActivity.this, _mensagem, Toast.LENGTH_LONG).show();
                    finish();
                } else {
                    Toast.makeText(ExtratoActivity.this, "Tente novamente mais tarde!", Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        } catch (Exception e) {
            Toast.makeText(ExtratoActivity.this, "Falha na conexão de rede.", LENGTH_LONG).show(); //+ e.getMessage()
            System.gc();
            finish();
            return;
        }
    }


}
