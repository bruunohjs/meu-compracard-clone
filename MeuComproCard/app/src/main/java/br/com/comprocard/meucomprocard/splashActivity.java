package br.com.comprocard.meucomprocard;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.provider.SyncStateContract;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;

import com.daimajia.androidanimations.library.Techniques;
import com.viksaa.sssplash.lib.activity.AwesomeSplash;
import com.viksaa.sssplash.lib.cnst.Flags;
import com.viksaa.sssplash.lib.model.ConfigSplash;

import br.com.comprocard.meucomprocard.dbHelper.ConexaoSQLite;

public class splashActivity extends AwesomeSplash {

    @Override
    public void initSplash(ConfigSplash configSplash) {
        configSplash.setBackgroundColor(R.color.cinza);
        configSplash.setAnimCircularRevealDuration(400);
        configSplash.setRevealFlagX(Flags.REVEAL_RIGHT);
        configSplash.setRevealFlagY(Flags.REVEAL_BOTTOM);

        configSplash.setLogoSplash(R.drawable.logo_cc_m2);
        configSplash.setAnimLogoSplashDuration(300);
        configSplash.setAnimLogoSplashTechnique(Techniques.FadeInUp);

        final String CC_LOGO = "M 120.00,32.38" +
                               "C 116.22,32.38 106.09,23.02 103.02,20.27" +
                               "101.46,18.87 99.23,17.10 98.74,14.96" +
                               "97.92,11.38 105.16,2.69 108.17,1.02" +
                               "110.01,0.00 111.97,0.04 114.00,0.00" +
                               "124.80,-0.19 130.09,-2.03 137.70,7.00" +
                               "139.53,9.18 141.93,11.96 140.84,15.00" +
                               "139.74,18.05 132.67,23.54 130.00,26.00" ;

        //Customize Path
        /*configSplash.setPathSplash(CC_LOGO); //set path String
        configSplash.setOriginalHeight(57); //in relation to your svg (path) resource
        configSplash.setOriginalWidth(200); //in relation to your svg (path) resource
        configSplash.setAnimPathStrokeDrawingDuration(3000);
        configSplash.setPathSplashStrokeSize(3); //I advise value be <5
        configSplash.setPathSplashStrokeColor(R.color.ColorPrimaryDark); //any color you want form colors.xml
        configSplash.setAnimPathFillingDuration(3000);
        configSplash.setPathSplashFillColor(R.color.branco); //path object filling color
        */

        //Customize Title
        configSplash.setTitleSplash("O bem que nos move!");
        configSplash.setTitleTextColor(R.color.colorPrimary );
        configSplash.setTitleTextSize(25f);
        configSplash.setAnimTitleDuration(400);
        configSplash.setAnimTitleTechnique(Techniques.FlipInX);
    }

    @Override
    public void animationsFinished() {
        Intent intent;
//        intent = new Intent(splashActivity.this, mainComproCardActivity.class);
        intent = new Intent(splashActivity.this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        splashActivity.this.finish();
    }
}