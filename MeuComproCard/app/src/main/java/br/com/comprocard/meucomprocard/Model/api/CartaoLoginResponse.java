package br.com.comprocard.meucomprocard.Model.api;

import java.io.Serializable;

public class CartaoLoginResponse implements Serializable {
    CartaoApi cartao;
    private String token;
    private String dataExpiracao;

    public CartaoApi getCartao() {
        return cartao;
    }

    public void setCartao(CartaoApi cartao) {
        this.cartao = cartao;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDataExpiracao() {
        return dataExpiracao;
    }

    public void setDataExpiracao(String dataExpiracao) {
        this.dataExpiracao = dataExpiracao;
    }
}
