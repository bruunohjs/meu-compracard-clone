package br.com.comprocard.meucomprocard.Util;


import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.media.Image;
import android.os.Build;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ListAdapter;
import android.widget.ListView;

import android.util.DisplayMetrics;

public class uiUtils {
    private static uiUtils mInstance = null;

    public static synchronized uiUtils getInstance() {
        if (null == mInstance) {
            mInstance = new uiUtils();
        }
        return mInstance;
    }

    public static boolean setListViewHeightBasedOnItems(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();

            return true;

        } else {
            return false;
        }

    }

    public static String replaceNull(String input) {
        return input == null ? "" : input;
    }

    public static String replaceNullToStr0(String input) {
        return input == null ? "0" : input;
    }

    public static String replaceNothingToStr0(String input) {
        String retorno = "0";
        if ( input != null ) {
            if (input.toString().length() > 0) {
                retorno = input;
            }
        }
        return retorno;
    }

    public static String setdoubleQuote(String myText) {
        String quoteText = "";
        //if (!myText.isEmpty()) {
        quoteText = "\"" + myText + "\"";
        //}
        return quoteText;
    }

    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    public static String getPhoneNumberWinthoutMask( String telNumber ) {
        return telNumber.replaceAll("[.]", "").replaceAll("[-]", "").replaceAll("[/]", "").replaceAll("[(]", "").replaceAll("[)]", "").replaceAll(" ", "");
    }

    public static Bitmap base64ToBitmap(String b64) {
        byte[] imageAsBytes = Base64.decode(b64.getBytes(), Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
    }
}