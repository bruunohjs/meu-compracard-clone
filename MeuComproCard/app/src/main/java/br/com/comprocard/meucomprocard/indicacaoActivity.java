package br.com.comprocard.meucomprocard;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import br.com.comprocard.meucomprocard.Controller.PerfilCtrl;
import br.com.comprocard.meucomprocard.DAO.indicacaoDAO;
import br.com.comprocard.meucomprocard.DataSync.ResultsListener;
import br.com.comprocard.meucomprocard.DataSync.postAccountIndicacao;
import br.com.comprocard.meucomprocard.DataSync.resServer;
import br.com.comprocard.meucomprocard.Model.Indicacao;
import br.com.comprocard.meucomprocard.Model.Perfil;
import br.com.comprocard.meucomprocard.Util.GPSTracker;
import br.com.comprocard.meucomprocard.Util.Global;
import br.com.comprocard.meucomprocard.Util.Mascara;
import br.com.comprocard.meucomprocard.dbHelper.ConexaoSQLite;

public class indicacaoActivity extends AppCompatActivity implements ResultsListener  {

    private ConexaoSQLite _banco;
    private TextWatcher _Mask;
    private EditText _edtNome;
    private EditText _edtEmail;
    private EditText _edtNomeContato;
    private EditText _edtTelefone;
    private TextView _btnEnviar;
    double latitude;
    double longitude;
    private final int MY_PERMISSIONS_REQUEST = 1;
    InputMethodManager imm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_indicacao);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Indicar Estabelecimento");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        _banco = ConexaoSQLite.getInstancia(this);
        _edtNome = (EditText) findViewById(R.id.edtNomeInd);
        _edtNomeContato = (EditText) findViewById(R.id.edtNomeContatoInd);
        _edtEmail = (EditText) findViewById(R.id.edtEmailInd);
        _edtTelefone = (EditText) findViewById(R.id.edtTelefoneInd);
        _edtTelefone.addTextChangedListener(Mascara.insert("(##)#####-####", _edtTelefone));
        _btnEnviar = (TextView) findViewById(R.id.btnEnviar);
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (!isGPSAtivado()) return;
    }

    private boolean isGPSAtivado() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET}, 10);
            }
            return false;
        } else {
            final LocationManager manager = (LocationManager) indicacaoActivity.this.getSystemService(Context.LOCATION_SERVICE);
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
                LayoutInflater inflater = (LayoutInflater)indicacaoActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View titleView = inflater.inflate(R.layout.custom_dialog, null);
                ((TextView) titleView.findViewById(R.id.partName)).setText(" Atenção");
                builder.setCustomTitle(titleView);
                builder.setMessage("O GPS não está ativo, é necessário ativar para continuar, ativar agora?");
                builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        int RESULT_GPS = 0;
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivityForResult(intent, RESULT_GPS);
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                android.support.v7.app.AlertDialog alert = builder.create();
                alert.show();
            } else {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home ) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private boolean validaFormulario() {

        if ( null != this._edtNome.getText().toString() ) {
            if (this._edtNome.getText().toString().trim().length() == 0) {
                if (imm != null) {
                    imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }
                Toast.makeText(getApplicationContext(), "Campo [Nome] deve ser preenchido!", Toast.LENGTH_LONG).show();
                this._edtNome.requestFocus();
                return false;
            }
        } else {
            if (imm != null) {
                imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
            }

            Toast.makeText(getApplicationContext(), "Campo [Nome] deve ser preenchido!", Toast.LENGTH_LONG).show();
            this._edtNome.requestFocus();
            return false;
        }

        return true;
    }

    private Indicacao getDadosIndicacaoFromFormulario() {
        Indicacao _indicacao = new Indicacao();
        _indicacao.setNome( _edtNome.getText().toString() );
        _indicacao.setNomeContato( _edtNomeContato.getText().toString() );
        _indicacao.setTelefone( _edtTelefone.getText().toString() );
        _indicacao.setEmailContato( _edtEmail.getText().toString() );
        return _indicacao;
    }

    public void putIndicacao(View paramView) {
        if (isGPSAtivado()) {
            GPSTracker gps = new GPSTracker(indicacaoActivity.this);
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
               /*try {
                //latitude = location.getLatitude();
                //longitude = location.getLongitude();
                latitude = gps.getLatitude();
                longitude = gps.getLongitude();

                Toast.makeText(getApplicationContext(), "LAT:" + String.valueOf(latitude) + " LOG:" + String.valueOf(longitude), Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Erro ao tentar ler o GPS", Toast.LENGTH_LONG).show();
            }*/
            if (validaFormulario()) {
                try {
                    Indicacao _indicacaoPreenchida = getDadosIndicacaoFromFormulario();
                    PerfilCtrl _perCTRL = new PerfilCtrl(_banco);
                    Perfil _perfil = _perCTRL.getPerfilCtrl(1);
                    _indicacaoPreenchida.setCPF(_perfil.getCPF().toString());
                    //GPSTracker gps = new GPSTracker(indicacaoActivity.this);
                    //latitude = location.getLatitude(); //gps.getLatitude();
                    //longitude = location.getLongitude(); //gps.getLongitude();
                    _indicacaoPreenchida.setLatitude(String.valueOf(latitude));
                    _indicacaoPreenchida.setLongitude(String.valueOf(longitude));
                    indicacaoDAO _dao = new indicacaoDAO(_banco);
                    _dao.salvarIndicacaoDAO(_indicacaoPreenchida);
                    postAccountIndicacao _taskPostIndicacao = new postAccountIndicacao(Global.getInstance().apiURLIndicacao, _indicacaoPreenchida, indicacaoActivity.this);
                    //postAccountIndicacao _taskPostIndicacao = new postAccountIndicacao(Global.getInstance().apiURLIndicacao, _indicacaoPreenchida, getActivity() );
                    _taskPostIndicacao.setOnResultsListener(this);
                    _taskPostIndicacao.execute();
                } catch (Exception e) {
                    Log.d("Erro", e.getMessage());
                }
            }
        }
   }

    public void onResultsSucceeded(resServer result) {
        if ( result.getHttpStatus() == 200) {
            Toast.makeText(getApplicationContext(), "Agradecemos sua indicação!", Toast.LENGTH_LONG).show();
            finish();
        } else {
            Toast.makeText(getApplicationContext(), "Não foi possível enviar indicação, tente novamente mais tarde!", Toast.LENGTH_LONG).show();
        }
    }
}
