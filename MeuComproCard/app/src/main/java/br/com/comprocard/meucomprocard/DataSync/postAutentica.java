package br.com.comprocard.meucomprocard.DataSync;

/**
 * Created by Andre on 06/12/2016.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import br.com.comprocard.meucomprocard.Controller.CartaoCtrl;
import br.com.comprocard.meucomprocard.Model.Cartao;
import br.com.comprocard.meucomprocard.Model.Perfil;
import br.com.comprocard.meucomprocard.R;
import br.com.comprocard.meucomprocard.Util.Data;
import br.com.comprocard.meucomprocard.Util.Global;
import br.com.comprocard.meucomprocard.Util.MeuComproCardProgressDialog;
import br.com.comprocard.meucomprocard.dbHelper.ConexaoSQLite;

public class postAutentica extends AsyncTask<String, Void, resServer> {

    private ConexaoSQLite sqliteDataBase;
    private String _localURL;
    private Perfil _perfilPost;

    private Context mContext;
    protected ProgressDialog mProgressDialog;
    ResultsListener listener;
    private Boolean _showProgress;

    public postAutentica(String _url, Perfil _perfil, Context context, Boolean showProgress) {
        this.mContext = context;
        this._localURL = _url;
        this._perfilPost = _perfil;
        this._showProgress = showProgress;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (this._showProgress) {
            mProgressDialog = MeuComproCardProgressDialog.ctor(mContext);
            mProgressDialog.show();
        }
    }

    @Override
    protected void onPostExecute(resServer result) {
        if (mProgressDialog != null && mProgressDialog.isShowing() && mProgressDialog.getWindow() != null) {
            try {
                mProgressDialog.hide();
            } catch ( IllegalArgumentException ignore ) { ; }
        }
        listener.onResultsSucceeded(result);
        super.onPostExecute(result);
    }

    public void setOnResultsListener(ResultsListener listener) {
        this.listener = listener;
    }

    protected resServer doInBackground(final String... args) {
        Map<String, String> mapPerfil = new HashMap<String, String>();
        mapPerfil.put("deviceId", String.valueOf(_perfilPost.getIMEI()));
        mapPerfil.put("cpf", String.valueOf(_perfilPost.getCPF().replace(".","").replace("-","").replace("/","")));

        String _jsonAutenticacao = new GsonBuilder().create().toJson(mapPerfil, Map.class);
        resServer _res;
        try {
            _res = uploadToServer( Global.getInstance().apiURLAutenticacao, _jsonAutenticacao);
            try { Thread.sleep(200); }
            catch (InterruptedException e) { e.printStackTrace(); }
        } catch (IOException e) {
            e.printStackTrace();
            return new resServer();
        } catch (JSONException e) {
            e.printStackTrace();
            return new resServer();
        }
        return _res;
    }

    private resServer uploadToServer(String query, String json ) throws IOException, JSONException {
        JSONObject jsonObj = null;
        URL url = new URL(query);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        try {
            conn.setConnectTimeout(5000);
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");

            OutputStream os = conn.getOutputStream();
            os.write(json.getBytes("UTF-8"));
            os.close();

            InputStream in = new BufferedInputStream(conn.getInputStream());
            byte[] bytes = new byte[1000];

            StringBuilder _reader = new StringBuilder();
            int numRead = 0;
            while ((numRead = in.read(bytes)) >= 0) {
                _reader.append(new String(bytes, 0, numRead));
            }
            jsonObj = new JSONObject(_reader.toString());
            in.close();
            conn.disconnect();

        } catch (Exception e) {
            e.getMessage();
        } finally {
            resServer _resServer = new resServer();
            _resServer.setHttpStatus(conn.getResponseCode());

            InputStream is = null;
            if (_resServer.getHttpStatus() != 200) {
                if (_resServer.getHttpStatus() > 200 && _resServer.getHttpStatus() < 400) {
                    is = conn.getInputStream();
                } else {
                    is = conn.getErrorStream();
                }

                try {
                    jsonObj = new JSONObject(Global.getInstance().convertStreamToString(is));
                } catch (Exception e) {
                    jsonObj = null;
                }

                if (null == jsonObj) {
                    jsonObj = new JSONObject("{\n" +
                            "  \"codigo\": -1,\n" +
                            "  \"mensagem\": \"Erro de autenticação.\"\n" +
                            "}");
                }
            }
            _resServer.setJsonBody( jsonObj );
            return _resServer;
        }
    }

}
