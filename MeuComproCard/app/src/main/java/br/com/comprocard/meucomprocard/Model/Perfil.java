package br.com.comprocard.meucomprocard.Model;

/**
 * Created by andre.cardoso on 25/10/2016.
 */
public class Perfil {

    private int ID;
    private String Nome;
    private String Email;
    private String Telefone;
    private String CPF;
    private String DataNascimento;
    private String IMEI;
    private String Senha;
    private int Ativo;
    private String Token;
    private String DataHora;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getCPF() {
        return CPF;
    }

    public void setCPF(String CPF) {
        this.CPF = CPF;
    }

    public String getDataNascimento() {
        return DataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        DataNascimento = dataNascimento;
    }

    public String getIMEI() {
        return IMEI;
    }

    public void setIMEI(String IMEI) {
        this.IMEI = IMEI;
    }

    public String getSenha() {
        return Senha;
    }

    public void setSenha(String senha) {
        Senha = senha;
    }

    public int getAtivo() {
        return Ativo;
    }

    public void setAtivo(int ativo) {
        Ativo = ativo;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public String getTelefone() {
        return Telefone;
    }

    public void setTelefone(String telefone) {
        Telefone = telefone;
    }

    public String getDataHora() {
        return DataHora;
    }

    public void setDataHora(String dataHora) {
        DataHora = dataHora;
    }

    @Override
    public String toString() {
        return "Perfil [ID="+ID+", Nome="+Nome+", Email="+Email+", CPF="+CPF+", DataNascimento="+DataNascimento+", IMEI="+IMEI+", Senha="+Senha+", Ativo="+Ativo+", Token="+Token+"]";
    }

}
