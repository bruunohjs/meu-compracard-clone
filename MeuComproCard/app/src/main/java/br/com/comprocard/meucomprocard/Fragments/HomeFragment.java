package br.com.comprocard.meucomprocard.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ViewListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import br.com.comprocard.meucomprocard.Adapters.api.CartaoAdapter;
import br.com.comprocard.meucomprocard.Controller.CartaoCtrl;
import br.com.comprocard.meucomprocard.Controller.PerfilCtrl;
import br.com.comprocard.meucomprocard.DataSync.ResultsListener;
import br.com.comprocard.meucomprocard.DataSync.ResultsListenerRefresh;
import br.com.comprocard.meucomprocard.DataSync.getImagemCartao;
import br.com.comprocard.meucomprocard.DataSync.postAutentica;
import br.com.comprocard.meucomprocard.DataSync.postCartaoSaldos;
import br.com.comprocard.meucomprocard.DataSync.postSendToken;
import br.com.comprocard.meucomprocard.DataSync.resServer;
import br.com.comprocard.meucomprocard.Model.Cartao;
import br.com.comprocard.meucomprocard.Model.Perfil;
import br.com.comprocard.meucomprocard.Model.TokenComproCard;
import br.com.comprocard.meucomprocard.Model.api.AuthRequest;
import br.com.comprocard.meucomprocard.Model.api.AuthResponse;
import br.com.comprocard.meucomprocard.Model.api.PopupResponse;
import br.com.comprocard.meucomprocard.Model.api.Publicidade;
import br.com.comprocard.meucomprocard.Model.api.PublicidadeResponse;
import br.com.comprocard.meucomprocard.R;
import br.com.comprocard.meucomprocard.Util.CirclePageIndicatorDecoration;
import br.com.comprocard.meucomprocard.Util.Global;
import br.com.comprocard.meucomprocard.Util.ImagensCartaoHelper;
import br.com.comprocard.meucomprocard.Util.MCrypt;
import br.com.comprocard.meucomprocard.config.RetrofitConfig;
import br.com.comprocard.meucomprocard.dbHelper.ConexaoSQLite;
import br.com.comprocard.meucomprocard.interfaces.CartaoService;
import br.com.comprocard.meucomprocard.interfaces.PopupService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_LONG;

public class HomeFragment extends Fragment implements ResultsListener, ResultsListenerRefresh {

    private String token;

    ConexaoSQLite _banco;

    public List<Cartao> _cartoesArray;
    private CartaoAdapter _cartoesAdapter;
    private RecyclerView _cartaoRecyclerView;

    private CartaoCtrl _carCTRL;
    private PerfilCtrl _perCTRL;
    private Perfil _perfil;

    TextView tvAviso;

    public List<Publicidade> _bannersArray;
    private CarouselView _carouselViewBanners;
    private ProgressBar progressBar;

    private OnRefreshMenuItem onRefreshMenuItem;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnRefreshMenuItem) {
            onRefreshMenuItem = (OnRefreshMenuItem) context;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_home, container, false);
        tvAviso = view.findViewById(R.id.tv_aviso);
        _cartaoRecyclerView = view.findViewById(R.id.recycler_view_cartoes);
        progressBar = view.findViewById(R.id.progress);

        token = FirebaseInstanceId.getInstance().getToken();

        _banco = ConexaoSQLite.getInstancia(getActivity());
        SQLiteDatabase _db = _banco.getWritableDatabase();
        _banco.onUpgrade(_db, 1, 2);

        _carCTRL = new CartaoCtrl(_banco);
        _perCTRL = new PerfilCtrl(_banco);

        _perfil = _perCTRL.getPerfilCtrl(1);

        if ((null == Global.getInstance().apiTokenPerfil)
                || (Global.getInstance().apiTokenPerfil.equals(""))) {
            cartaoCarregaLista();
            refreshSaldoCartaoByPerfil(0);
            getBanners(view);
        } else {
            autenticaUsuario(view);
        }
//
//        swipeRefresh = view.findViewById(R.id.swipe_refresh);
//        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                cartaoCarregaLista();
//                refreshSaldoCartaoByPerfil(0);
//                getBanners(view);
//
//                swipeRefresh.setRefreshing(false);
//            }
//        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        isConnected();
        refreshSaldoCartaoByPerfil(0);
    }

    public void setupAlertDialog(final PopupResponse popupResponse) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.popup, null);

        final AlertDialog alertDialog = new AlertDialog.Builder(requireContext())
                .setView(popupView)
                .create();

//        alertDialog.setContentView();

        TextView tvMensagemPrincipal = popupView.findViewById(R.id.tv_mensagem);
        tvMensagemPrincipal.setText(popupResponse.getMensagemPrincipal());
        ImageView img = popupView.findViewById(R.id.popup_imageview);

        FrameLayout frame = popupView.findViewById(R.id.frame);
        frame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        TextView button = popupView.findViewById(R.id.btn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (popupResponse.getExecutaBotao()
                        && popupResponse.getLinkBotao() != null) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(popupResponse.getLinkBotao()));
                    startActivity(i);
                } else {
                    alertDialog.dismiss();
                }
            }
        });

        try {
            Picasso.with(getActivity()).
                    load(popupResponse.getLinkBackground()).
                    placeholder(R.drawable.banner_default).
                    fit().
                    into(img);

        } catch (Exception e) {
        }

        alertDialog.show();
        SharedPreferences preferences = getContext().getSharedPreferences("popup", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("popUpShowed", true);
        editor.apply();

    }

//    private void getDadosCartao(List<br.com.comprocard.meucomprocard.Model.Cartao> cartoes) {
//        CartaoService service = RetrofitConfig.getInstance()
//                .create(CartaoService.class);
//
//        final CartaoLoginRequest request = new CartaoLoginRequest();
//        request.setNumeroCartao("0");
//
//        String token = "Basic " + Global.getInstance().apiTokenPerfil;
//        String deviceId = Global.getInstance().apiDeviceID;
//
//        Call<CartaoLoginResponse> callCartaoLogin = service.logarCartao(token, deviceId, request);
//        callCartaoLogin.enqueue(new Callback<CartaoLoginResponse>() {
//            @Override
//            public void onResponse(Call<CartaoLoginResponse> call, Response<CartaoLoginResponse> response) {
//                CartaoLoginResponse cartaoLogin = response.body();
//
//                // convert retorno no model
//
//                _cartoesArray.add( parseResultCartao( cartaoLogin ) );
//                _cartaoAdapter.notifyDataSetChanged();
//            }
//
//            @Override
//            public void onFailure(Call<CartaoLoginResponse> call, Throwable t) {
//
//            }
//        });
//    }

    private String getCartaoDecript(String _numeroCartaoCrip) {
        String result = "";
        try {
            MCrypt mcrypt = new MCrypt();
            result = new String(mcrypt.decrypt(_numeroCartaoCrip));
        } catch (Exception e) {
            Log.d("Erro", e.getMessage());
        }
        return result;
    }

//    private void getCartoes() {
//
//        CartaoService service = RetrofitConfig.getInstance()
//                .create(CartaoService.class);
//
//        final CartaoSaldosRequest request = new CartaoSaldosRequest();
//
//        List<CartaoSaldo> cartaoSaldoList = new ArrayList<>();
//
//        for( Cartao cartao : _carCTRL.getListaCartaoCtrl()) {
//            cartaoSaldoList.add(
//                    new CartaoSaldo( getCartaoDecript(cartao.getNumero()).trim() )
//            );
//        }
//
//        request.setCartaoSaldos( cartaoSaldoList );
//
//        String token = "Basic " + Global.getInstance().apiTokenPerfil;
//        String deviceId = Global.getInstance().apiDeviceID;
//
//        Call<List<CartaoApi>> callCartaoLogin = service.getCartoes(token, deviceId, request);
//        callCartaoLogin.enqueue(new Callback<List<CartaoApi>>() {
//            @Override
//            public void onResponse(Call<List<CartaoApi>> call, Response<List<CartaoApi>> response) {
//
//                if ( response.isSuccessful() ){
//                    if ( response.body().size() > 0 ){
//
//                        for( CartaoApi api : response.body() ){
//
//                            Cartao _cartaoServer = new Cartao();
//
//                            _cartaoServer.setCartaoId( api.getCartaoId() );
//                            _cartaoServer.setBandeira("ComproCard");
//                            _cartaoServer.setProduto( api.getProdutoDesc() );
//                            _cartaoServer.setEmpresa( api.getEmpresaNome() );
//                            _cartaoServer.setNome( api.getCartaoNome() );
//                            _cartaoServer.setNumero(getCartaoCript( api.getCartaoNome()));
//                            _cartaoServer.setCartaoNumFmt( api.getCartaoNumFmt() );
//                            _cartaoServer.setUltimaConsulta( api.getDataUltimaConsulta() );
//                            _cartaoServer.setValidade("31/05/2095");
//                            _cartaoServer.setCVV("999");
//                            _cartaoServer.setSaldo(new BigDecimal( api.getCartaoSaldo() ));
//                            _cartaoServer.setLimite(new BigDecimal( api.getCartaoLimite() ));
//                            _cartaoServer.setDiaVencimento(api.getCartaoVencto());
//                            _cartaoServer.setSituacao( api.getSituacaoDescr() );
//                            _cartaoServer.setSenha( api.isExigeSenha() ? "true" : "false");
//                            _cartaoServer.setToken(Global.getInstance().apiTokenCartao);
//                            _cartaoServer.setUltimaConsulta( api.getDataUltimaConsulta() );
//
//                            _cartoesArray.add( _cartaoServer );
//
////                            _cartaoAdapter.notifyDataSetChanged();
//                            _cartoesAdapter.notifyDataSetChanged();
//                        }
//
//                    }else{
//                        Toast.makeText(getActivity(), "Nenhum cartão cadastrado", Toast.LENGTH_SHORT).show();
//                    }
//                }else{
//                    Toast.makeText(getActivity(), "Erro ao recuperar cartoes " + response.message(), Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<CartaoApi>> call, Throwable t) {
//
//            }
//        });
//    }

    private void getBanners(final View view) {
        CartaoService service = RetrofitConfig.getInstance()
                .create(CartaoService.class);

//        String url = "http://comprocard-publicidade-api.azurewebsites.net/v1/PublicidadeServices/GetUrlPublicacoesAtivasByModulo?identificacaoModulo=ACFDF91C-013A-443E-BC02-1A0754B581EC";
        String url = "https://comprocard-publicidade-api.azurewebsites.net/v1/PublicidadeServices/GetUrlPublicacoesAtivasByModulo?identificacaoModulo=7228CF94-1AD5-4DF7-95A7-E80A18980364";

        Call<PublicidadeResponse> callBanners = service.getBanners(url);
        callBanners.enqueue(new Callback<PublicidadeResponse>() {
            @Override
            public void onResponse(Call<PublicidadeResponse> call, Response<PublicidadeResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getData().size() > 0) {

                        _bannersArray = new ArrayList<>();

                        for (Publicidade p : response.body().getData()) {
                            _bannersArray.add(p);
//                            _bannerAdapter.notifyDataSetChanged();
                        }

                        bannerCarregaLista(view);
                    }
                } else {
                    Toast.makeText(getActivity(), "Erro ao recuperar banners", Toast.LENGTH_SHORT)
                            .show();
                }
            }

            @Override
            public void onFailure(Call<PublicidadeResponse> call, Throwable t) {

            }
        });
    }

    private void getPopUp() {
        String token = Global.getInstance().apiTokenPerfil;
        fetchPopup(token);
    }

    private void fetchPopup(String token) {
        PopupService service = RetrofitConfig.getInstance()
                .create(PopupService.class);

        String url = "https://sistemas.comprocard.com.br/meu-comprocard-api/api/novidades";

        String deviceId = Global.getInstance().apiDeviceID;
        String beareToken = "Bearer " + token;
        Call<PopupResponse> callResponse = service.getPopup(url, beareToken, deviceId);
        callResponse.enqueue(new Callback<PopupResponse>() {
            @Override
            public void onResponse(Call<PopupResponse> call, Response<PopupResponse> response) {
                if (response.isSuccessful()
                ) {
                    SharedPreferences preferences = getContext().getSharedPreferences("popup", Context.MODE_PRIVATE);
                    if (preferences.contains("popUpShowed")
                            && !preferences.getBoolean("popUpShowed", false)) {
                        PopupResponse popupResponse = response.body();
//                        setupAlertDialog(popupResponse);
                    }
                }
            }

            @Override
            public void onFailure(Call<PopupResponse> call, Throwable t) {
            }
        });
    }

    private void verificaTokenPerfil(View view) {

        if ((null == Global.getInstance().apiTokenPerfil)
                || (Global.getInstance().apiTokenPerfil.equals(""))) {
            autenticaUsuario(view);
        }

    }

    private void autenticaUsuario(final View view) {
        CartaoService service = RetrofitConfig.getInstance()
                .create(CartaoService.class);

        String cpf = _perfil.getCPF();

        String deviceId = Global.getInstance().apiDeviceID;

        AuthRequest request = new AuthRequest();
        request.setCpf(cpf);
        request.setDeviceId(deviceId);

        Call<AuthResponse> callAutentica = service.autenticar(token, request);
        callAutentica.enqueue(new Callback<AuthResponse>() {
            @Override
            public void onResponse(Call<AuthResponse> call, Response<AuthResponse> response) {

                if (response.isSuccessful()) {
                    AuthResponse authResponse = response.body();
                    Global.getInstance().apiTokenPerfil = authResponse.getToken();
//                    cartaoCarregaLista(view);
                    cartaoCarregaLista();
                    getBanners(view);
                    getPopUp();
                } else {
                    verificaTokenPerfil(view);
                    Toast.makeText(getActivity(), "Erro ao realizar autenticação!", Toast.LENGTH_SHORT)
                            .show();
                }
            }

            @Override
            public void onFailure(Call<AuthResponse> call, Throwable t) {

            }
        });
    }

    public void atualizarCartao(br.com.comprocard.meucomprocard.Model.Cartao _cartao, String cartaoID) {
        CartaoCtrl _cartaoCTRL = new CartaoCtrl(_banco);
        _cartaoCTRL.salvarCartaoCtrl(_cartao, cartaoID);
    }

    public void carregaLista() {
        cartaoCarregaLista();
    }

    private void cartaoCarregaLista() {
        criarPopularprodutosArray();

        _cartaoRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        _cartoesAdapter = new CartaoAdapter(getActivity(), _cartoesArray);
        _cartaoRecyclerView.setAdapter(_cartoesAdapter);
        _cartaoRecyclerView.setHasFixedSize(true);

        if (_cartaoRecyclerView.getOnFlingListener() != null) {
            SnapHelper snapHelper = new LinearSnapHelper();
            snapHelper.attachToRecyclerView(_cartaoRecyclerView);
        }

        if (!(_cartaoRecyclerView.getItemDecorationCount() > 0)) {
            CirclePageIndicatorDecoration circlePageIndicatorDecoration = new CirclePageIndicatorDecoration();
            _cartaoRecyclerView.addItemDecoration(circlePageIndicatorDecoration);
        }

    }

    private void bannerCarregaLista(View view) {
//        criarPopularprodutosArray();
//        _bannersArray = new ArrayList<>();
//        _bannerAdapter = new BannerAdapter(getActivity(), _bannersArray);
//        _bannerRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_banners);
//        _bannerRecyclerView.setAdapter(_bannerAdapter);
//        _bannerRecyclerView.setItemAnimator(new DefaultItemAnimator());
//        _bannerRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        _carouselViewBanners = view.findViewById(R.id.carousel_view_banners);
        _carouselViewBanners.setViewListener(viewListener);
        _carouselViewBanners.setPageCount(_bannersArray.size());


//        getBanners();
    }

    ViewListener viewListener = new ViewListener() {

        @Override
        public View setViewForPosition(int position) {
            View customView = getLayoutInflater().inflate(R.layout.banner_item, null, false);

            Publicidade _banner = _bannersArray.get(position);

            //set view attributes here
            ImageView _banner_pic = (ImageView) customView.findViewById(R.id.banner_photo);

            try {
                Picasso.with(getActivity()).
                        load(_banner.getPublicidadeImagemUrlMobile()).
                        placeholder(R.drawable.banner_default).
                        fit().
                        into(_banner_pic);

            } catch (Exception e) {
            }

            return customView;
        }
    };

    private List<Cartao> criarPopularprodutosArray() {
        _cartoesArray = null;
        _cartoesArray = new ArrayList<Cartao>();
        _cartoesArray = _carCTRL.getListaCartaoCtrl();

        return _cartoesArray;
    }

    private String getCartaoCript(String _numeroCartao) {
        String encrypted = "";
        try {
            MCrypt mcrypt = new MCrypt();
            encrypted = mcrypt.bytesToHex(mcrypt.encrypt(_numeroCartao));
        } catch (Exception e) {
            Log.d("Erro", e.getMessage());
        }
        return encrypted;
    }

    // Apagar tudo

    public void refreshSaldoCartaoByPerfil(int isManual) {
        if (isConnected() == true) {
            cartaoCarregaLista();
            onRefreshMenuItem.startLoadingSaldo();

            if (_perfil.getID() != 1) {
                _perfil = _perCTRL.getPerfilCtrl(1);
                Global.getInstance().apiDeviceID = _perfil.getIMEI();
                //Global.getInstance().apiTokenPerfil = _perfil.getToken();
            }

            if (_carCTRL.getListaCartaoCtrl().size() > 0) {
                Global.getInstance().apiDeviceID = _perfil.getIMEI();
                //Global.getInstance().apiTokenPerfil = _perfil.getToken();
                postAutentica _taskAutentica = new postAutentica(Global.getInstance().apiURLAutenticacao, _perfil, getActivity(), false);
                _taskAutentica.setOnResultsListener(this);
                _taskAutentica.execute();

                tvAviso.setVisibility(View.GONE);
                _cartaoRecyclerView.setVisibility(View.VISIBLE);

            } else {

                tvAviso.setVisibility(View.VISIBLE);
                _cartaoRecyclerView.setVisibility(View.GONE);

                onRefreshMenuItem.stopLoadingSaldo();

                if (isManual == 1) {
                    Toast.makeText(getActivity(), "Favor inserir um cartão para consultar seu saldo!", Toast.LENGTH_LONG).show();
                }

            }
        } else {
            cartaoCarregaLista();

            onRefreshMenuItem.stopLoadingSaldo();

            if (_carCTRL.getListaCartaoCtrl().size() > 0) {

                tvAviso.setVisibility(View.GONE);
                _cartaoRecyclerView.setVisibility(View.VISIBLE);
            } else {

                tvAviso.setVisibility(View.VISIBLE);
                _cartaoRecyclerView.setVisibility(View.GONE);
            }
        }

    }

    public void onResultsSucceededRefresh(resServer result) {
        int httpStatus;
        JSONArray _jsonArray;
        JSONObject _json;
        JSONObject _jsonResp;
        String _mensagem = "";
        ImagensCartaoHelper _imgHelper = new ImagensCartaoHelper();

        try {
            httpStatus = result.getHttpStatus();
            _jsonArray = result.getJsonArray();
            _jsonResp = result.getJsonBody();

            if (httpStatus == HttpURLConnection.HTTP_OK) {

                _cartoesArray = new ArrayList<>();

                int n = _jsonArray.length();

                for (int i = 0; i < n; i++) {

                    Cartao _cartaoServer = new Cartao();

                    _json = _jsonArray.getJSONObject(i);
                    _cartaoServer.setCartaoId(Integer.parseInt(_json.getString("CartaoId")));
                    _cartaoServer.setBandeira("ComproCard");
                    _cartaoServer.setProduto(_json.getString("ProdutoDesc"));
                    _cartaoServer.setEmpresa(_json.getString("EmpresaNome"));
                    _cartaoServer.setNome(_json.getString("CartaoNome"));
                    _cartaoServer.setNumero(getCartaoCript(_json.getString("CartaoNumero").toString()));
                    _cartaoServer.setCartaoNumFmt(_json.getString("CartaoNumFmt").toString());
                    _cartaoServer.setUltimaConsulta(_json.getString("dataUltimaConsulta"));
                    _cartaoServer.setValidade("31/05/2095");
                    _cartaoServer.setCVV("999");
                    _cartaoServer.setSaldo(new BigDecimal(_json.getString("CartaoSaldo")));
                    _cartaoServer.setLimite(new BigDecimal(_json.getString("CartaoLimite")));
                    _cartaoServer.setDiaVencimento(Integer.parseInt(_json.getString("CartaoVencto")));
                    _cartaoServer.setSituacao(_json.getString("SituacaoDescr"));
                    _cartaoServer.setSenha(_json.getString("exigeSenha"));
                    _cartaoServer.setToken(Global.getInstance().apiTokenCartao);
                    _cartaoServer.setUltimaConsulta(_json.getString("dataUltimaConsulta"));

                    CartaoCtrl _cartaoControler = new CartaoCtrl(_banco);
                    Cartao _cartao = _cartaoControler.getCartaoCtrlByCartaoId(Integer.parseInt(_json.getString("CartaoId")));

                    if (null == _cartao.getVersaoImagemCartao() || _cartao.getVersaoImagemCartao().equals("")) {

                        try {
                            sendPushToken();
                        } catch (Exception e) {
                        }

                        String URLbase = _json.getString("ImagemCartao").substring(0, _json.getString("ImagemCartao").lastIndexOf("/") + 1);
                        String fileName = _json.getString("ImagemCartao").substring(_json.getString("ImagemCartao").lastIndexOf("/") + 1, _json.getString("ImagemCartao").length());
                        String versaoImagem = getResources().getString(R.string.versaoImg);
                        _cartaoServer.setVersaoImagemCartao(versaoImagem);
                        URLbase += versaoImagem + "/" + fileName;

                        if (null != URLbase) {
                            if (!URLbase.equals("")) {
                                _cartaoServer.setImagemCartao(_imgHelper.getCaminhoImagenCartaoByURL(URLbase));
                                getImagemCartao _imgTask = new getImagemCartao(URLbase, getActivity());
                                _imgTask.execute();
                            }
                        }

                    } else {
                        if (!Global.existeArquivo(_cartao.getImagemCartao())) {
                            try {
                                sendPushToken();
                            } catch (Exception e) {
                            }

                            String URLbase = _json.getString("ImagemCartao").substring(0, _json.getString("ImagemCartao").lastIndexOf("/") + 1);
                            String fileName = _json.getString("ImagemCartao").substring(_json.getString("ImagemCartao").lastIndexOf("/") + 1, _json.getString("ImagemCartao").length());
                            String versaoImagem = getResources().getString(R.string.versaoImg);
                            _cartaoServer.setVersaoImagemCartao(versaoImagem);
                            URLbase += versaoImagem + "/" + fileName;

                            if (null != URLbase) {
                                if (!URLbase.equals("")) {
                                    _cartaoServer.setImagemCartao(_imgHelper.getCaminhoImagenCartaoByURL(URLbase));
                                    getImagemCartao _imgTask = new getImagemCartao(URLbase, getActivity());
                                    _imgTask.execute();
                                }
                            }
                        }
                    }

                    if (_cartao.getImagemCartao() != null)
                        _cartaoServer.setImagemCartao(_cartao.getImagemCartao());
                    _cartoesArray.add(_cartaoServer);

                    atualizarCartao(_cartaoServer, String.valueOf(_cartaoServer.getCartaoId()));
                }

                onRefreshMenuItem.stopLoadingSaldo();

                _cartoesAdapter = new CartaoAdapter(getActivity(), _cartoesArray);
                _cartaoRecyclerView.setAdapter(_cartoesAdapter);

            } else {

                _mensagem = _jsonResp.getString("mensagem");
                if (_mensagem != "") {
                    Toast.makeText(getActivity(), _mensagem, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), "Tente novamente mais tarde", Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Falha na conexão de rede.", LENGTH_LONG).show(); //+ e.getMessage()
            return;
        }
    }

    private void sendPushToken() {
        ConexaoSQLite _banco;
        _banco = ConexaoSQLite.getInstancia(getActivity());

        Perfil _perfil;
        PerfilCtrl _perCTRL;
        _perCTRL = new PerfilCtrl(_banco);
        _perfil = _perCTRL.getPerfilCtrl(1);

        if (null != _perfil) {
            if (_perfil.getID() == 1) {
                Global.getInstance().apiDeviceID = _perfil.getIMEI();
                //Global.getInstance().apiTokenPerfil = _perfil.getToken();

                TokenComproCard _token = new TokenComproCard();
                _token.setCPF(_perfil.getCPF());
                _token.setIMEI(_perfil.getIMEI());
                _token.setToken(FirebaseInstanceId.getInstance().getToken());

                postSendToken _taskToken = new postSendToken(Global.getInstance().apiURLPushToken, _token, getActivity(), false);
                _taskToken.execute();
            }
        }
    }

    @Override
    public void onResultsSucceeded(resServer result) {
        int httpStatus;
        JSONObject _json;
        String _mensagem = "";

        try {
            httpStatus = result.getHttpStatus();
            _json = result.getJsonBody();

            if (httpStatus == HttpURLConnection.HTTP_OK) {
                Global.getInstance().apiTokenPerfil = _json.getString("token");

                postCartaoSaldos _taskGetSaldos = new postCartaoSaldos(Global.getInstance().apiURLSaldos, getActivity());
                _taskGetSaldos.setOnResultsListener(this);
                _taskGetSaldos.execute();
            } else {
                _mensagem = _json.getString("mensagem");
                if (_mensagem != "") {
                    Toast.makeText(getActivity(), _mensagem, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), "Tente novamente mais tarde", Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Falha na conexão de rede.", LENGTH_LONG).show(); //+ e.getMessage()
            return;
        }
    }

    public boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(getActivity().CONNECTIVITY_SERVICE);
        NetworkInfo net = cm.getActiveNetworkInfo();
        if (net != null && net.isAvailable() && net.isConnected()) {
            Global.getInstance().isConected = true;
            return true;
        } else {
            Global.getInstance().isConected = false;
            Toast.makeText(getActivity(), "Por favor conecte-se a internet", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    public void refresh() {
        if (null != getView()) {
            cartaoCarregaLista();
            refreshSaldoCartaoByPerfil(0);
            getBanners(getView());
        }
    }

    public interface OnRefreshMenuItem {
        public void startLoadingSaldo();

        public void stopLoadingSaldo();
    }
}
