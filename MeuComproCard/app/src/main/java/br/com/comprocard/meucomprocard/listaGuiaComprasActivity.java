package br.com.comprocard.meucomprocard;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import java.util.ArrayList;

import br.com.comprocard.meucomprocard.Adapters.guiaComprasAdapter;
import br.com.comprocard.meucomprocard.Model.guiaCompras;
import br.com.comprocard.meucomprocard.Util.Global;

/**
 * Created by andre.cardoso on 14/06/2017.
 */

public class listaGuiaComprasActivity extends AppCompatActivity {

    public ArrayList<guiaCompras> _guiaComprasArray;
    private guiaComprasAdapter _guiaAdapter;
    private RecyclerView _guiaRecyclerView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guia_compras);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Filtro - Guia de Compras");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        criarPopularGuiaComprasArray(Global.getInstance().guiaComprasList);
    }

    private void criarPopularGuiaComprasArray(ArrayList<guiaCompras> _resultadoBusca) {
        _guiaComprasArray = _resultadoBusca;
        guiaoCarregaLista();
    }

    private void guiaoCarregaLista() {
        _guiaAdapter = new guiaComprasAdapter(listaGuiaComprasActivity.this, _guiaComprasArray);
        _guiaRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_guia);
        _guiaRecyclerView.setAdapter(_guiaAdapter);
        _guiaRecyclerView.setItemAnimator(new DefaultItemAnimator());
        _guiaRecyclerView.setLayoutManager(new LinearLayoutManager(listaGuiaComprasActivity.this));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
