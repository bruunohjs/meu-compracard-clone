package br.com.comprocard.meucomprocard.config;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitConfig {
    public static final String BASE_URL = "https://sistemas.comprocard.com.br/meucomprocard/api/";

    public static Retrofit getInstance() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl( BASE_URL )
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }
}
