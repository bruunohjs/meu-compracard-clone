package br.com.comprocard.meucomprocard.Controller;

import java.util.ArrayList;

import br.com.comprocard.meucomprocard.DAO.FaturasDAO;
import br.com.comprocard.meucomprocard.dbHelper.ConexaoSQLite;
import br.com.comprocard.meucomprocard.Model.Faturas;

public class FaturasCtrl {
	private FaturasDAO daoFaturas;

	public FaturasCtrl() {
	} 

	public FaturasCtrl(ConexaoSQLite pSQLite) {
		this.daoFaturas = new FaturasDAO(pSQLite);
	} 

	public long salvarFaturasCtrl(Faturas pFatura) {
		return this.daoFaturas.salvarFaturasDAO(pFatura);
	} 

	public Faturas getFaturasCtrl(int pId) {
		return this.daoFaturas.getFaturasDAO(pId);
	} 

	public ArrayList<Faturas> getListaFaturasCtrl() {
		return this.daoFaturas.getListaFaturasDAO();
	} 

	public boolean excluirFaturasCtrl(long pId) {
		return this.daoFaturas.excluirFaturasDAO(pId);
	} 

}