package br.com.comprocard.meucomprocard.dbHelper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by andre.cardoso on 25/10/2016.
 */
public class ConexaoSQLite extends SQLiteOpenHelper {

    private static ConexaoSQLite INSTANCIA_CONEXAO = null;
    private static final int VERSAO_DB = 1;
    private static final String NOME_DB = "meuCartao.db";
    public static final int ID_PERFIL = 1;

    public ConexaoSQLite(Context context) {
        super(context, NOME_DB, null, VERSAO_DB);
    }

    public static synchronized ConexaoSQLite getInstancia(Context context){
        if(INSTANCIA_CONEXAO == null){
            INSTANCIA_CONEXAO = new ConexaoSQLite(context);
            INSTANCIA_CONEXAO.getWritableDatabase();
        }
        return INSTANCIA_CONEXAO;
    }

    private void AtualizaCampos(SQLiteDatabase db) {
        try {
            String sqlTemp = "ALTER TABLE Perfil ADD Telefone VARCHAR;";
            if (!existsColumnInTable(db, "Perfil", "Telefone")) {
                Log.i("banco", "Atualizando tabela perfil.... ");
                db.execSQL(sqlTemp);
            }

            sqlTemp = "ALTER TABLE Perfil ADD DataHora VARCHAR;";
            if (!existsColumnInTable(db, "Perfil", "DataHora")) {
                Log.i("banco", "Atualizando tabela perfil.... ");
                db.execSQL(sqlTemp);
            }

            sqlTemp = "ALTER TABLE Cartao ADD Token VARCHAR;";
            if (!existsColumnInTable(db, "Cartao", "Token")) {
                Log.i("banco", "Atualizando tabela cartão.... ");
                db.execSQL(sqlTemp);
            }

            sqlTemp = "ALTER TABLE Cartao ADD CartaoId INTEGER;";
            if (!existsColumnInTable(db, "Cartao", "CartaoId")) {
                Log.i("banco", "Atualizando tabela cartão.... ");
                db.execSQL(sqlTemp);
            }

            sqlTemp = "ALTER TABLE Cartao ADD CartaoNumFmt VARCHAR;";
            if (!existsColumnInTable(db, "Cartao", "CartaoNumFmt")) {
                Log.i("banco", "Atualizando tabela cartão.... ");
                db.execSQL(sqlTemp);
            }

            sqlTemp = "ALTER TABLE Cartao ADD UltimaConsulta VARCHAR;";
            if (!existsColumnInTable(db, "Cartao", "UltimaConsulta")) {
                Log.i("banco", "Atualizando tabela cartão.... ");
                db.execSQL(sqlTemp);
            }

            sqlTemp = "ALTER TABLE Cartao ADD ImagemCartao VARCHAR;";
            if (!existsColumnInTable(db, "Cartao", "ImagemCartao")) {
                Log.i("banco", "Atualizando tabela cartão.... ");
                db.execSQL(sqlTemp);
            }

            sqlTemp = "ALTER TABLE Cartao ADD versaoImagemCartao VARCHAR;";
            if (!existsColumnInTable(db, "Cartao", "versaoImagemCartao")) {
                Log.i("banco", "Atualizando tabela cartão.... ");
                db.execSQL(sqlTemp);
            }

            sqlTemp = "ALTER TABLE Historico ADD Parcela INTEGER;";
            if (!existsColumnInTable(db, "Historico", "Parcela")) {
                Log.i("banco", "Atualizando tabela historico.... ");
                db.execSQL(sqlTemp);
            }

        } catch (Exception Exp) {
            Log.d("Erro", "Erro ao checar se coluna existe: " + Exp.getMessage());
        } finally {

        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String sqlTabelaPerfil =
                "CREATE TABLE IF NOT EXISTS Perfil " +
                        "( " +
                        "	Id INTEGER, " +
                        "	Nome VARCHAR, " +
                        "	Email VARCHAR, " +
                        "	CPF VARCHAR, " +
                        "	DataNascimento VARCHAR, " +
                        "	IMEI VARCHAR, " +
                        "	Senha VARCHAR, " +
                        "	Ativo INTEGER, " +
                        "	Token VARCHAR, " +
                        "   Telefone VARCHAR, "+
                        "   DataHora VARCHAR "+
                        "); ";

        String sqlTabelaCartao =
                "CREATE TABLE IF NOT EXISTS Cartao " +
                        "( " +
                        "	Id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "	Bandeira VARCHAR, " +
                        "	Produto VARCHAR, " +
                        "	Empresa VARCHAR, " +
                        "	Nome VARCHAR, " +
                        "	Numero VARCHAR, " +
                        "	Validade VARCHAR, " +
                        "	CVV VARCHAR, " +
                        "	Saldo BIGDECIMAL, " +
                        "	Limite BIGDECIMAL, " +
                        "	DiaVencimento INTEGER, " +
                        "	Situacao VARCHAR, " +
                        "	Senha VARCHAR, " +
                        "   Token VARCHAR, " +
                        "   CartaoId INTEGER, "+
                        "   CartaoNumFmt VARCHAR, "+
                        "   UltimaConsulta VARCHAR, "+
                        "   ImagemCartao VARCHAR "+
                        "	); ";

        String sqlTabelaHistorico =
                "CREATE TABLE IF NOT EXISTS Historico " +
                        "( " +
                        "	Id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "	cartaoID INTEGER, " +
                        "	DataHora VARCHAR, " +
                        "	Valor BIGDECIMAL, " +
                        "	Tipo VARCHAR, " +
                        "   Parcela  INTEGER, "+
                        "	Descricao VARCHAR " +
                        "); ";

        String sqlTabelaFaturas =
                "CREATE TABLE IF NOT EXISTS Faturas " +
                        "( " +
                        "	Id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "	cartaoID INTEGER, " +
                        "	MesAnoReferencia VARCHAR, " +
                        "	Valor BIGDECIMAL, " +
                        "	Situacao VARCHAR " +
                        "); ";

        String sqlTabelaIndicacao =
                "CREATE TABLE IF NOT EXISTS Indicacao " +
                        "( " +
                        "	cpf VARCHAR, " +
                        "	estabelecimento VARCHAR, " +
                        "	contato VARCHAR, " +
                        "	telefone VARCHAR, " +
                        "	email VARCHAR, " +
                        "	Latitude VARCHAR, " +
                        "	Longitude VARCHAR " +
                        "); ";

        Log.i("banco", "Criando tabela perfil.");
        db.execSQL(sqlTabelaPerfil);

        Log.i("banco", "Criando tabela cartao.");
        db.execSQL(sqlTabelaCartao);

        Log.i("banco", "Criando tabela historico.");
        db.execSQL(sqlTabelaHistorico);

        Log.i("banco", "Criando tabela faturas.");
        db.execSQL(sqlTabelaFaturas);

        Log.i("banco", "Criando tabela estabelecimento.");
        db.execSQL(sqlTabelaIndicacao);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        this.onCreate(db);
        AtualizaCampos(db);
    }

    private boolean existsColumnInTable(SQLiteDatabase inDatabase, String inTable, String columnToCheck) {
        Cursor mCursor = null;
        try {
            mCursor = inDatabase.rawQuery("SELECT * FROM " + inTable + " LIMIT 0", null);

            if (mCursor.getColumnIndex(columnToCheck) != -1)
                return true;
            else
                return false;

        } catch (Exception Exp) {
            Log.d("Erro", "Erro ao checar se coluna existe: " + Exp.getMessage());
            return false;
        } finally {
            if (mCursor != null) mCursor.close();
        }
    }

}