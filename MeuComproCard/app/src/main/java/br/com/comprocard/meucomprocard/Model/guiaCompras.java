package br.com.comprocard.meucomprocard.Model;

import java.io.Serializable;

/**
 * Created by andre.cardoso on 23/11/2016.
 */

public class guiaCompras implements Serializable {
    private String Estabelecimento;
    private String Cidade;
    private String UF;
    private String Bairro;
    private String Atividade;
    private String Teletone;
    private String geo_Latitude;
    private String geo_Longitude;

    public String getEstabelecimento() {
        return Estabelecimento;
    }

    public void setEstabelecimento(String estabelecimento) {
        Estabelecimento = estabelecimento;
    }

    public String getCidade() {
        return Cidade;
    }

    public void setCidade(String cidade) {
        Cidade = cidade;
    }

    public String getUF() {
        return UF;
    }

    public void setUF(String UF) {
        this.UF = UF;
    }

    public String getBairro() {
        return Bairro;
    }

    public void setBairro(String bairro) {
        Bairro = bairro;
    }

    public String getAtividade() {
        return Atividade;
    }

    public void setAtividade(String atividade) {
        Atividade = atividade;
    }

    public String getTeletone() {
        return Teletone;
    }

    public void setTeletone(String teletone) {
        Teletone = teletone;
    }

    public String getGeo_Latitude() {
        return geo_Latitude;
    }

    public void setGeo_Latitude(String geo_Latitude) {
        this.geo_Latitude = geo_Latitude;
    }

    public String getGeo_Longitude() {
        return geo_Longitude;
    }

    public void setGeo_Longitude(String geo_Longitude) {
        this.geo_Longitude = geo_Longitude;
    }
}
