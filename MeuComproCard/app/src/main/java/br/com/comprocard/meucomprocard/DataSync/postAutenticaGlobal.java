package br.com.comprocard.meucomprocard.DataSync;

/**
 * Created by Andre on 06/12/2016.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import br.com.comprocard.meucomprocard.Model.Perfil;
import br.com.comprocard.meucomprocard.Util.Global;
import br.com.comprocard.meucomprocard.Util.MeuComproCardProgressDialog;
import br.com.comprocard.meucomprocard.dbHelper.ConexaoSQLite;

public class postAutenticaGlobal extends AsyncTask<String, Void, String> {

    private ConexaoSQLite sqliteDataBase;
    private String _localURL;
    private Perfil _perfilPost;
    private Context mContext;

    public postAutenticaGlobal(String _url, Perfil _perfil, Context context, Boolean showProgress) {
        this.mContext = context;
        this._localURL = _url;
        this._perfilPost = _perfil;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    protected String doInBackground(final String... args) {
        Map<String, String> mapPerfil = new HashMap<String, String>();
        mapPerfil.put("deviceId", String.valueOf(_perfilPost.getIMEI()));
        mapPerfil.put("cpf", String.valueOf(_perfilPost.getCPF().replace(".","").replace("-","").replace("/","")));

        String _jsonAutenticacao = new GsonBuilder().create().toJson(mapPerfil, Map.class);
        String _res;
        try {
            _res = uploadToServer( Global.getInstance().apiURLAutenticacao, _jsonAutenticacao);
            try { Thread.sleep(200); }
            catch (InterruptedException e) { e.printStackTrace(); }
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
        return _res;
    }

    private String uploadToServer(String query, String json ) throws IOException, JSONException {
        JSONObject jsonObj = null;
        String _jsonToken;
        URL url = new URL(query);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        try {
            conn.setConnectTimeout(5000);
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");

            OutputStream os = conn.getOutputStream();
            os.write(json.getBytes("UTF-8"));
            os.close();

            InputStream in = new BufferedInputStream(conn.getInputStream());
            byte[] bytes = new byte[1000];

            StringBuilder _reader = new StringBuilder();
            int numRead = 0;
            while ((numRead = in.read(bytes)) >= 0) {
                _reader.append(new String(bytes, 0, numRead));
            }
            jsonObj = new JSONObject(_reader.toString());
            in.close();
            conn.disconnect();

        } catch (Exception e) {
            e.getMessage();
        } finally {
            resServer _resServer = new resServer();
            _resServer.setHttpStatus(conn.getResponseCode());
            InputStream is = null;

            if (_resServer.getHttpStatus() == 200) {
                _jsonToken  = jsonObj.getString("token");
                Global.getInstance().apiTokenCartao = _jsonToken;
            }
            else {
                if (_resServer.getHttpStatus() > 200 && _resServer.getHttpStatus() < 400) {
                    is = conn.getInputStream();
                } else {
                    is = conn.getErrorStream();
                }

                try {
                    jsonObj = new JSONObject(Global.getInstance().convertStreamToString(is));
                } catch (Exception e) {
                    jsonObj = null;
                }

                if (null == jsonObj) {
                    jsonObj = new JSONObject("{\n" +
                            "  \"codigo\": -1,\n" +
                            "  \"mensagem\": \"Erro de autenticação.\"\n" +
                            "}");
                }
            }
            _resServer.setJsonBody( jsonObj );
            return "";
        }
    }

}
