package br.com.comprocard.meucomprocard.Model;

import java.math.BigDecimal;

/**
 * Created by andre.cardoso on 23/12/2016.
 */

public class Indicacao {

    private String CPF;
    private String Nome;
    private String NomeContato;
    private String Telefone;
    private String EmailContato;
    private String Latitude;
    private String Longitude;

    public String getCPF() {
        return CPF;
    }

    public void setCPF(String CPF) {
        this.CPF = CPF;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    public String getNomeContato() {
        return NomeContato;
    }

    public void setNomeContato(String nomeContato) {
        NomeContato = nomeContato;
    }

    public String getTelefone() {
        return Telefone;
    }

    public void setTelefone(String telefone) {
        Telefone = telefone;
    }

    public String getEmailContato() {
        return EmailContato;
    }

    public void setEmailContato(String emailContato) {
        EmailContato = emailContato;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }
}
