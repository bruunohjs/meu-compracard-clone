package br.com.comprocard.meucomprocard.Model.api;

public class CartaoLoginRequest {
    private String numeroCartao;

    public String getNumeroCartao() {
        return numeroCartao;
    }

    public void setNumeroCartao(String numeroCartao) {
        this.numeroCartao = numeroCartao;
    }
}
