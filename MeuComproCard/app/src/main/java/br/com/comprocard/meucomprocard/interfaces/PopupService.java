package br.com.comprocard.meucomprocard.interfaces;

import br.com.comprocard.meucomprocard.Model.api.PopupResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Url;

public interface PopupService {

    @GET
    Call<PopupResponse> getPopup(@Url String url, @Header("Authorization") String token, @Header("deviceId") String deviceId);
}
