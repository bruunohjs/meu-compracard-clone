package br.com.comprocard.meucomprocard.DataSync;

/**
 * Created by Andre on 06/12/2016.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import br.com.comprocard.meucomprocard.Controller.CartaoCtrl;
import br.com.comprocard.meucomprocard.Model.Cartao;
import br.com.comprocard.meucomprocard.Util.Data;
import br.com.comprocard.meucomprocard.Util.Global;
import br.com.comprocard.meucomprocard.Util.MeuComproCardProgressDialog;
import br.com.comprocard.meucomprocard.dbHelper.ConexaoSQLite;

public class postCartaoBloqDesbq extends AsyncTask<String, Void, resServer> {

    private ConexaoSQLite sqliteDataBase;
    private String _localURL;
    private CartaoCtrl _cartaoCtrl;
    private Cartao _cartaoPost;

    private Context mContext;
    private Data datas;
    protected ProgressDialog mProgressDialog;
    ResultsBloqDesbloq listener;

    public postCartaoBloqDesbq(String _url, Context context) {
        this.mContext = context;
        this._localURL = _url;
        _cartaoCtrl = new CartaoCtrl(ConexaoSQLite.getInstancia(mContext));
        this.datas = new Data();
    }

    @Override
    protected void onPreExecute() {
        mProgressDialog = MeuComproCardProgressDialog.ctor(mContext);
        mProgressDialog.show();
    }

    @Override
    protected void onPostExecute(resServer result) {
        if (mProgressDialog != null && mProgressDialog.isShowing() && mProgressDialog.getWindow() != null) {
            try {
                mProgressDialog.dismiss();
            } catch ( IllegalArgumentException ignore ) { ; }
        }

        listener.onResultsBloqDesbloq(result);
        super.onPostExecute(result);
    }

    public void setOnResultsListener(ResultsBloqDesbloq listener) {
        this.listener = listener;
    }

    protected resServer doInBackground(final String... args) {
        resServer _res;
        try {
            _res = uploadToServer( this._localURL );
        } catch (IOException e) {
            e.printStackTrace();
            return new resServer();
        } catch (JSONException e) {
            e.printStackTrace();
            return new resServer();
        }
        return _res;
    }

    private resServer uploadToServer(String _apiurl) throws IOException, JSONException {
        JSONObject jsonObj = null;
        URL url = new URL(_apiurl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        try {
            conn.setConnectTimeout(5000);
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", "Basic " + Global.getInstance().apiTokenCartao);
            conn.setRequestProperty("deviceId", Global.getInstance().apiDeviceID);

            InputStream in = new BufferedInputStream(conn.getInputStream());
            byte[] bytes = new byte[1000];

            StringBuilder _reader = new StringBuilder();
            int numRead = 0;
            while ((numRead = in.read(bytes)) >= 0) {
                _reader.append(new String(bytes, 0, numRead));
            }
            jsonObj = new JSONObject(_reader.toString());
            in.close();
            conn.disconnect();

        } catch (Exception e) {
            e.getMessage();
        } finally {
            resServer _resServer = new resServer();
            _resServer.setHttpStatus(conn.getResponseCode());

            InputStream is = null;
            if (_resServer.getHttpStatus() != 200) {
                if (_resServer.getHttpStatus() > 200 && _resServer.getHttpStatus() < 400) {
                    is = conn.getInputStream();
                } else {
                    is = conn.getErrorStream();
                }

                try {
                    jsonObj = new JSONObject(Global.getInstance().convertStreamToString(is));
                } catch (Exception e) {
                    jsonObj = null;
                }

                if (null == jsonObj) {
                    jsonObj = new JSONObject("{\n" +
                            "  \"codigo\": -1,\n" +
                            "  \"mensagem\": \"Erro de autenticação.\"\n" +
                            "}");
                }
            }
            _resServer.setJsonBody( jsonObj );
            return _resServer;        }
    }
}
