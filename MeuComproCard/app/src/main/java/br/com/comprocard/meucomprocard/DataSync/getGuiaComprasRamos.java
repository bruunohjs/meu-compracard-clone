package br.com.comprocard.meucomprocard.DataSync;

/**
 * Created by Andre on 06/12/2016.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import br.com.comprocard.meucomprocard.Util.Data;
import br.com.comprocard.meucomprocard.Util.Global;
import br.com.comprocard.meucomprocard.Util.MeuComproCardProgressDialog;

public class getGuiaComprasRamos extends AsyncTask<String, Void, resServer> {

    private String _localURL;
    private Context mContext;
    private Data datas;
    protected ProgressDialog mProgressDialog;
    private String mjsonPost;
    ResultsListener listener;

    public getGuiaComprasRamos(String _url, String _jsonPost, Context context) {
        this.mContext = context;
        this._localURL = _url;
        this.datas = new Data();
        this.mjsonPost = _jsonPost;
    }

    public void setOnResultsListener(ResultsListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mProgressDialog = MeuComproCardProgressDialog.ctor(mContext);
        mProgressDialog.show();
    }

    @Override
    protected void onPostExecute(resServer result) {
        if (mProgressDialog != null && mProgressDialog.isShowing() && mProgressDialog.getWindow() != null) {
            try {
                mProgressDialog.dismiss();
            } catch ( IllegalArgumentException ignore ) { ; }
        }
        listener.onResultsSucceeded(result);
        super.onPostExecute(result);
    }

    protected resServer doInBackground(final String... args) {

        resServer _res;
        try {
            _res = uploadToServer( this._localURL, mjsonPost);
        } catch (IOException e) {
            e.printStackTrace();
            return new resServer();
        } catch (JSONException e) {
            e.printStackTrace();
            return new resServer();
        }
        return _res;
    }

    private resServer uploadToServer(String query, String json ) throws IOException, JSONException {
        JSONArray _json = null;
        URL url = new URL(query);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        try {
            conn.setConnectTimeout(5000);
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", "Basic " + Global.getInstance().apiTokenPerfil );
            conn.setRequestProperty("deviceId", Global.getInstance().apiDeviceID);

            OutputStream os = conn.getOutputStream();
            os.write(json.getBytes("UTF-8"));
            os.close();

            InputStream in = new BufferedInputStream(conn.getInputStream());
            byte[] bytes = new byte[1000];

            StringBuilder _reader = new StringBuilder();
            int numRead = 0;
            while ((numRead = in.read(bytes)) >= 0) {
                _reader.append(new String(bytes, 0, numRead));
            }
            _json = new JSONArray(_reader.toString());
            in.close();
            conn.disconnect();

        } catch (Exception e) {
            e.getMessage();
        } finally {
            resServer _resServer = new resServer();
            _resServer.setHttpStatus(conn.getResponseCode());

            InputStream is = null;
            if (_resServer.getHttpStatus() != 200) {
                if (_resServer.getHttpStatus() > 200 && _resServer.getHttpStatus() < 400) {
                    is = conn.getInputStream();
                } else {
                    is = conn.getErrorStream();
                }

                try {
                    _json = new JSONArray(Global.getInstance().convertStreamToString(is));
                } catch (Exception e) {
                    _json = null;
                }

                if (null == _json) {
                    _json = new JSONArray("{\n" +
                            "  \"codigo\": -1,\n" +
                            "  \"mensagem\": \"Erro de autenticação.\"\n" +
                            "}");
                }
            }
            _resServer.setJsonArray ( _json );
            return _resServer;
        }
    }

}
