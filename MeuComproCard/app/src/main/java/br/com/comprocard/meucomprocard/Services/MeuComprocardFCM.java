package br.com.comprocard.meucomprocard.Services;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by esgama on 07/06/2017.
 */

public class MeuComprocardFCM extends FirebaseMessagingService {
    private static String TAG = "FCM";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO(developer): Handle FCM messages here.

        // Só será necessário escrever código aqui caso precisemos de tratamento especial das mensagens recebidas.
    }
}

