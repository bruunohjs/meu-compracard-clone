package br.com.comprocard.meucomprocard.DAO;

import java.util.ArrayList;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.com.comprocard.meucomprocard.dbHelper.ConexaoSQLite;
import br.com.comprocard.meucomprocard.Model.Faturas;
import br.com.comprocard.meucomprocard.Util.Data;

public class FaturasDAO {
	private ConexaoSQLite sqliteDataBase;

	private final String[] COLUNAS_ARRAY = { "Id", "cartaoID", "MesAnoReferencia", "Valor", "Situacao" };
	private Data datas;

	public FaturasDAO(ConexaoSQLite pSqliteDataBase) {
		this.sqliteDataBase = pSqliteDataBase;
	}

	public long salvarFaturasDAO(Faturas faturas) {
		long Id = 0;
		SQLiteDatabase db = this.sqliteDataBase.getWritableDatabase();
		try {
			ContentValues values = new ContentValues();
			values.put("cartaoID", faturas.getCartaoID());
			values.put("MesAnoReferencia", faturas.getMesAnoReferencia());
			values.put("Valor", String.valueOf(faturas.getValor()));
			values.put("Situacao", faturas.getSituacao());
			Id = db.insert("Faturas", null, values);
		} catch (Exception e) {
			Log.d("DB_ERROR", "NAO FOI POSSIVEL INSERIR FATURA");
		} finally {
			db.close();
		}
		return Id;
	}

	public Faturas getFaturasDAO(int pId) {
		Faturas faturas = new Faturas();
		SQLiteDatabase db = this.sqliteDataBase.getReadableDatabase();
		Cursor cursor = null;
		try {
			cursor = db.query("Faturas", COLUNAS_ARRAY, " Id = ?", new String[] { String.valueOf(pId) }, null,null,null,null);
			if (cursor != null) {
				cursor.moveToFirst();
			}
			faturas.setID(Integer.parseInt(cursor.getString(0)));
			faturas.setCartaoID(Integer.parseInt(cursor.getString(1)));
			faturas.setMesAnoReferencia(cursor.getString(2));
			faturas.setSituacao(cursor.getString(4));
		} catch (Exception e) {
			Log.d("DB_ERROR", "NAO FOI POSSIVEL RECUPERAR O FATURA");
		} finally {
			cursor.close();
				db.close();
		}
		return faturas;
	}

	public ArrayList<Faturas> getListaFaturasDAO() {
		Faturas faturas = null;
		ArrayList<Faturas> listaFaturas = new ArrayList<Faturas>();
		SQLiteDatabase db = null;
		Cursor cursor = null;
		String query = "SELECT * FROM Faturas  ORDER BY MesAnoReferencia ";
		try {
			db = this.sqliteDataBase.getReadableDatabase();
			cursor = db.rawQuery(query, null);
			if (cursor.moveToFirst()) {
			do {
			faturas = new Faturas();
			faturas.setID(Integer.parseInt(cursor.getString(0)));
			faturas.setCartaoID(Integer.parseInt(cursor.getString(1)));
			faturas.setMesAnoReferencia(cursor.getString(2));
			faturas.setSituacao(cursor.getString(4));
				listaFaturas.add(faturas);
			} while (cursor.moveToNext());
		}
		} catch (Exception e) {
			Log.d("DB_ERROR", "NAO FOI POSSIVEL LISTAR FATURA");
		} finally {
			cursor.close();
			db.close();
		}
			return listaFaturas;
	}

	public boolean excluirFaturasDAO(long pId) {
		SQLiteDatabase db = null;
		try {
		db = this.sqliteDataBase.getWritableDatabase();
		db.delete(
			"Faturas",
			"Id = ?",
			new String[] { String.valueOf(pId) });
		} catch (Exception e) {
			Log.d("DB_ERROR", "NAO FOI POSSIVEL DELETAR FATURA");
			return false;
		} finally {
			db.close();
		}
		return true;
	}

}