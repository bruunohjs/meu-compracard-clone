package br.com.comprocard.meucomprocard.DataSync;

/**
 * Created by Andre on 06/12/2016.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import br.com.comprocard.meucomprocard.Util.ImagensCartaoHelper;


public class getImagemCartao extends AsyncTask<String, String, String> {

    private String _localURL;
    private Context mContext;

    public getImagemCartao(String _url, Context context) {
        this.mContext = context;
        this._localURL = _url;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    protected String doInBackground(final String... args) {
        String sResult="";
        try {
            ImagensCartaoHelper _imgHelper = new ImagensCartaoHelper();
            String localFilename = _imgHelper.getCaminhoImagenCartaoByURL(this._localURL);
            File img = new File(localFilename);
            try {
                img.delete();
            } catch (Exception e) {

            }

            try {
                URL imageUrl = null;
                imageUrl = new URL( this._localURL );

                URLConnection urlConnection = imageUrl.openConnection();
                urlConnection.connect();
                int file_size = urlConnection.getContentLength()/1000;

                InputStream in = new BufferedInputStream(imageUrl.openStream(), 8192);
                int total = 0;
                OutputStream out = new BufferedOutputStream(new FileOutputStream(img));
                byte data[] = new byte[1024];
                for (int b; (b = in.read(data)) != -1;) {
                    total += b/1000;
                    out.write(data, 0, b);
                }

                out.close();
                in.close();
                sResult =localFilename;
            } catch (MalformedURLException e) {
                img = null;
                Log.e("saveImage",e.getMessage());
            } catch (IOException e) {
                img = null;
                Log.e("saveImage",e.getMessage());
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return sResult;
    }

}
