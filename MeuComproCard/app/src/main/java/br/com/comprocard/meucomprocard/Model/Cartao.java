package br.com.comprocard.meucomprocard.Model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by andre.cardoso on 25/10/2016.
 */
public class Cartao {

    private int ID;
    private int CartaoId;
    private String Bandeira;
    private String Produto;
    private String Empresa;
    private String Nome;
    private String Numero;
    private String Validade;
    private String CVV;
    private BigDecimal Saldo;
    private BigDecimal Limite;
    private int DiaVencimento;
    private String Situacao;
    private String Senha;
    private String Token;
    private String UltimaConsulta;
    private String CartaoNumFmt;
    private String ImagemCartao;
    private String versaoImagemCartao;

    public String getImagemCartao() {
        return ImagemCartao;
    }

    public void setImagemCartao(String imagemCartao) {
        ImagemCartao = imagemCartao;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getBandeira() {
        return Bandeira;
    }

    public void setBandeira(String bandeira) {
        Bandeira = bandeira;
    }

    public String getProduto() {
        return Produto;
    }

    public void setProduto(String produto) {
        Produto = produto;
    }

    public String getEmpresa() {
        return Empresa;
    }

    public void setEmpresa(String empresa) {
        Empresa = empresa;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    public String getNumero() {
        return Numero;
    }

    public void setNumero(String numero) {
        Numero = numero;
    }

    public String getValidade() {
        return Validade;
    }

    public void setValidade(String validade) {
        Validade = validade;
    }

    public String getCVV() {
        return CVV;
    }

    public void setCVV(String CVV) {
        this.CVV = CVV;
    }

    public BigDecimal getSaldo() {
        return new BigDecimal(Saldo.toString());
    }

    public void setSaldo(BigDecimal saldo) {
        Saldo = saldo;
    }

    public BigDecimal getLimite() {
        return new BigDecimal(Limite.toString());
    }

    public void setLimite(BigDecimal limite) {
        Limite = limite;
    }

    public int getDiaVencimento() {
        return DiaVencimento;
    }

    public void setDiaVencimento(int diaVencimento) {
        DiaVencimento = diaVencimento;
    }

    public String getSituacao() {
        return Situacao;
    }

    public void setSituacao(String situacao) {
        Situacao = situacao;
    }

    public String getSenha() {
        return Senha;
    }

    public void setSenha(String senha) {
        Senha = senha;
    }

    public String getToken() {

        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public String getUltimaConsulta() {
        return UltimaConsulta;
    }

    public void setUltimaConsulta(String ultimaConsulta) {
        UltimaConsulta = ultimaConsulta;
    }

    public int getCartaoId() {
        return CartaoId;
    }

    public void setCartaoId(int cartaoId) {
        CartaoId = cartaoId;
    }

    public String getCartaoNumFmt() {
        return CartaoNumFmt;
    }

    public void setCartaoNumFmt(String cartaoNumFmt) {
        CartaoNumFmt = cartaoNumFmt;
    }

    @Override
    public String toString() {
        return "Cartao [ID="+ID+", Bandeira="+Bandeira+", Produto="+Produto+", Empresa="+Empresa+", Nome="+Nome+", Numero="+Numero+", Validade="+Validade+", CVV="+CVV+", Saldo="+Saldo+", Limite="+Limite+", DiaVencimento="+DiaVencimento+", Situacao="+Situacao+", Senha="+Senha+"] ";
    }

    public String getVersaoImagemCartao() {
        return versaoImagemCartao;
    }

    public void setVersaoImagemCartao(String versaoImagemCartao) {
        this.versaoImagemCartao = versaoImagemCartao;
    }
}