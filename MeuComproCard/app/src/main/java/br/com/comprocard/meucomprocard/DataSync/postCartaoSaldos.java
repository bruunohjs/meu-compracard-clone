package br.com.comprocard.meucomprocard.DataSync;

/**
 * Created by Andre on 06/12/2016.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import br.com.comprocard.meucomprocard.Controller.CartaoCtrl;
import br.com.comprocard.meucomprocard.Model.Cartao;
import br.com.comprocard.meucomprocard.Util.Data;
import br.com.comprocard.meucomprocard.Util.Global;
import br.com.comprocard.meucomprocard.Util.MCrypt;
import br.com.comprocard.meucomprocard.dbHelper.ConexaoSQLite;

public class postCartaoSaldos extends AsyncTask<String, Void, resServer> {

    private ConexaoSQLite sqliteDataBase;
    private String _localURL;
    private CartaoCtrl _cartaoCtrl;

    private Context mContext;
    private Data datas;
    protected ProgressDialog mProgressDialog;
    ResultsListenerRefresh listener;

    protected void showProgress( ) {
        mProgressDialog = new ProgressDialog(mContext);
        mProgressDialog.setMessage("Por favor aguarde...");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    protected void dismissProgress() {
        if (mProgressDialog != null && mProgressDialog.isShowing() && mProgressDialog.getWindow() != null) {
            try {
                mProgressDialog.dismiss();
            } catch ( IllegalArgumentException ignore ) { ; }
        }
    }

    public postCartaoSaldos(String _url, Context context) {
        this.mContext = context;
        this._localURL = _url;
        _cartaoCtrl = new CartaoCtrl(ConexaoSQLite.getInstancia(mContext));
        this.datas = new Data();
    }

    @Override
    protected void onPreExecute() {

        //showProgress();
    }

    @Override
    protected void onPostExecute(resServer result) {
        dismissProgress();
        listener.onResultsSucceededRefresh(result);
        super.onPostExecute(result);
    }

    public void setOnResultsListener(ResultsListenerRefresh listener) {
        this.listener = listener;
    }

    protected resServer doInBackground(final String... args) {
        Map<String, String> mapCartoes = new HashMap<String, String>();
        ArrayList<Cartao> _cartoes = new ArrayList<Cartao>();
        _cartoes = _cartaoCtrl.getListaCartaoCtrl();
        String _jsonCartoes = "[ ";

        for (int i = 0; i < _cartoes.size(); i++ ) {
            mapCartoes.put("numeroCartao", getCartaoDecript(String.valueOf(_cartoes.get(i).getNumero()).replace(".", "")).replace(" ",""));
            _jsonCartoes += new GsonBuilder().create().toJson(mapCartoes, Map.class) + ", ";
        }
        _jsonCartoes = _jsonCartoes.substring(0, _jsonCartoes.length() - 2) + "]";

        //String _jsonCartaoSync = new GsonBuilder().create().toJson(mapCartoes, Map.class);
        resServer _res;
        try {
            _res = uploadToServer( Global.getInstance().apiURLSaldos, _jsonCartoes);
        } catch (IOException e) {
            e.printStackTrace();
            return new resServer();
        } catch (JSONException e) {
            e.printStackTrace();
            return new resServer();
        }
        return _res;
    }

    private resServer uploadToServer(String query, String json ) throws IOException, JSONException {
        JSONObject jsonobj = null;
        JSONArray jsonArray = null;
        URL url = new URL(query);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        try {
            conn.setConnectTimeout(5000);
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", "Basic " + Global.getInstance().apiTokenPerfil);
            conn.setRequestProperty("deviceId", Global.getInstance().apiDeviceID);

            OutputStream os = conn.getOutputStream();
            os.write(json.getBytes("UTF-8"));
            os.close();

            InputStream in = new BufferedInputStream(conn.getInputStream());
            byte[] bytes = new byte[1000];

            StringBuilder _reader = new StringBuilder();
            int numRead = 0;
            while ((numRead = in.read(bytes)) >= 0) {
                _reader.append(new String(bytes, 0, numRead));
            }
            //String _listCards = _reader.toString().substring(1);
            //_listCards = _listCards.substring(0, _listCards.length() -1);
            jsonArray = new JSONArray(_reader.toString());
            in.close();
            conn.disconnect();

        } catch (Exception e) {
            e.getMessage();
        } finally {
            resServer _resServer = new resServer();
            _resServer.setHttpStatus(conn.getResponseCode());

            InputStream is = null;
            if (_resServer.getHttpStatus() != 200) {
                if (_resServer.getHttpStatus() > 200 && _resServer.getHttpStatus() < 400) {
                    is = conn.getInputStream();
                } else {
                    is = conn.getErrorStream();
                }

                try {
                    _resServer.setJsonArray(jsonArray);
                } catch (Exception e) {

                }

                try {
                    jsonArray = new JSONArray(Global.getInstance().convertStreamToString(is));
                } catch (Exception e) {
                    jsonArray = null;
                }

                if (null == jsonArray) {
                    jsonArray = new JSONArray("[{\n" +
                            "  \"codigo\": -1,\n" +
                            "  \"mensagem\": \"Erro de autenticação.\"\n" +
                            "}]");
                }
            }
            _resServer.setJsonArray(jsonArray);
            return _resServer;        }
    }

    private String getCartaoDecript(String _numeroCartaoCrip) {
        String result = "";
        try {
            MCrypt mcrypt = new MCrypt();
            //result = mcrypt.bytesToHex(mcrypt.decrypt(_numeroCartaoCrip));
            result = new String( mcrypt.decrypt( _numeroCartaoCrip ) );
        } catch (Exception e) {
            Log.d("Erro", e.getMessage());
        }
        return result;
    }


}
