package br.com.comprocard.meucomprocard;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.net.HttpURLConnection;

import br.com.comprocard.meucomprocard.Controller.PerfilCtrl;
import br.com.comprocard.meucomprocard.DataSync.ResultsListener;
import br.com.comprocard.meucomprocard.DataSync.postPerfilRegister;
import br.com.comprocard.meucomprocard.DataSync.resServer;
import br.com.comprocard.meucomprocard.Model.Perfil;
import br.com.comprocard.meucomprocard.Util.CpfCnpjMask;
import br.com.comprocard.meucomprocard.Util.Global;
import br.com.comprocard.meucomprocard.Util.MCrypt;
import br.com.comprocard.meucomprocard.Util.Mascara;
import br.com.comprocard.meucomprocard.Util.Validacoes;
import br.com.comprocard.meucomprocard.dbHelper.ConexaoSQLite;

import static android.widget.Toast.LENGTH_LONG;

public class perfilActivity extends AppCompatActivity implements ResultsListener {

    private ConexaoSQLite _banco;
    private TextWatcher cpfCnpjMask;
    private EditText _edtNome;
    private EditText _edtEmail;
    private EditText _edtCPF;
    private EditText _edtDataNasc;
    private EditText _edtTelefone;
    private TextView _txtFrase1;
    private TextView _txtFrase2;
    private TextView _btnEnviar;
    private TextView _txtSenha;
    private TextView _txtSenhaConf;
    private TextView _tvTerms;
    private TextView _tvPrivacy;
    private CheckBox _cbTerms;
    private PerfilCtrl _perfilCTRL;
    private Perfil _perfilPreenchido;
    InputMethodManager imm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        _banco = ConexaoSQLite.getInstancia(this);
        _edtNome = (EditText) findViewById(R.id.edtNome);
        _edtEmail = (EditText) findViewById(R.id.edtEmail);
//        _edtDataNasc = (EditText) findViewById(R.id.edtDataNascimento);
        _edtCPF = (EditText) findViewById(R.id.edtCPF);
        _edtTelefone = (EditText) findViewById(R.id.edtTelefone);
        _btnEnviar = (TextView) findViewById(R.id.btnEnviar);
        //_txtSenha = (EditText) findViewById(R.id.edtSenha);
        //_txtSenhaConf = (EditText) findViewById(R.id.edtSenhaConfirm);

        _txtFrase1 = (TextView) findViewById(R.id.txtFrase1);
        _txtFrase2 = (TextView) findViewById(R.id.txtFrase2);
        _tvTerms = (TextView) findViewById(R.id.tv_terms);
        _tvTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://www.comprocard.com.br/docs/meu_comprocard_termo_de_uso.html"));
                startActivity(intent);
            }
        });
        _tvPrivacy = (TextView) findViewById(R.id.tv_privacy);
        _tvPrivacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://www.comprocard.com.br/docs/meu_comprocard_politica_de_privacidade.html"));
                startActivity(intent);
            }
        });
        _cbTerms = (CheckBox) findViewById(R.id.cb_terms);
        _cbTerms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                _btnEnviar.setEnabled(b);
            }
        });

        isConnected();
        Global.getInstance().isEditPerfil = false;

        _perfilCTRL = new PerfilCtrl(_banco);
        _txtFrase1.setText("Bem vindo ao APP");
        _txtFrase2.setText("Por favor informe os dados básicos do portador do cartão para continuar.");
        _btnEnviar.setText("Continuar");
        _edtTelefone.addTextChangedListener(Mascara.insert("(##)#####-####", _edtTelefone));
//        _edtDataNasc.addTextChangedListener(Mascara.insert("##/##/####", _edtDataNasc));
        cpfCnpjMask = CpfCnpjMask.insert(_edtCPF);
        _edtCPF.addTextChangedListener(cpfCnpjMask);

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        _edtCPF.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (_edtCPF.length() > 0) {
                        if (_edtCPF.length() > 14) {
                            if (!Validacoes.validaCNPJ(_edtCPF.getText().toString())) {
                                Toast.makeText(getApplicationContext(), "CNPJ informado inválido!", Toast.LENGTH_LONG).show();
                                _edtCPF.setTextColor(Color.RED);
                            } else {
                                _edtCPF.setTextColor(Color.BLACK);
                            }
                        } else {
                            if (!Validacoes.validaCPF(_edtCPF.getText().toString())) {
                                Toast.makeText(getApplicationContext(), "CPF informado inválido!", Toast.LENGTH_LONG).show();
                                _edtCPF.setTextColor(Color.RED);
                            } else {
                                _edtCPF.setTextColor(Color.BLACK);
                            }
                        }
                    }
                }
            }
        });

//        _edtDataNasc.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//            if (!hasFocus &&  _edtDataNasc.length() > 0) {
//                if (!Validacoes.isValidDate(_edtDataNasc.getText().toString())) {
//                    Toast.makeText(getApplicationContext(), "Data inválida!", Toast.LENGTH_LONG).show();
//                    _edtDataNasc.setTextColor(Color.RED);
//                } else {
//                    _edtDataNasc.setTextColor(Color.BLACK);
//                }
//            }
//            }
//        });

        _edtEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus && _edtEmail.length() > 0) {
                    if (!Validacoes.isValidEmail(_edtEmail.getText().toString())) {
                        Toast.makeText(getApplicationContext(), "E-mail inválido!", Toast.LENGTH_LONG).show();
                        _edtEmail.setTextColor(Color.RED);
                    } else {
                        _edtEmail.setTextColor(Color.BLACK);
                    }
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (Global.getInstance().isEditPerfil == false) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater) perfilActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View titleView = inflater.inflate(R.layout.custom_dialog, null);
            ((TextView) titleView.findViewById(R.id.partName)).setText(" Meu ComproCard");
            builder.setCustomTitle(titleView);
            builder.setMessage("Você ainda não fez seu registro. Sair assim mesmo?");
            builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    finish();
                    moveTaskToBack(true);
                    android.os.Process.killProcess(android.os.Process.myPid());
                }
            });
            builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        } else {
            finish();
        }
    }

    public void putPerfil(View paramView) {
        if (Global.getInstance().isEditPerfil == false) {
            if (isConnected() == true) {
                if (validaFormulario()) {
                    _perfilPreenchido = getDadosPerfilFromFormulario();
                    _perfilPreenchido.setIMEI(getDeviceUniqueID(this));
                    //_perfilPreenchido.setSenha("9999"); //<-- Não é usado por enquanto
                    postPerfilRegister _taskPostPerfil = new postPerfilRegister(Global.getInstance().apiURLRegister, _perfilPreenchido, perfilActivity.this);
                    _taskPostPerfil.setOnResultsListener(this);
                    _taskPostPerfil.execute();
                }
            }
        } else {
            finish();
        }
    }

    public void callAjuda(View paramView) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(perfilActivity.this);
        LayoutInflater inflater = perfilActivity.this.getLayoutInflater();
        View alertView = inflater.inflate(R.layout.activity_ajuda, null);
        alertDialog.setView(alertView);
        final AlertDialog show = alertDialog.show();
        TextView alertButton = (TextView) alertView.findViewById(R.id.btnEnviar);
        alertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show.dismiss();
            }
        });
    }

    public void onResultsSucceeded(resServer result) {
        int httpStatus;
        JSONObject _json;
        String _mensagem = "";

        try {
            httpStatus = result.getHttpStatus();
            _json = result.getJsonBody();

            if (httpStatus == HttpURLConnection.HTTP_OK) {
                _perfilPreenchido.setToken(_json.getString("token"));
                Global.getInstance().apiTokenPerfil = _json.getString("token");
                Global.getInstance().apiDeviceID = _perfilPreenchido.getIMEI();
                _perfilPreenchido.setAtivo(1);
                salvarPerfil(_perfilPreenchido);
                finish();

//                Intent intent = new Intent(getApplicationContext(), cardAlertActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//                startActivity(intent);
            } else {
                _mensagem = _json.getString("mensagem");
                if (_mensagem != "") {
                    Toast.makeText(perfilActivity.this, _mensagem, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(perfilActivity.this, "Tente novamente mais tarde", Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            Toast.makeText(perfilActivity.this, "Falha na conexão de internet", LENGTH_LONG).show();
            return;
        }
    }

    public void salvarPerfil(Perfil _perfil) {
        _perfilCTRL.salvarPerfilCtrl(_perfil, false);
    }

    private boolean validaFormulario() {

        if (this._edtNome.getText().toString().trim().length() == 0) {
            if (imm != null) {
                imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
            }
            Toast.makeText(getApplicationContext(), "Campo Nome deve ser preenchido!", Toast.LENGTH_LONG).show();
            this._edtNome.requestFocus();
            return false;
        }

        if (this._edtTelefone.getText().toString().trim().length() == 0) {
            if (imm != null) {
                imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
            }
            Toast.makeText(getApplicationContext(), "Campo Telefone deve ser preenchido!", Toast.LENGTH_LONG).show();
            this._edtTelefone.requestFocus();
            return false;
        }

        if (this._edtEmail.getText().toString().trim().length() == 0) {
            if (imm != null) {
                imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
            }
            Toast.makeText(getApplicationContext(), "Campo E-Mail deve ser preenchido!", Toast.LENGTH_LONG).show();
            this._edtEmail.requestFocus();
            return false;
        }

        if (this._edtEmail.getText().toString().length() > 0) {
            if (!Validacoes.isValidEmail(this._edtEmail.getText().toString())) {
                Toast.makeText(getApplicationContext(), "E-Mail inválido!", Toast.LENGTH_LONG).show();
                this._edtEmail.requestFocus();
                if (imm != null) {
                    imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }
                return false;
            }
        }

        if (this._edtCPF.getText().toString().trim().length() == 0) {
            if (imm != null) {
                imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
            }
            Toast.makeText(getApplicationContext(), "Campo CPF/CNPJ deve ser preenchido!", Toast.LENGTH_LONG).show();
            this._edtCPF.requestFocus();
            return false;
        }

        if (this._edtCPF.getText().toString().length() > 0) {
            if (!Validacoes.validaCPF(this._edtCPF.getText().toString())) {
                if (!Validacoes.validaCNPJ(this._edtCPF.getText().toString())) {
                    Toast.makeText(getApplicationContext(), "CPF/CNPJ inválido!", Toast.LENGTH_LONG).show();
                    this._edtCPF.requestFocus();
                    if (imm != null) {
                        imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
                    }
                    return false;
                }
            }
        }
//
//        if (this._edtDataNasc.getText().toString().trim().length() == 0) {
//            if (imm != null) {
//                imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
//            }
//            Toast.makeText(getApplicationContext(), "Campo Data de Nascimento deve ser preenchido!", Toast.LENGTH_LONG).show();
//            this._edtDataNasc.requestFocus();
//            return false;
//        }

//        if (this._edtDataNasc.getText().toString().length() > 0) {
//            if (!Validacoes.isValidDate(this._edtDataNasc.getText().toString())) {
//                Toast.makeText(getApplicationContext(), "Data de Nascimento inválida!", Toast.LENGTH_LONG).show();
//                this._edtDataNasc.requestFocus();
//                if (imm != null) {
//                    imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
//                }
//                return false;
//            }
//        }
/*
        if (this._txtSenha.getText().toString().trim().length() == 0) {
            if (imm != null) {
                imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
            }
            Toast.makeText(getApplicationContext(), "Campo Senha deve ser preenchido!", Toast.LENGTH_LONG).show();
            this._txtSenha.requestFocus();
            return false;
        }

        if (this._txtSenhaConf.getText().toString().trim().length() == 0) {
            if (imm != null) {
                imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
            }
            Toast.makeText(getApplicationContext(), "Campo Confirmar Senha deve ser preenchido!", Toast.LENGTH_LONG).show();
            this._txtSenhaConf.requestFocus();
            return false;
        }

        if (!this._txtSenha.getText().toString().equals(this._txtSenhaConf.getText().toString())) {
            if (imm != null) {
                imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
            }
            Toast.makeText(getApplicationContext(), "Senhas não conferem!", Toast.LENGTH_LONG).show();
            this._txtSenhaConf.requestFocus();
            return false;
        }*/

        return true;
    }

    private Perfil getDadosPerfilFromFormulario() {
        Perfil _perfil = new Perfil();
        _perfil.setID(_banco.ID_PERFIL);
        _perfil.setNome(this._edtNome.getText().toString());
        _perfil.setTelefone(this._edtTelefone.getText().toString());
        _perfil.setEmail(this._edtEmail.getText().toString());
        _perfil.setCPF(this._edtCPF.getText().toString());
//        _perfil.setDataNascimento( this._edtDataNasc.getText().toString() );
        _perfil.setDataNascimento("24/10/1991");
        _perfil.setSenha("2222"); //getSenhaCript(this._txtSenha.getText().toString()) );
        return _perfil;
    }

    public String getDeviceUniqueID(Activity activity) {
        String device_unique_id;
        try {
            device_unique_id = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
        } catch (Exception e) {
            Log.d("Erro", e.getMessage());
            return "";
        }
        return device_unique_id;
    }

    public boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(meuCartaoActivity.CONNECTIVITY_SERVICE);
        NetworkInfo net = cm.getActiveNetworkInfo();
        if (net != null && net.isAvailable() && net.isConnected()) {
            Global.getInstance().isConected = true;
            return true;
        } else {
            Global.getInstance().isConected = false;
            Toast.makeText(getApplicationContext(), "Por favor conecte-se a internet", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    private String getSenhaCript(String _SenhaPerfil) {
        String encrypted = _SenhaPerfil;
        try {
            MCrypt mcrypt = new MCrypt();
            encrypted = mcrypt.bytesToHex(mcrypt.encrypt(_SenhaPerfil));
        } catch (Exception e) {
            Log.d("Erro", e.getMessage());
            encrypted = "";
        }
        return encrypted;
    }
}
