package br.com.comprocard.meucomprocard.Model.api;

import java.io.Serializable;

public class HistoricoResponse implements Serializable {
    private String data;
    private String estabelecimento;
    private float parcelas;
    private String estornada;
    private float valor;
    private String dataFormatada;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getEstabelecimento() {
        return estabelecimento;
    }

    public void setEstabelecimento(String estabelecimento) {
        this.estabelecimento = estabelecimento;
    }

    public float getParcelas() {
        return parcelas;
    }

    public void setParcelas(float parcelas) {
        this.parcelas = parcelas;
    }

    public String getEstornada() {
        return estornada;
    }

    public void setEstornada(String estornada) {
        this.estornada = estornada;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public String getDataFormatada() {
        return dataFormatada;
    }

    public void setDataFormatada(String dataFormatada) {
        this.dataFormatada = dataFormatada;
    }
}
