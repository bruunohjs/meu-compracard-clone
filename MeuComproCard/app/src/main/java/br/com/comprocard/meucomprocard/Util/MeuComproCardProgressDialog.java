package br.com.comprocard.meucomprocard.Util;

/**
 * Created by andre.cardoso on 21/12/2016.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.widget.ImageView;

import br.com.comprocard.meucomprocard.R;

public class MeuComproCardProgressDialog extends ProgressDialog {
    private AnimationDrawable animation;

    public static ProgressDialog ctor(Context context) {
        MeuComproCardProgressDialog dialog = new MeuComproCardProgressDialog(context);
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        return dialog;
    }

    public MeuComproCardProgressDialog(Context context) {
        super(context);
    }

    public MeuComproCardProgressDialog(Context context, int theme) {
        super(context, theme);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.view_custom_progress_dialog);
        setProgressStyle(ProgressDialog.STYLE_SPINNER);
        setMessage("Por favor aguarde...");
        /*ImageView la = (ImageView) findViewById(R.id.animation);
        la.setBackgroundResource(R.drawable.custom_progress_dialog_animation);
        animation = (AnimationDrawable) la.getBackground();*/
    }
//
//    @Override
//    public void show() {
//        super.show();
//        //animation.start();
//    }
//
//    @Override
//    public void dismiss() {
//        super.dismiss();
//        //animation.stop();
//    }

}
