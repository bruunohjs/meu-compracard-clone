package br.com.comprocard.meucomprocard.Model.api;

public class CartaoSaldo {

    public CartaoSaldo(String numeroCartao) {
        this.numeroCartao = numeroCartao;
    }

    private String numeroCartao;

    public String getNumeroCartao() {
        return numeroCartao;
    }

    public void setNumeroCartao(String numeroCartao) {
        this.numeroCartao = numeroCartao;
    }
}
