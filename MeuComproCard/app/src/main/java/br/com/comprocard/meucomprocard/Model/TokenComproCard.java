package br.com.comprocard.meucomprocard.Model;

/**
 * Created by andre.cardoso on 26/06/2017.
 */

public class TokenComproCard {

    private String CPF;
    private String IMEI;
    private String Token;

    public String getCPF() {
        return CPF;
    }

    public void setCPF(String CPF) {
        this.CPF = CPF;
    }

    public String getIMEI() {
        return IMEI;
    }

    public void setIMEI(String IMEI) {
        this.IMEI = IMEI;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    @Override
    public String toString() {
        return "Perfil [CPF="+ getCPF() +", IMEI="+ getIMEI() +", Token="+ getToken() +"]";
    }

}
