package br.com.comprocard.meucomprocard.DAO;

import java.math.BigDecimal;
import java.util.ArrayList;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.com.comprocard.meucomprocard.dbHelper.ConexaoSQLite;
import br.com.comprocard.meucomprocard.Model.Historico;
import br.com.comprocard.meucomprocard.Util.Data;

public class HistoricoDAO {
	private ConexaoSQLite sqliteDataBase;

	private final String[] COLUNAS_ARRAY = { "Id", "cartaoID", "DataHora", "Valor", "Tipo", "Descricao", "Parcela" };
	private Data datas;

	public HistoricoDAO(ConexaoSQLite pSqliteDataBase) {
		this.sqliteDataBase = pSqliteDataBase;
	}

	public long salvarHistoricoDAO(Historico historico) {
		long Id = 0;
		SQLiteDatabase db = this.sqliteDataBase.getWritableDatabase();
		try {
			ContentValues values = new ContentValues();
			values.put("cartaoID", historico.getCartaoID());
			values.put("DataHora", historico.getDataHora());
			values.put("Valor", String.valueOf(historico.getValor()));
			values.put("Tipo", historico.getTipo());
			values.put("Descricao", historico.getDescricao());
			values.put("Parcela", String.valueOf(historico.getParcela()));
			Id = db.insert("Historico", null, values);
		} catch (Exception e) {
			Log.d("DB_ERROR", "NAO FOI POSSIVEL INSERIR HISTORICO");
		} finally {
			db.close();
		}
		return Id;
	}

	public Historico getHistoricoDAO(int pId) {
		Historico historico = new Historico();
		SQLiteDatabase db = this.sqliteDataBase.getReadableDatabase();
		Cursor cursor = null;
		try {
			cursor = db.query("Historico", COLUNAS_ARRAY, " Id = ?", new String[] { String.valueOf(pId) }, null,null,null,null);
			if (cursor != null) {
				cursor.moveToFirst();
			}
			historico.setID(Integer.parseInt(cursor.getString( cursor.getColumnIndex("Id") )));
			historico.setCartaoID(Integer.parseInt(cursor.getString( cursor.getColumnIndex("cartaoID") )));
			historico.setDataHora(cursor.getString(cursor.getColumnIndex("DataHora")));
			historico.setValor(new BigDecimal(cursor.getString(cursor.getColumnIndex("Valor"))));
			historico.setTipo(cursor.getString(cursor.getColumnIndex("Tipo")));
			historico.setDescricao(cursor.getString(cursor.getColumnIndex("Descricao")));
			historico.setParcela(Integer.parseInt(cursor.getString(cursor.getColumnIndex("Parcela"))));
		} catch (Exception e) {
			Log.d("DB_ERROR", "NAO FOI POSSIVEL RECUPERAR O HISTORICO");
		} finally {
			cursor.close();
				db.close();
		}
		return historico;
	}

	public ArrayList<Historico> getListaHistoricoDAO() {
		Historico historico = null;
		ArrayList<Historico> listaHistorico = new ArrayList<Historico>();
		SQLiteDatabase db = null;
		Cursor cursor = null;
		String query = "SELECT * FROM Historico ORDER BY DataHora ";
		try {
			db = this.sqliteDataBase.getReadableDatabase();
			cursor = db.rawQuery(query, null);
			if (cursor.moveToFirst()) {
			do {
			historico = new Historico();
			historico.setID(Integer.parseInt(cursor.getString( cursor.getColumnIndex("Id") )));
			historico.setCartaoID(Integer.parseInt(cursor.getString( cursor.getColumnIndex("cartaoID") )));
			historico.setDataHora(cursor.getString(cursor.getColumnIndex("DataHora")));
			historico.setValor(new BigDecimal(cursor.getString(cursor.getColumnIndex("Valor"))));
			historico.setTipo(cursor.getString(cursor.getColumnIndex("Tipo")));
			historico.setDescricao(cursor.getString(cursor.getColumnIndex("Descricao")));
			historico.setParcela(Integer.parseInt(cursor.getString(cursor.getColumnIndex("Parcela"))));
			listaHistorico.add(historico);
			} while (cursor.moveToNext());
		}
		} catch (Exception e) {
			Log.d("DB_ERROR", "NAO FOI POSSIVEL LISTAR HISTORICO");
		} finally {
			cursor.close();
			db.close();
		}
			return listaHistorico;
	}

	public ArrayList<Historico> getListaHistoricoDAOByCartaoID(String _idCartao) {
		Historico historico = null;
		ArrayList<Historico> listaHistorico = new ArrayList<Historico>();
		SQLiteDatabase db = null;
		Cursor cursor = null;
		String query = "SELECT * FROM Historico WHERE cartaoID = "+ _idCartao +" ORDER BY DataHora ";
		try {
			db = this.sqliteDataBase.getReadableDatabase();
			cursor = db.rawQuery(query, null);
			if (cursor.moveToFirst()) {
				do {
					historico = new Historico();
					historico.setID(Integer.parseInt(cursor.getString( cursor.getColumnIndex("Id") )));
					historico.setCartaoID(Integer.parseInt(cursor.getString( cursor.getColumnIndex("cartaoID") )));
					historico.setDataHora(cursor.getString(cursor.getColumnIndex("DataHora")));
					historico.setValor(new BigDecimal(cursor.getString(cursor.getColumnIndex("Valor"))));
					historico.setTipo(cursor.getString(cursor.getColumnIndex("Tipo")));
					historico.setDescricao(cursor.getString(cursor.getColumnIndex("Descricao")));
					historico.setParcela(Integer.parseInt(cursor.getString(cursor.getColumnIndex("Parcela"))));
					listaHistorico.add(historico);
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			Log.d("DB_ERROR", "NAO FOI POSSIVEL LISTAR HISTORICO");
		} finally {
			cursor.close();
			db.close();
		}
		return listaHistorico;
	}

	public ArrayList<Historico> getListaHistoricoTestDAO() {
		Historico historico = null;
		ArrayList<Historico> listaHistorico = new ArrayList<Historico>();

		historico = new Historico();
		historico.setID(1);
		historico.setCartaoID(111222);
		historico.setDataHora("30/11/2016 00:51:15");
		historico.setTipo("C");
		historico.setDescricao("Saldo Inicial");
		historico.setParcela(1);
		historico.setValor(new BigDecimal("600.00"));
		listaHistorico.add(historico);


		for (int i = 2; i < 60; i++) {
			historico = new Historico();
			historico.setID(i);
			historico.setCartaoID(111222);
			historico.setDataHora("30/11/2016 00:51:15");
			historico.setTipo("D");
			historico.setDescricao("SUPERMERCADO PERIM");
			historico.setParcela(1);
			historico.setValor(new BigDecimal("8.55"));
			listaHistorico.add(historico);

			if (i == 27){
				historico = new Historico();
				historico.setID(1);
				historico.setCartaoID(111222);
				historico.setDataHora("30/11/2016 00:51:15");
				historico.setTipo("C");
				historico.setDescricao("Lançamento de crédito");
				historico.setParcela(1);
				historico.setValor(new BigDecimal("800.00"));
				listaHistorico.add(historico);
			}

		}
		return listaHistorico;
	}

	public boolean excluirHistoricoDAO() {
		SQLiteDatabase db = null;
		try {
		db = this.sqliteDataBase.getWritableDatabase();
		db.delete(
			"Historico",
			null,
			null);
		} catch (Exception e) {
			Log.d("DB_ERROR", "NAO FOI POSSIVEL DELETAR HISTORICO");
			return false;
		} finally {
			db.close();
		}
		return true;
	}

	public boolean excluirHistoricoDAOByCartaoID(String _IDCartao) {
		SQLiteDatabase db = null;
		try {
			db = this.sqliteDataBase.getWritableDatabase();
			db.delete(
					"Historico",
					"cartaoID = ?",
					new String[] { _IDCartao });
		} catch (Exception e) {
			Log.d("DB_ERROR", "NAO FOI POSSIVEL DELETAR HISTORICO");
			return false;
		} finally {
			db.close();
		}
		return true;
	}


}