package br.com.comprocard.meucomprocard;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;
import org.json.JSONObject;

import java.io.Serializable;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import br.com.comprocard.meucomprocard.Controller.CartaoCtrl;
import br.com.comprocard.meucomprocard.Controller.PerfilCtrl;
import br.com.comprocard.meucomprocard.DataSync.ResultsListener;
import br.com.comprocard.meucomprocard.DataSync.ResultsListenerRefresh;
import br.com.comprocard.meucomprocard.DataSync.getImagemCartao;
import br.com.comprocard.meucomprocard.DataSync.postAutentica;
import br.com.comprocard.meucomprocard.DataSync.postCartaoLogin;
import br.com.comprocard.meucomprocard.DataSync.resServer;
import br.com.comprocard.meucomprocard.Model.Cartao;
import br.com.comprocard.meucomprocard.Model.Perfil;
import br.com.comprocard.meucomprocard.Util.Global;
import br.com.comprocard.meucomprocard.Util.ImagensCartaoHelper;
import br.com.comprocard.meucomprocard.Util.MCrypt;
import br.com.comprocard.meucomprocard.Util.Mascara;
import br.com.comprocard.meucomprocard.dbHelper.ConexaoSQLite;

import static android.widget.Toast.LENGTH_LONG;

public class incluirCartaoActivity extends AppCompatActivity implements
        ResultsListener, ResultsListenerRefresh, Serializable {

    private EditText _edtCartao;
    InputMethodManager imm;
    private Cartao _cartaoPreenchido;
    ResultsListenerRefresh listener;
    private ConexaoSQLite _banco;
    private Perfil _perfil;
    private PerfilCtrl _perCTRL;
    private CartaoCtrl _cartaoCTRL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incluir_cartao);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Incluir cartão");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        isConnected();
        _banco = ConexaoSQLite.getInstancia(this);
        _perCTRL = new PerfilCtrl(_banco);
        _cartaoCTRL = new CartaoCtrl(_banco);
        _perfil = _perCTRL.getPerfilCtrl(1);
        _edtCartao = (EditText) findViewById(R.id.edNumCartao);
        _edtCartao.addTextChangedListener(Mascara.insert("####.####.####.####", _edtCartao));
    }

    public void incluirCartao(View paramView) {
        if (isConnected() == true)
            if (validaCliFormulario()) {
                _cartaoPreenchido = getDadosCartaoFromFormulario();
                postAutentica _taskAutentica = new postAutentica(Global.getInstance().apiURLAutenticacao, _perfil, incluirCartaoActivity.this, true);
                _taskAutentica.setOnResultsListener(incluirCartaoActivity.this);
                _taskAutentica.execute();
            }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home ) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }


    /*@Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(getApplicationContext(), "On Start...", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Toast.makeText(getApplicationContext(), "On Restart...", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Toast.makeText(getApplicationContext(), "On Stop...", Toast.LENGTH_LONG).show();
    }*/

    private Cartao getDadosCartaoFromFormulario() {
        Cartao _cartao = new Cartao();
        _cartao.setNumero ( this._edtCartao.getText().toString() );
        return _cartao;
    }

    private boolean validaCliFormulario() {
        if (this._edtCartao.getText().toString().trim().length() == 0) {
            if (imm != null) {
                imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
            }
            Toast.makeText(getApplicationContext(), "Número do Cartão deve ser preenchido!", Toast.LENGTH_LONG).show();
            this._edtCartao.requestFocus();
            return false;
        }

        if ( null == this._edtCartao.getText()){
            Toast.makeText(getApplicationContext(), "Cartão inválido. Insira um cartão ComproCard válido!", Toast.LENGTH_LONG).show();
            this._edtCartao.requestFocus();
            return false;
        }

        if (this._edtCartao.getText().length() < 19){
            Toast.makeText(getApplicationContext(), "Cartão inválido. Insira um cartão ComproCard válido!", Toast.LENGTH_LONG).show();
            this._edtCartao.requestFocus();
            return false;
        }

        try {
            if ((!this._edtCartao.getText().toString().substring(0, 7).replace(".", "").equals("639231")) && (!this._edtCartao.getText().toString().substring(0, 7).replace(".", "").equals("639233"))) {
                if (imm != null) {
                    imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
                }
                Toast.makeText(getApplicationContext(), "Cartão inválido. Insira um cartão ComproCard válido!", Toast.LENGTH_LONG).show();
                this._edtCartao.requestFocus();
                return false;
            }
        } catch (Exception e){
            Toast.makeText(getApplicationContext(), "Cartão inválido. Insira um cartão ComproCard válido!", Toast.LENGTH_LONG).show();
            this._edtCartao.requestFocus();
            return false;
        }
        return true;
    }

    public void onResultsSucceeded(resServer result) {
        int httpStatus;
        JSONObject _json;
        String _mensagem = "";

        try {
            httpStatus = result.getHttpStatus();
            _json = result.getJsonBody();

            if (httpStatus == HttpURLConnection.HTTP_OK) {
                Global.getInstance().apiTokenPerfil = _json.getString("token");
                postCartaoLogin _taskGetCartaoLogin = new postCartaoLogin(Global.getInstance().apiURLLogin, _cartaoPreenchido.getNumero().trim(), incluirCartaoActivity.this);
                _taskGetCartaoLogin.setOnResultsListener(this);
                _taskGetCartaoLogin.execute();
            } else {
                _mensagem = _json.getString("mensagem");
                if (_mensagem != "") {
                    Toast.makeText(incluirCartaoActivity.this, _mensagem, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(incluirCartaoActivity.this, "Tente novamente mais tarde", Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            Toast.makeText(incluirCartaoActivity.this, "Falha na conexão de internet", LENGTH_LONG).show();
            return;
        }
    }

    public void onResultsSucceededRefresh(resServer result) {
        int httpStatus;
        JSONObject _json;
        JSONObject _jsonCartao;
        String _jsonToken;
        String _jsonExpLoginCartao;
        String _mensagem = "";
        ImagensCartaoHelper _imgHelper = new ImagensCartaoHelper();

        try {
            httpStatus = result.getHttpStatus();
            _json = result.getJsonBody();

            if (httpStatus == HttpURLConnection.HTTP_OK) {
                _jsonCartao = _json.getJSONObject("cartao");
                _jsonToken  = _json.getString("token");
                _jsonExpLoginCartao = _json.getString("dataExpiracao").replaceAll("T","");
                Global.getInstance().apiTokenCartao = _jsonToken;

                Cartao _cartaoServer = _cartaoCTRL.getCartaoCtrlByCartaoId(Integer.parseInt(_jsonCartao.getString("CartaoId")));
                if (null == _cartaoServer) {
                    _cartaoServer = new Cartao();
                    _cartaoServer.setCartaoId(Integer.parseInt(_jsonCartao.getString("CartaoId")));
                    _cartaoServer.setBandeira("ComproCard");
                    _cartaoServer.setProduto(_jsonCartao.getString("ProdutoDesc"));
                    _cartaoServer.setEmpresa(_jsonCartao.getString("EmpresaNome"));
                    _cartaoServer.setNome(_jsonCartao.getString("CartaoNome"));
                    _cartaoServer.setNumero(getCartaoCript(_jsonCartao.getString("CartaoNumero").toString()));
                    _cartaoServer.setCartaoNumFmt(_jsonCartao.getString("CartaoNumFmt").toString());
                    _cartaoServer.setUltimaConsulta(_jsonCartao.getString("dataUltimaConsulta"));
                    _cartaoServer.setValidade("31/05/2095");
                    _cartaoServer.setCVV("999");
                    _cartaoServer.setSaldo(new BigDecimal(_jsonCartao.getString("CartaoSaldo")));
                    _cartaoServer.setLimite(new BigDecimal(_jsonCartao.getString("CartaoLimite")));
                    _cartaoServer.setDiaVencimento(Integer.parseInt(_jsonCartao.getString("CartaoVencto")));
                    _cartaoServer.setSituacao(_jsonCartao.getString("SituacaoDescr"));
                    _cartaoServer.setSenha(_jsonCartao.getString("exigeSenha"));
                    _cartaoServer.setToken(Global.getInstance().apiTokenCartao);
                    _cartaoServer.setUltimaConsulta(_jsonCartao.getString("dataUltimaConsulta"));

                    //String URLbase  = _jsonCartao.getString("ImagemCartao").substring(0, _jsonCartao.getString("ImagemCartao").lastIndexOf("/") + 1 );
                    //String fileName = _jsonCartao.getString("ImagemCartao").substring(_jsonCartao.getString("ImagemCartao").lastIndexOf("/") +1, _jsonCartao.getString("ImagemCartao").length());
                    String versaoImagem  = getResources().getString(R.string.versaoImg);
                    //URLbase += versaoImagem + "/" + fileName;

                    /*if (null != URLbase) {
                        if (!URLbase.equals("")) {
                            _cartaoServer.setVersaoImagemCartao(versaoImagem);
                            _cartaoServer.setImagemCartao(_imgHelper.getCaminhoImagenCartaoByURL(URLbase));
                            getImagemCartao _imgTask = new getImagemCartao(URLbase, incluirCartaoActivity.this);
                            _imgTask.execute();
                        }
                    }*/

                    salvarCartao(_cartaoServer);

                    setResult(RESULT_OK);
                    finish();
                } else {
                    Toast.makeText(incluirCartaoActivity.this, "Este cartão já consta na sua lista!", Toast.LENGTH_LONG).show();
                }
            } else {
                _mensagem = _json.getString("mensagem");
                if (_mensagem != "") {
                    Toast.makeText(incluirCartaoActivity.this, _mensagem, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(incluirCartaoActivity.this, "Tente novamente mais tarde", Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            Toast.makeText(incluirCartaoActivity.this, "Falha na conexão de rede.", LENGTH_LONG).show(); //+ e.getMessage()
            return;
        }
    }

    public void salvarCartao(Cartao _cartao) {
        _cartaoCTRL = new CartaoCtrl(_banco);
        _cartaoCTRL.salvarCartaoCtrl(_cartao, "");
    }

    private String getCartaoCript(String _numeroCartao) {
        String encrypted = _numeroCartao;
        try {
            MCrypt mcrypt = new MCrypt();
            encrypted = mcrypt.bytesToHex(mcrypt.encrypt(_numeroCartao));
        } catch (Exception e) {
            Log.d("Erro", e.getMessage());
        }
        return encrypted;
    }

    public boolean isConnected()
    {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(meuCartaoActivity.CONNECTIVITY_SERVICE);
        NetworkInfo net = cm.getActiveNetworkInfo();
        if (net!=null && net.isAvailable() && net.isConnected()) {
            Global.getInstance().isConected = true;
            return true;
        } else {
            Global.getInstance().isConected = false;
            Toast.makeText(getApplicationContext(), "Por favor conecte-se a internet", Toast.LENGTH_LONG).show();
            return false;
        }
    }



}
