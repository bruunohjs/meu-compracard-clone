package br.com.comprocard.meucomprocard;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.squareup.picasso.Picasso;
import org.json.JSONObject;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.text.DecimalFormat;
import br.com.comprocard.meucomprocard.Controller.CartaoCtrl;
import br.com.comprocard.meucomprocard.Controller.PerfilCtrl;
import br.com.comprocard.meucomprocard.DataSync.ResultsBloqDesbloq;
import br.com.comprocard.meucomprocard.DataSync.ResultsListener;
import br.com.comprocard.meucomprocard.DataSync.ResultsListenerRefresh;
import br.com.comprocard.meucomprocard.DataSync.postAutentica;
import br.com.comprocard.meucomprocard.DataSync.postCartaoBloqDesbq;
import br.com.comprocard.meucomprocard.DataSync.postCartaoLogin;
import br.com.comprocard.meucomprocard.DataSync.resServer;
import br.com.comprocard.meucomprocard.Model.Cartao;
import br.com.comprocard.meucomprocard.Model.Perfil;
import br.com.comprocard.meucomprocard.Util.Global;
import br.com.comprocard.meucomprocard.Util.MCrypt;
import br.com.comprocard.meucomprocard.dbHelper.ConexaoSQLite;

import static android.widget.Toast.LENGTH_LONG;

public class cartaoDetalhePopup extends AppCompatActivity implements ResultsListener, ResultsListenerRefresh, ResultsBloqDesbloq {

    private ConexaoSQLite _banco;
    private int _idCartao;
    private TextView _CartaoNumero;
    private TextView _CartaoNome;
    private TextView _Saldo;
    private CartaoCtrl _carCTRL;
    private Cartao _cartao;
    private Perfil _perfil;
    private PerfilCtrl _perCTRL;
    private DrawerLayout mDrawerLayout;
    private ImageView _imgCartao;
    private TextView _UltimaConsulta;
    private TextView _Situacao;
    private String _acaoCartao;
    private TextView _tvEmpresa;
    private TextView _tvLimite;
    private TextView _tvType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cartao_detalhe_popup);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Detalhe do cartão");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        _banco = ConexaoSQLite.getInstancia(this);
        Bundle extras = getIntent().getExtras();
        _idCartao = Integer.parseInt(extras.getString("ID_CARTAO"));
        _carCTRL = new CartaoCtrl(_banco);
        _cartao = _carCTRL.getCartaoCtrlByCartaoId( _idCartao );

        _CartaoNumero = (TextView) findViewById(R.id.tvNumCartao);
        _CartaoNome = (TextView) findViewById(R.id.tvTitular);
        _Saldo = (TextView) findViewById(R.id.tvSaldoValor);
        _imgCartao = (ImageView) findViewById(R.id.imageCard);
        _UltimaConsulta = (TextView) findViewById(R.id.tvConsultaD);
        _Situacao = (TextView) findViewById(R.id.tvSituacaoD);
        _tvEmpresa = (TextView) findViewById(R.id.tvEmpresa);
        _tvLimite = (TextView) findViewById(R.id.tvLimite);
        _tvType = (TextView) findViewById(R.id.tvType);

        _banco = ConexaoSQLite.getInstancia(this);
        _perCTRL = new PerfilCtrl(_banco);
        _perfil = _perCTRL.getPerfilCtrl(1);

        loadCartaoTela();
    }

    private void loadCartaoTela() {
        if (null != _cartao) {

            if (_cartao.getSituacao().toUpperCase().equals("ATIVO")) {
                ImageView _btnBloqueio = (ImageView) findViewById(R.id.imgbloqueio);
                _btnBloqueio.setBackgroundResource(R.drawable.bloqueio_bg);
            } else {
                ImageView _btnBloqueio = (ImageView) findViewById(R.id.imgbloqueio);
                _btnBloqueio.setBackgroundResource(R.drawable.desbloqueio_bg);
            }

            try {
                _CartaoNumero.setText(_cartao.getCartaoNumFmt());
                _CartaoNome.setText(_cartao.getNome());
                _UltimaConsulta.setText("Consulta: "+_cartao.getUltimaConsulta());
                _Situacao.setText("Situação: "+_cartao.getSituacao());
                _tvType.setText("Cartão: "+_cartao.getProduto());

                DecimalFormat df = new DecimalFormat("#,###,##0.00");
                _Saldo.setText("Saldo R$ " + df.format(_cartao.getSaldo()));

                if ( _cartao.getLimite().compareTo(BigDecimal.ZERO) > 0 ) {
                    _tvLimite.setText("Limite R$: " + df.format(_cartao.getLimite()));
                    _tvEmpresa.setText("Empresa: "+_cartao.getEmpresa());
                } else {
                    _tvLimite.setText("Empresa: "+_cartao.getEmpresa());
                    _tvEmpresa.setText("");
                }

                if (Global.getInstance().isConected == true) {
                    _Saldo.setPaintFlags(0);
                } else {
                    _Saldo.setPaintFlags(_Saldo.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                }

            } catch (Exception e) {

            }

            /*try {
                Picasso
                        .with(cartaoDetalhePopup.this)
                        .load("file://" + _cartao.getImagemCartao() )
                        .placeholder(R.drawable.cartao_default)
                        .fit()
                        .into(_imgCartao);
            } catch (Exception e) {

            }*/
            System.gc();
        } else {
            Toast.makeText(cartaoDetalhePopup.this, "Não foi possível exibir detalhes deste cartão no momento.", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isConnected()) {
            postAutentica _taskAutentica = new postAutentica(Global.getInstance().apiURLAutenticacao, _perfil, cartaoDetalhePopup.this, false);
            _taskAutentica.setOnResultsListener(cartaoDetalhePopup.this);
            _taskAutentica.execute();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home ) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    public void fechar(View paramView) {
        finish();
    }

    public void BloqDesbloq(View paramView) {
        if (_cartao.getSituacao().toUpperCase().equals("ATIVO")) {
            _acaoCartao = "BLOQ";
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)cartaoDetalhePopup.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View titleView = inflater.inflate(R.layout.custom_dialog, null);
            ((TextView) titleView.findViewById(R.id.partName)).setText(" Meu ComproCard");
            builder.setCustomTitle(titleView);
            builder.setMessage("Bloquear cartão " + _cartao.getCartaoNumFmt() + "?");
            builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    postCartaoBloqDesbq _taskGetCartaoBloqDesbq = new postCartaoBloqDesbq(Global.getInstance().apiURLBloquear, cartaoDetalhePopup.this);
                    _taskGetCartaoBloqDesbq.setOnResultsListener(cartaoDetalhePopup.this);
                    _taskGetCartaoBloqDesbq.execute();
                }
            });
            builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        } else {
            _acaoCartao = "DESBLOQ";

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)cartaoDetalhePopup.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View titleView = inflater.inflate(R.layout.custom_dialog, null);
            ((TextView) titleView.findViewById(R.id.partName)).setText(" Meu ComproCard");
            builder.setCustomTitle(titleView);
            builder.setMessage("Desbloquear cartão " + _cartao.getCartaoNumFmt() + "?");
            builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    postCartaoBloqDesbq _taskGetCartaoBloqDesbq = new postCartaoBloqDesbq(Global.getInstance().apiURLDesbloquear, cartaoDetalhePopup.this);
                    _taskGetCartaoBloqDesbq.setOnResultsListener(cartaoDetalhePopup.this);
                    _taskGetCartaoBloqDesbq.execute();
                }
            });
            builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    public void abreExtrato(View paramView) {
        Intent intent = new Intent(getApplicationContext(), ExtratoActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.putExtra("ID_CARTAO", String.valueOf(_idCartao));
        startActivity(intent);
    }

    public void removerCartao(View paramView) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater)cartaoDetalhePopup.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View titleView = inflater.inflate(R.layout.custom_dialog, null);
        ((TextView) titleView.findViewById(R.id.partName)).setText(" Meu ComproCard");
        builder.setCustomTitle(titleView);
        builder.setMessage("Remover o cartão " + _cartao.getCartaoNumFmt() + " da sua lista de cartões?");
        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (_carCTRL.excluirCartaoCtrl( _idCartao )){
                    Toast.makeText(cartaoDetalhePopup.this, "Cartão removido!", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(cartaoDetalhePopup.this, "Não foi possível remover este cartão no momento.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void onResultsSucceeded(resServer result) {
        int httpStatus;
        JSONObject _json;
        String _mensagem = "";

        try {
            httpStatus = result.getHttpStatus();
            _json = result.getJsonBody();

            if (httpStatus == HttpURLConnection.HTTP_OK) {
                Global.getInstance().apiTokenPerfil = _json.getString("token");
                String _cartaoNumero = getCartaoDecript(String.valueOf(_cartao.getNumero()));
                postCartaoLogin _taskGetCartaoLogin = new postCartaoLogin(Global.getInstance().apiURLLogin, _cartaoNumero, cartaoDetalhePopup.this);
                _taskGetCartaoLogin.setOnResultsListener(this);
                _taskGetCartaoLogin.execute();
            } else {
                _mensagem = _json.getString("mensagem");
                if (_mensagem != "") {
                    Toast.makeText(cartaoDetalhePopup.this, _mensagem, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(cartaoDetalhePopup.this, "Tente novamente mais tarde", Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            Toast.makeText(cartaoDetalhePopup.this, "Falha na conexão de rede.", LENGTH_LONG).show(); //+ e.getMessage()
            System.gc();
            return;
        }
    }

    public void onResultsSucceededRefresh(resServer result) {
        int httpStatus;
        JSONObject _json;
        JSONObject _jsonCartao;
        String _jsonToken;
        String _jsonExpLoginCartao;
        String _mensagem = "";

        try {
            httpStatus = result.getHttpStatus();
            _json = result.getJsonBody();

            if (httpStatus == HttpURLConnection.HTTP_OK) {
                Cartao _cartaoServer = new Cartao();
                _jsonCartao = _json.getJSONObject("cartao");
                _jsonToken  = _json.getString("token");
                _jsonExpLoginCartao = _json.getString("dataExpiracao").replaceAll("T","");
                Global.getInstance().apiTokenCartao = _jsonToken;
                _cartaoServer.setCartaoId(Integer.parseInt(_jsonCartao.getString("CartaoId")));
                _cartaoServer.setBandeira("ComproCard");
                _cartaoServer.setProduto(_jsonCartao.getString("ProdutoDesc"));
                _cartaoServer.setEmpresa(_jsonCartao.getString("EmpresaNome"));
                _cartaoServer.setNome(_jsonCartao.getString("CartaoNome"));
                _cartaoServer.setNumero(getCartaoCript(_jsonCartao.getString("CartaoNumero").toString()));
                _cartaoServer.setCartaoNumFmt(_jsonCartao.getString("CartaoNumFmt").toString());
                _cartaoServer.setUltimaConsulta(_jsonCartao.getString("dataUltimaConsulta"));
                _cartaoServer.setValidade("31/05/2095");
                _cartaoServer.setCVV("999");
                _cartaoServer.setSaldo(new BigDecimal(_jsonCartao.getString("CartaoSaldo")));
                _cartaoServer.setLimite(new BigDecimal(_jsonCartao.getString("CartaoLimite")));
                _cartaoServer.setDiaVencimento(Integer.parseInt(_jsonCartao.getString("CartaoVencto")));
                _cartaoServer.setSituacao(_jsonCartao.getString("SituacaoDescr"));
                _cartaoServer.setSenha(_jsonCartao.getString("exigeSenha"));
                _cartaoServer.setToken(Global.getInstance().apiTokenCartao);
                _cartaoServer.setUltimaConsulta(_jsonCartao.getString("dataUltimaConsulta"));
                //_cartaoServer.setImagemCartao(_jsonCartao.getString("ImagemCartao"));
                salvarCartao(_cartaoServer);
                loadCartaoTela();
            } else {
                _mensagem = _json.getString("mensagem");
                if (_mensagem != "") {
                    Toast.makeText(cartaoDetalhePopup.this, _mensagem, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(cartaoDetalhePopup.this, "Tente novamente mais tarde", Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            Toast.makeText(cartaoDetalhePopup.this, "Falha na conexão de rede.", LENGTH_LONG).show(); //+ e.getMessage()
            System.gc();
            return;
        }
    }

    public void onResultsBloqDesbloq(resServer result) {
        int httpStatus;
        JSONObject _json;
        String _mensagem = "";

        try {
            httpStatus = result.getHttpStatus();
            _json = result.getJsonBody();

            if (httpStatus == HttpURLConnection.HTTP_OK) {
                Cartao _cartaoAtual = _carCTRL.getCartaoCtrlByCartaoId( _idCartao );
                String _mensagemb = "";
                if (_acaoCartao.toUpperCase().equals("BLOQ")){
                    //_cartao.setSituacao("BLOQUEADO");
                    _mensagemb = "Cartão bloqueado!";
                    _cartaoAtual.setSituacao("BLOQUEIO TEMPORÁRIO");
                    ImageView _btnBloqueio = (ImageView) findViewById(R.id.imgbloqueio);
                    _btnBloqueio.setBackgroundResource(R.drawable.desbloqueio_bg);
                } else {
                    //_cartao.setSituacao("ATIVO");
                    _mensagemb = "Cartão desbloqueado!";
                    _cartaoAtual.setSituacao("ATIVO");
                    ImageView _btnBloqueio = (ImageView) findViewById(R.id.imgbloqueio);
                    _btnBloqueio.setBackgroundResource(R.drawable.desbloqueio_bg);
                }
                salvarCartao(_cartaoAtual);
                Toast.makeText(cartaoDetalhePopup.this, _mensagemb, Toast.LENGTH_SHORT).show();
                _cartao = _carCTRL.getCartaoCtrlByCartaoId( _idCartao );
                loadCartaoTela();
            } else {
                _mensagem = _json.getString("mensagem");
                if (_mensagem != "") {
                    Toast.makeText(cartaoDetalhePopup.this, _mensagem, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(cartaoDetalhePopup.this, "Tente novamente mais tarde", Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            Toast.makeText(cartaoDetalhePopup.this, "Falha na conexão de rede.", LENGTH_LONG).show(); //+ e.getMessage()
            System.gc();
            return;
        }
    }


    public void salvarCartao(Cartao _cartao) {
        CartaoCtrl _cartaoCTRL = new CartaoCtrl(_banco);
        _cartaoCTRL.salvarCartaoCtrl(_cartao, String.valueOf(_cartao.getCartaoId()));
    }

    private String getCartaoDecript(String _numeroCartaoCrip) {
        String result = "";
        try {
            MCrypt mcrypt = new MCrypt();
            result = new String( mcrypt.decrypt( _numeroCartaoCrip ) );
        } catch (Exception e) {
            Log.d("Erro", e.getMessage());
        }
        return result;
    }

    private String getCartaoCript(String _numeroCartao) {
        String encrypted = _numeroCartao;
        try {
            MCrypt mcrypt = new MCrypt();
            encrypted = mcrypt.bytesToHex(mcrypt.encrypt(_numeroCartao));
        } catch (Exception e) {
            Log.d("Erro", e.getMessage());
        }
        return encrypted;
    }

    public boolean isConnected()
    {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(meuCartaoActivity.CONNECTIVITY_SERVICE);
        NetworkInfo net = cm.getActiveNetworkInfo();
        if (net!=null && net.isAvailable() && net.isConnected()) {
            Global.getInstance().isConected = true;
            return true;
        } else {
            Global.getInstance().isConected = false;
            Toast.makeText(getApplicationContext(), "Por favor conecte-se a internet", Toast.LENGTH_LONG).show();
            return false;
        }
    }

}
