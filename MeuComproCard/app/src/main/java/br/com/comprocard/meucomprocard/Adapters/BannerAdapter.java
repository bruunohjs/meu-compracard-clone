package br.com.comprocard.meucomprocard.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.comprocard.meucomprocard.DataSync.postCartaoLoginWait;
import br.com.comprocard.meucomprocard.ExtratoActivity;
import br.com.comprocard.meucomprocard.Model.Cartao;
import br.com.comprocard.meucomprocard.Model.api.Publicidade;
import br.com.comprocard.meucomprocard.R;
import br.com.comprocard.meucomprocard.Util.Global;
import br.com.comprocard.meucomprocard.Util.MCrypt;
import br.com.comprocard.meucomprocard.Util.uiUtils;
import br.com.comprocard.meucomprocard.cartaoDetalhePopup;
import br.com.comprocard.meucomprocard.mainComproCardActivity;

public class BannerAdapter extends RecyclerView.Adapter<BannerAdapter.CartaoViewHolder> {

    List<Publicidade> mListData;
    private Context mContext;
    //private ArrayList<Cartao> items;

    public BannerAdapter(Context context, List<Publicidade> items) {
        this.mContext = context;
        this.mListData = items;
    }

    @Override
    public BannerAdapter.CartaoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.banner_item, parent, false);
        return new BannerAdapter.CartaoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final BannerAdapter.CartaoViewHolder viewHolder, int position) {
        Publicidade _banner = mListData.get(position);

        try {
            Picasso.with(this.mContext).
                    load( _banner.getPublicidadeImagemUrlMobile() ).
                    placeholder(R.drawable.banner_default).
                    fit().
                    into(viewHolder._banner_pic);

        } catch (Exception e) { }

        viewHolder._banner_pic.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //
            }
        });
    }

    @Override
    public int getItemCount() {
        return mListData == null ? 0 : mListData.size();
    }

    class CartaoViewHolder extends RecyclerView.ViewHolder {
        private Context context;
        public ImageView _banner_pic;

        public CartaoViewHolder(View itemView) {
            super(itemView);
            _banner_pic = (ImageView) itemView.findViewById(R.id.banner_photo);
        }
    }
}
