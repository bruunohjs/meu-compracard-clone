package br.com.comprocard.meucomprocard.DataSync;

/**
 * Created by andre.cardoso on 12/12/2016.
 */

public interface ResultsBloqDesbloq {
    public void onResultsBloqDesbloq(resServer result);
}
