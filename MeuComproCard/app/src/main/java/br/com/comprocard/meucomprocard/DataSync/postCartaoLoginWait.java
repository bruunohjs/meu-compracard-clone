package br.com.comprocard.meucomprocard.DataSync;

/**
 * Created by Andre on 06/12/2016.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import br.com.comprocard.meucomprocard.Controller.CartaoCtrl;
import br.com.comprocard.meucomprocard.Util.Data;
import br.com.comprocard.meucomprocard.Util.Global;
import br.com.comprocard.meucomprocard.Util.MeuComproCardProgressDialog;
import br.com.comprocard.meucomprocard.dbHelper.ConexaoSQLite;

public class postCartaoLoginWait extends AsyncTask<String, Void, String> {

    private ConexaoSQLite sqliteDataBase;
    private String _localURL;
    private CartaoCtrl _cartaoCtrl;
    private String _numeroCartaoPost;
    private Context mContext;


    public postCartaoLoginWait(String _url, String _cartaoNumero, Context context) {
        this.mContext = context;
        this._localURL = _url;
        this._numeroCartaoPost = _cartaoNumero.replace(".","");
        _cartaoCtrl = new CartaoCtrl(ConexaoSQLite.getInstancia(mContext));
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
    }

    protected String doInBackground(final String... args) {
        Map<String, String> mapCartao = new HashMap<String, String>();
        mapCartao.put("numeroCartao", String.valueOf(_numeroCartaoPost.trim()));
        String _jsonCartaoSync = new GsonBuilder().create().toJson(mapCartao, Map.class);
        String _res;
        try {
            _res = uploadToServer( Global.getInstance().apiURLLogin, _jsonCartaoSync);
            try { Thread.sleep(200); }
            catch (InterruptedException e) { e.printStackTrace(); }
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
        return _res;
    }

    private String uploadToServer(String query, String json ) throws IOException, JSONException {
        JSONObject jsonObj = null;
        URL url = new URL(query);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        try {
            conn.setConnectTimeout(5000);
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", "Basic " + Global.getInstance().apiTokenPerfil);
            conn.setRequestProperty("deviceId", Global.getInstance().apiDeviceID);

            OutputStream os = conn.getOutputStream();
            os.write(json.getBytes("UTF-8"));
            os.close();

            InputStream in = new BufferedInputStream(conn.getInputStream());
            byte[] bytes = new byte[1000];

            StringBuilder _reader = new StringBuilder();
            int numRead = 0;
            while ((numRead = in.read(bytes)) >= 0) {
                _reader.append(new String(bytes, 0, numRead));
            }
            jsonObj = new JSONObject(_reader.toString());
            in.close();
            conn.disconnect();

        } catch (Exception e) {
            e.getMessage();
        } finally {
            if (conn.getResponseCode() == 200) {
                String _jsonToken;
                _jsonToken  = jsonObj.getString("token");
                Global.getInstance().apiTokenCartao = _jsonToken;
            }

            return "";
        }
    }
}
