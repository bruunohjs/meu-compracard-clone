package br.com.comprocard.meucomprocard.Controller;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import br.com.comprocard.meucomprocard.DAO.PerfilDAO;
import br.com.comprocard.meucomprocard.dbHelper.ConexaoSQLite;
import br.com.comprocard.meucomprocard.Model.Perfil;

public class PerfilCtrl {
	private PerfilDAO daoPerfil;

	public PerfilCtrl() {
	} 

	public PerfilCtrl(ConexaoSQLite pSQLite) {
		this.daoPerfil = new PerfilDAO(pSQLite);
	} 

	public long salvarPerfilCtrl(Perfil pPerfil, Boolean atualizar) {
		return this.daoPerfil.salvarPerfilDAO(pPerfil, atualizar);
	}

	public Perfil getPerfilCtrl(int pId) {
		return this.daoPerfil.getPerfilDAO(pId);
	} 

	public ArrayList<Perfil> getListaPerfilCtrl() {
		return this.daoPerfil.getListaPerfilDAO();
	} 

	public boolean excluirPerfilCtrl(long pId) {
		return this.daoPerfil.excluirPerfilDAO(pId);
	}

	public String PerfilToJSON(Perfil paramPerfil){
		JSONObject jsonObject= new JSONObject();
		try {
			jsonObject.put("id", String.valueOf(paramPerfil.getID()));

			return jsonObject.toString();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

}