package br.com.comprocard.meucomprocard;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import java.util.ArrayList;
import java.util.List;

import br.com.comprocard.meucomprocard.Controller.CartaoCtrl;
import br.com.comprocard.meucomprocard.Controller.PerfilCtrl;
import br.com.comprocard.meucomprocard.Fragments.ContatoFragment;
import br.com.comprocard.meucomprocard.Fragments.EnderecoFragment;
import br.com.comprocard.meucomprocard.Fragments.HomeFragment;
import br.com.comprocard.meucomprocard.Fragments.IndicarFragment;
import br.com.comprocard.meucomprocard.Model.Cartao;
import br.com.comprocard.meucomprocard.Model.Perfil;
import br.com.comprocard.meucomprocard.Model.api.AuthRequest;
import br.com.comprocard.meucomprocard.Model.api.AuthResponse;
import br.com.comprocard.meucomprocard.Util.Global;
import br.com.comprocard.meucomprocard.components.ViewPagerNoSwipe;
import br.com.comprocard.meucomprocard.config.RetrofitConfig;
import br.com.comprocard.meucomprocard.dbHelper.ConexaoSQLite;
import br.com.comprocard.meucomprocard.interfaces.CartaoService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity
        implements BottomNavigationView.OnNavigationItemSelectedListener, HomeFragment.OnRefreshMenuItem {

    private static final String TAG = HomeActivity.class.getSimpleName();

    private BottomNavigationView navigationView;

    private ViewPagerNoSwipe vp;
    private VpAdapter adapter;

    private SparseIntArray items;// used for change ViewPager selected item
    private List<Fragment> fragments;// used for ViewPager adapter

    private CartaoCtrl _carCTRL;
    private PerfilCtrl _perCTRL;

    private Perfil _perfil;

    ConexaoSQLite _banco;

    private String token;

    BottomNavigationViewEx bnve;

    private boolean isConsultingCard;
    private ImageView iv;
    private Menu mMenuTop;
    private MenuItem mMenuItem;
    private Animation rotation;

    private String[] permissions = new String[]{
            android.Manifest.permission.INTERNET,
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.ACCESS_NETWORK_STATE,
            android.Manifest.permission.ACCESS_COARSE_LOCATION,
            android.Manifest.permission.ACCESS_FINE_LOCATION
            //android.Manifest.permission.READ_PHONE_STATE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        vp = findViewById(R.id.vp);
        token = FirebaseInstanceId.getInstance().getToken();
        loadDefaultParams();
        checkPermissions();
        _banco = ConexaoSQLite.getInstancia(this);
        SQLiteDatabase _db = _banco.getWritableDatabase();
        _banco.onUpgrade(_db, 1, 2);
        isConsultingCard = false;
        //trustAllCertificates();
        _perCTRL = new PerfilCtrl(_banco);
        _carCTRL = new CartaoCtrl(_banco);
        _perfil = _perCTRL.getPerfilCtrl(1);
        Global.getInstance().apiDeviceID = _perfil.getIMEI();
        Global.getInstance().apiTokenPerfil = _perfil.getToken();

        bnve = findViewById(R.id.bnve_center_icon_only);
        initCenterIconOnly();

        initData();
        initEvent();

        if (null != _perfil) {
            if (_perfil.getID() == 0) {

                Global.getInstance().isEditPerfil = false;
                Intent intent = new Intent(getApplicationContext(), perfilActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);

            } else {

                ArrayList<Cartao> _cartaoes = _carCTRL.getListaCartaoCtrl();
//                if (_cartaoes.size() == 0) {
//                    Intent intent = new Intent(getApplicationContext(), cardAlertActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//                    startActivity(intent);
//                }
            }
        }
    }

    /*public void trustAllCertificates() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        public X509Certificate[] getAcceptedIssuers() {
                            X509Certificate[] myTrustedAnchors = new X509Certificate[0];
                            return myTrustedAnchors;
                        }

                        @Override
                        public void checkClientTrusted(X509Certificate[] certs, String authType) {
                        }

                        @Override
                        public void checkServerTrusted(X509Certificate[] certs, String authType) {
                        }
                    }
            };

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String arg0, SSLSession arg1) {
                    return true;
                }
            });
        } catch (Exception e) {
        }
    }*/

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_home:
                break;
            case R.id.nav_rede:
                break;
            case R.id.nav_incluir:
                break;
            case R.id.nav_indicar:
                break;
            case R.id.nav_contato:
                break;
        }

        return true;
    }

    private void autenticaUsuario() {
        CartaoService service = RetrofitConfig.getInstance()
                .create(CartaoService.class);

        String cpf = _perfil.getCPF();

        String deviceId = Global.getInstance().apiDeviceID;

        AuthRequest request = new AuthRequest();
        request.setCpf(cpf);
        request.setDeviceId(deviceId);

        Call<AuthResponse> callAutentica = service.autenticar(token, request);
        callAutentica.enqueue(new Callback<AuthResponse>() {
            @Override
            public void onResponse(Call<AuthResponse> call, Response<AuthResponse> response) {

                if (response.isSuccessful()) {
                    AuthResponse authResponse = response.body();
                    Global.getInstance().apiTokenPerfil = authResponse.getToken();
                }

                verificaTokenPerfil();
            }

            @Override
            public void onFailure(Call<AuthResponse> call, Throwable t) {

            }
        });
    }

    private void verificaTokenPerfil() {

        if (Global.getInstance().apiTokenPerfil == null
                || Global.getInstance().apiTokenPerfil.isEmpty()) {
            autenticaUsuario();
        }
    }

    private void loadDefaultParams() {
        Global.getInstance().apiURLHostCC = getResources().getString(R.string.sistemaURLHostCC);
        String baseURL = Global.getInstance().apiURLHostCC;
        Global.getInstance().apiURLAutenticacao = baseURL + getResources().getString(R.string.sistemaURLAutentica);
        Global.getInstance().apiURLRegister = baseURL + getResources().getString(R.string.sistemaURLRegister);
        Global.getInstance().apiURLLogin = baseURL + getResources().getString(R.string.sistemaURLLogin);
        Global.getInstance().apiURLSaldo = baseURL + getResources().getString(R.string.sistemaURLSaldo);
        Global.getInstance().apiURLSaldos = baseURL + getResources().getString(R.string.sistemaURLSaldos);
        Global.getInstance().apiURLHistoricos = baseURL + getResources().getString(R.string.sistemaURLHistoricos);
        Global.getInstance().apiURLFaturas = baseURL + getResources().getString(R.string.sistemaURLFaturas);
        Global.getInstance().apiURLIndicacao = baseURL + getResources().getString(R.string.sistemaURLIndicacao);
        Global.getInstance().apiURLBloquear = baseURL + getResources().getString(R.string.sistemaURLBloquear);
        Global.getInstance().apiURLDesbloquear = baseURL + getResources().getString(R.string.sistemaURLDesbloquear);
        Global.getInstance().apiURLguiaCompras = baseURL + getResources().getString(R.string.sistemaURLGuiaCompras);
        Global.getInstance().apiURLPushToken = baseURL + getResources().getString(R.string.sistemaURLPushToken);
    }

    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 100);
            return false;
        }
        return true;
    }

    private void disableAllAnimation(BottomNavigationViewEx bnve) {
        bnve.enableAnimation(false);
        bnve.enableShiftingMode(false);
        bnve.enableItemShiftingMode(false);
    }

    private void initCenterIconOnly() {
        disableAllAnimation(bnve);

        int centerPosition = 2;
        // attention: you must ensure the center menu item title is empty
        // make icon bigger at centerPosition
        bnve.setIconSizeAt(centerPosition, 48, 48);
        bnve.setItemBackground(centerPosition, R.color.colorPrimaryDark);
        bnve.setTextTintList(centerPosition, getResources().getColorStateList(R.color.white));
        bnve.setIconTintList(centerPosition,
                getResources().getColorStateList(R.color.white));
        bnve.setIconMarginTop(centerPosition, BottomNavigationViewEx.dp2px(this, 4));

    }

    private void initData() {
        fragments = new ArrayList<>(4);
        // add to fragments for adapter
        fragments.add(new HomeFragment());
        fragments.add(new EnderecoFragment());
        fragments.add(new HomeFragment());
        fragments.add(new IndicarFragment());
        fragments.add(new ContatoFragment());

        items = new SparseIntArray(4);
        items.put(R.id.nav_home, 0);
        items.put(R.id.nav_rede, 1);
        items.put(R.id.nav_incluir, 2);
        items.put(R.id.nav_indicar, 3);
        items.put(R.id.nav_contato, 4);

        // set adapter
        adapter = new VpAdapter(getSupportFragmentManager(), fragments);
        vp.setAdapter(adapter);

        // binding with ViewPager
        bnve.setupWithViewPager(vp);
    }

    private void initEvent() {
        bnve.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            private int previousPosition = -1;

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int position = items.get(item.getItemId());

                if (position == 0) {
                    mMenuItem.setVisible(true);
                } else {
                    mMenuItem.setVisible(false);
                }

                if (item.getItemId() == R.id.nav_incluir) {
                    Intent intent = new Intent(HomeActivity.this, incluirCartaoActivity.class);
                    startActivityForResult(intent, 200);

                } else if (previousPosition != position) {
                    // only set item when item changed
                    previousPosition = position;
                    Log.i(TAG, "-----bnve-------- previous item:" + bnve.getCurrentItem() + " current item:" + position + " ------------------");
                    bnve.setCurrentItem(position);
                }

                return true;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == 200) {
                bnve.setCurrentItem(0);
            }
        }
    }

    private static class VpAdapter extends FragmentPagerAdapter {
        private List<Fragment> data;

        public VpAdapter(FragmentManager fm, List<Fragment> data) {
            super(fm);
            this.data = data;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Fragment getItem(int position) {
            return data.get(position);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences preferences = getSharedPreferences("popup", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("popUpShowed", false);
        editor.apply();

        verificaTokenPerfil();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mMenuTop = null;
        getMenuInflater().inflate(R.menu.menu_refresh, menu);
        mMenuItem = menu.getItem(0);
        mMenuTop = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_refresh:

                HomeFragment fragment = (HomeFragment) adapter.getItem(vp.getCurrentItem());
                fragment.refreshSaldoCartaoByPerfil(0);

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void startLoadingSaldo() {
        if (isConsultingCard == false) {
            isConsultingCard = true;
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            iv = (ImageView) inflater.inflate(R.layout.iv_refresh, null);
            rotation = AnimationUtils.loadAnimation(HomeActivity.this, R.anim.rotate_refresh);
            rotation.setRepeatCount(Animation.INFINITE);
            iv.startAnimation(rotation);
            if (null != mMenuTop) {
                mMenuTop.getItem(0).setActionView(iv);
            }
        }
    }

    @Override
    public void stopLoadingSaldo() {
        if (null != iv) {
            isConsultingCard = false;
            iv.clearAnimation();
            if (null != mMenuTop) {
                mMenuTop.getItem(0).setActionView(iv);
            }
            iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iv.setImageResource(R.drawable.ic_autorenew_white_24dp);

                    HomeFragment fragment = (HomeFragment) adapter.getItem(vp.getCurrentItem());
                    fragment.refreshSaldoCartaoByPerfil(1);
                }
            });
        }
    }

}
