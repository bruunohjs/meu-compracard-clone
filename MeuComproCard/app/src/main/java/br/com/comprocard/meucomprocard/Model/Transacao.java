package br.com.comprocard.meucomprocard.Model;

import java.io.Serializable;

public class Transacao implements Serializable{
    private String info;

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
