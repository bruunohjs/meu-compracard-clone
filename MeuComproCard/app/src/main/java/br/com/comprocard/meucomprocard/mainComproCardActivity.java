package br.com.comprocard.meucomprocard;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import br.com.comprocard.meucomprocard.Adapters.CartaoAdapter;
import br.com.comprocard.meucomprocard.Controller.CartaoCtrl;
import br.com.comprocard.meucomprocard.Controller.PerfilCtrl;
import br.com.comprocard.meucomprocard.DataSync.ResultsListener;
import br.com.comprocard.meucomprocard.DataSync.ResultsListenerRefresh;
import br.com.comprocard.meucomprocard.DataSync.getImagemCartao;
import br.com.comprocard.meucomprocard.DataSync.postAutentica;
import br.com.comprocard.meucomprocard.DataSync.postCartaoSaldos;
import br.com.comprocard.meucomprocard.DataSync.postSendToken;
import br.com.comprocard.meucomprocard.DataSync.resServer;
import br.com.comprocard.meucomprocard.Model.Cartao;
import br.com.comprocard.meucomprocard.Model.Perfil;
import br.com.comprocard.meucomprocard.Model.TokenComproCard;
import br.com.comprocard.meucomprocard.Services.MeuComprocardFCMInstanceIDService;
import br.com.comprocard.meucomprocard.Util.Global;
import br.com.comprocard.meucomprocard.Util.ImagensCartaoHelper;
import br.com.comprocard.meucomprocard.Util.MCrypt;
import br.com.comprocard.meucomprocard.dbHelper.ConexaoSQLite;

import static android.Manifest.permission.INTERNET;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.widget.Toast.LENGTH_LONG;

public class mainComproCardActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, ResultsListener, ResultsListenerRefresh {

    ConexaoSQLite _banco;
    public ArrayList<Cartao> _cartoesArray;
    private CartaoAdapter _cartaoAdapter;
    private RecyclerView _cartaoRecyclerView;
    private CartaoCtrl _carCTRL;

    private PerfilCtrl _perCTRL;
    private Perfil _perfil;
    private boolean isConsultingCard;
    private ImageView iv;
    private Menu mMenuTop;
    private Animation rotation;
    private boolean atualizarImagem;

    private String[] permissions = new String[]{
            android.Manifest.permission.INTERNET,
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.ACCESS_NETWORK_STATE,
            android.Manifest.permission.ACCESS_COARSE_LOCATION,
            android.Manifest.permission.ACCESS_FINE_LOCATION
            //android.Manifest.permission.READ_PHONE_STATE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_compro_card);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        _banco = ConexaoSQLite.getInstancia(this);
        SQLiteDatabase _db = _banco.getWritableDatabase();
        _banco.onUpgrade(_db,1,2);
        isConsultingCard = false;

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), incluirCartaoActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        loadDefaultParams();
        checkPermissions();

        _perCTRL = new PerfilCtrl(_banco);
        _carCTRL = new CartaoCtrl(_banco);

        MeuComprocardFCMInstanceIDService _token = new MeuComprocardFCMInstanceIDService();

        _perfil = _perCTRL.getPerfilCtrl(1);
        Global.getInstance().apiDeviceID = _perfil.getIMEI();
        //Global.getInstance().apiTokenPerfil = _perfil.getToken();

        if (null != _perfil) {
            if (_perfil.getID() == 0) {
                Global.getInstance().isEditPerfil = false;
                Intent intent = new Intent(getApplicationContext(), perfilActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            } else {

                ArrayList<Cartao> _cartaoes = _carCTRL.getListaCartaoCtrl();
                if (_cartaoes.size() == 0) {
//                    Intent intent = new Intent(getApplicationContext(), cardAlertActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//                    startActivity(intent);
                } else {
                    cartaoCarregaLista();
                }
            }
        }
    }

    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 100);
            return false;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isConnected();
        cartaoCarregaLista();

        try {
            new Thread() {
                @Override
                public void run() {
                    SystemClock.sleep(950);
                    mainComproCardActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            refreshSaldoCartaoByPerfil(0);
                        }
                    });
                }
            }.start();
        } catch (Exception e){

        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void startLoadingSaldo() {
        if (isConsultingCard == false) {
            isConsultingCard = true;
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            iv = (ImageView) inflater.inflate(R.layout.iv_refresh, null);
            rotation = AnimationUtils.loadAnimation(mainComproCardActivity.this, R.anim.rotate_refresh);
            rotation.setRepeatCount(Animation.INFINITE);
            iv.startAnimation(rotation);
            mMenuTop.getItem(0).setActionView(iv);
        }
    }

    public void stopLoadingSaldo() {
        if (null != iv) {
            isConsultingCard = false;
            iv.clearAnimation();
            mMenuTop.getItem(0).setActionView(iv);
            iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iv.setImageResource(R.drawable.ic_autorenew_white_24dp);
                    refreshSaldoCartaoByPerfil(1);
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        mMenuTop = null;
        getMenuInflater().inflate(R.menu.menu_refresh, menu);
        mMenuTop = menu;

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            Global.getInstance().isEditPerfil = true;
            Intent intent = new Intent(getApplicationContext(), showPerfil.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
        } else if (id == R.id.nav_gallery) {
            Intent intent = new Intent(getApplicationContext(),  incluirCartaoActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
        } else if (id == R.id.nav_slideshow) {
            Intent intent = new Intent(getApplicationContext(),  indicacaoActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
        } else if (id == R.id.nav_rede) {
            Intent intent = new Intent(getApplicationContext(),  filtroActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
        } else if (id == R.id.nav_share) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(mainComproCardActivity.this);
            LayoutInflater inflater = mainComproCardActivity.this.getLayoutInflater();
            View alertView = inflater.inflate(R.layout.activity_ajuda, null);
            alertDialog.setView(alertView);
            final AlertDialog show = alertDialog.show();

            TextView alertButton = (TextView) alertView.findViewById(R.id.btnEnviar);
            alertButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    show.dismiss();
                }
            });
        } else if (id == R.id.nav_send) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(mainComproCardActivity.this);
            LayoutInflater inflater = mainComproCardActivity.this.getLayoutInflater();
            View alertView = inflater.inflate(R.layout.activity_sobre, null);
            alertDialog.setView(alertView);
            final AlertDialog show = alertDialog.show();

            TextView alertButton = (TextView) alertView.findViewById(R.id.btnFechar);
            alertButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    show.dismiss();
                }
            });
        } else if (id == R.id.nav_exit) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            LayoutInflater inflater = (LayoutInflater)mainComproCardActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View titleView = inflater.inflate(R.layout.custom_dialog, null);
            ((TextView) titleView.findViewById(R.id.partName)).setText(" Meu ComproCard");
            builder.setCustomTitle(titleView);
            builder.setMessage("Confirma sair do App?");
            builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    android.os.Process.killProcess(android.os.Process.myPid());
                }
            });
            builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void loadDefaultParams() {
        Global.getInstance().apiURLHostCC = getResources().getString(R.string.sistemaURLHostCC);
		String baseURL = Global.getInstance().apiURLHostCC;
        Global.getInstance().apiURLAutenticacao = baseURL + getResources().getString(R.string.sistemaURLAutentica);
        Global.getInstance().apiURLRegister = baseURL + getResources().getString(R.string.sistemaURLRegister);
        Global.getInstance().apiURLLogin = baseURL + getResources().getString(R.string.sistemaURLLogin);
        Global.getInstance().apiURLSaldo = baseURL + getResources().getString(R.string.sistemaURLSaldo);
        Global.getInstance().apiURLSaldos = baseURL + getResources().getString(R.string.sistemaURLSaldos);
        Global.getInstance().apiURLHistoricos = baseURL + getResources().getString(R.string.sistemaURLHistoricos);
        Global.getInstance().apiURLFaturas = baseURL + getResources().getString(R.string.sistemaURLFaturas);
        Global.getInstance().apiURLIndicacao = baseURL + getResources().getString(R.string.sistemaURLIndicacao);
        Global.getInstance().apiURLBloquear = baseURL + getResources().getString(R.string.sistemaURLBloquear);
        Global.getInstance().apiURLDesbloquear = baseURL + getResources().getString(R.string.sistemaURLDesbloquear);
        Global.getInstance().apiURLguiaCompras = baseURL + getResources().getString(R.string.sistemaURLGuiaCompras);
        Global.getInstance().apiURLPushToken = baseURL + getResources().getString(R.string.sistemaURLPushToken);
    }

    private void refreshSaldoCartaoByPerfil(int isManual){
        if (isConnected() == true) {
            startLoadingSaldo();
            if (_perfil.getID() != 1 ) {
                _perfil = _perCTRL.getPerfilCtrl(1);
                Global.getInstance().apiDeviceID = _perfil.getIMEI();
                //Global.getInstance().apiTokenPerfil = _perfil.getToken();
            }

            _cartoesArray = _carCTRL.getListaCartaoCtrl();
            if (_cartoesArray.size() > 0 ) {
                Global.getInstance().apiDeviceID = _perfil.getIMEI();
                //Global.getInstance().apiTokenPerfil = _perfil.getToken();
                postAutentica _taskAutentica = new postAutentica(Global.getInstance().apiURLAutenticacao, _perfil, mainComproCardActivity.this, false);
                _taskAutentica.setOnResultsListener(this);
                _taskAutentica.execute();
            } else {
                if (isManual == 1) {
                    Toast.makeText(mainComproCardActivity.this, "Favor inserir um cartão para consultar seu saldo!", Toast.LENGTH_LONG).show();
                }
                stopLoadingSaldo();
            }
        } else {
            cartaoCarregaLista();
        }
    }

    public void onResultsSucceeded(resServer result) {
        int httpStatus;
        JSONObject _json;
        String _mensagem = "";

        try {
            httpStatus = result.getHttpStatus();
            _json = result.getJsonBody();

            if (httpStatus == HttpURLConnection.HTTP_OK) {
                Global.getInstance().apiTokenPerfil = _json.getString("token");

                postCartaoSaldos _taskGetSaldos = new postCartaoSaldos(Global.getInstance().apiURLSaldos, mainComproCardActivity.this);
                _taskGetSaldos.setOnResultsListener(this);
                _taskGetSaldos.execute();
            } else {
                _mensagem = _json.getString("mensagem");
                if (_mensagem != "") {
                    Toast.makeText(mainComproCardActivity.this, _mensagem, Toast.LENGTH_LONG).show();
                    stopLoadingSaldo();
                } else {
                    Toast.makeText(mainComproCardActivity.this, "Tente novamente mais tarde", Toast.LENGTH_LONG).show();
                    stopLoadingSaldo();
                }
            }
        } catch (Exception e) {
            Toast.makeText(mainComproCardActivity.this, "Falha na conexão de rede.", LENGTH_LONG).show(); //+ e.getMessage()
            stopLoadingSaldo();
            return;
        }
    }

    public void onResultsSucceededRefresh(resServer result) {
        int httpStatus;
        JSONArray _jsonArray;
        JSONObject _json;
        JSONObject _jsonResp;
        String _mensagem = "";
        ImagensCartaoHelper _imgHelper = new ImagensCartaoHelper();

        try {
            httpStatus = result.getHttpStatus();
            _jsonArray = result.getJsonArray();
            _jsonResp  = result.getJsonBody();

            if (httpStatus == HttpURLConnection.HTTP_OK) {
                int n = _jsonArray.length();
                for(int i = 0; i < n; i++){
                    Cartao _cartaoServer = new Cartao();
                    _json = _jsonArray.getJSONObject(i);
                    _cartaoServer.setCartaoId(Integer.parseInt(_json.getString("CartaoId")));
                    _cartaoServer.setBandeira("ComproCard");
                    _cartaoServer.setProduto(_json.getString("ProdutoDesc"));
                    _cartaoServer.setEmpresa(_json.getString("EmpresaNome"));
                    _cartaoServer.setNome(_json.getString("CartaoNome"));
                    _cartaoServer.setNumero(getCartaoCript(_json.getString("CartaoNumero").toString()));
                    _cartaoServer.setCartaoNumFmt(_json.getString("CartaoNumFmt").toString());
                    _cartaoServer.setUltimaConsulta(_json.getString("dataUltimaConsulta"));
                    _cartaoServer.setValidade("31/05/2095");
                    _cartaoServer.setCVV("999");
                    _cartaoServer.setSaldo(new BigDecimal(_json.getString("CartaoSaldo")));
                    _cartaoServer.setLimite(new BigDecimal(_json.getString("CartaoLimite")));
                    _cartaoServer.setDiaVencimento(Integer.parseInt(_json.getString("CartaoVencto")));
                    _cartaoServer.setSituacao(_json.getString("SituacaoDescr"));
                    _cartaoServer.setSenha(_json.getString("exigeSenha"));
                    _cartaoServer.setToken(Global.getInstance().apiTokenCartao);
                    _cartaoServer.setUltimaConsulta(_json.getString("dataUltimaConsulta"));

                    CartaoCtrl _cartaoControler = new CartaoCtrl( _banco );
                    Cartao _cartao = _cartaoControler.getCartaoCtrlByCartaoId(Integer.parseInt(_json.getString("CartaoId")));
                    if  (null == _cartao.getVersaoImagemCartao() || _cartao.getVersaoImagemCartao().equals("")) {
                        try {
                            sendPushToken();
                        } catch (Exception e){

                        }
                        String URLbase  = _json.getString("ImagemCartao").substring(0, _json.getString("ImagemCartao").lastIndexOf("/") + 1 );
                        String fileName = _json.getString("ImagemCartao").substring(_json.getString("ImagemCartao").lastIndexOf("/") +1, _json.getString("ImagemCartao").length());
                        String versaoImagem  = getResources().getString(R.string.versaoImg);
                        _cartaoServer.setVersaoImagemCartao(versaoImagem);
                        URLbase += versaoImagem + "/" + fileName;

                        if (null != URLbase) {
                            if (!URLbase.equals("")) {
                                _cartaoServer.setImagemCartao(_imgHelper.getCaminhoImagenCartaoByURL(URLbase));
                                getImagemCartao _imgTask = new getImagemCartao(URLbase, mainComproCardActivity.this);
                                _imgTask.execute();
                            }
                        }
                    } else {
                        if (! Global.existeArquivo( _cartao.getImagemCartao()) ) {
                            try {
                                sendPushToken();
                            } catch (Exception e){

                            }

                            String URLbase  = _json.getString("ImagemCartao").substring(0, _json.getString("ImagemCartao").lastIndexOf("/") + 1 );
                            String fileName = _json.getString("ImagemCartao").substring(_json.getString("ImagemCartao").lastIndexOf("/") +1, _json.getString("ImagemCartao").length());
                            String versaoImagem  = getResources().getString(R.string.versaoImg);
                            _cartaoServer.setVersaoImagemCartao(versaoImagem);
                            URLbase += versaoImagem + "/" + fileName;

                            if (null != URLbase) {
                                if (!URLbase.equals("")) {
                                    _cartaoServer.setImagemCartao(_imgHelper.getCaminhoImagenCartaoByURL(URLbase));
                                    getImagemCartao _imgTask = new getImagemCartao(URLbase, mainComproCardActivity.this);
                                    _imgTask.execute();
                                }
                            }
                        }
                    }

                    /*try {
                        sendPushToken();
                    } catch (Exception e){

                    }*/


                    atualizarCartao(_cartaoServer, String.valueOf(_cartaoServer.getCartaoId()));
                }
                cartaoCarregaLista();
                stopLoadingSaldo();
            } else {
                _mensagem = _jsonResp.getString("mensagem");
                if (_mensagem != "") {
                    Toast.makeText(mainComproCardActivity.this, _mensagem, Toast.LENGTH_LONG).show();
                    stopLoadingSaldo();
                } else {
                    Toast.makeText(mainComproCardActivity.this, "Tente novamente mais tarde", Toast.LENGTH_LONG).show();
                    stopLoadingSaldo();
                }
            }
        } catch (Exception e) {
            Toast.makeText(mainComproCardActivity.this, "Falha na conexão de rede.", LENGTH_LONG).show(); //+ e.getMessage()
            stopLoadingSaldo();
            return;
        }
    }

    private void sendPushToken() {
        ConexaoSQLite _banco;
        _banco = ConexaoSQLite.getInstancia(this);

        Perfil _perfil;
        PerfilCtrl _perCTRL;
        _perCTRL = new PerfilCtrl(_banco);
        _perfil = _perCTRL.getPerfilCtrl(1);

        if (null != _perfil) {
            if (_perfil.getID() == 1) {
                Global.getInstance().apiDeviceID = _perfil.getIMEI();
                //Global.getInstance().apiTokenPerfil = _perfil.getToken();

                TokenComproCard _token = new TokenComproCard();
                _token.setCPF(_perfil.getCPF());
                _token.setIMEI(_perfil.getIMEI());
                _token.setToken(FirebaseInstanceId.getInstance().getToken());

                postSendToken _taskToken = new postSendToken(Global.getInstance().apiURLPushToken, _token, getApplicationContext(), false);
                _taskToken.execute();
            }
        }
    }


    public void atualizarCartao(Cartao _cartao, String cartaoID) {
        CartaoCtrl _cartaoCTRL = new CartaoCtrl(_banco);
        _cartaoCTRL.salvarCartaoCtrl(_cartao, cartaoID);
    }

    private void cartaoCarregaLista() {
        criarPopularprodutosArray();
        _cartaoAdapter = new CartaoAdapter(mainComproCardActivity.this, _cartoesArray);
        _cartaoRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_detalhecartao);
        //_cartaoRecyclerView.addItemDecoration(new DividerItemDecoration(meuCartaoActivity.this, LinearLayoutManager.VERTICAL, 0));
        _cartaoRecyclerView.setAdapter(_cartaoAdapter);
        _cartaoRecyclerView.setItemAnimator(new DefaultItemAnimator());
        _cartaoRecyclerView.setLayoutManager(new LinearLayoutManager(mainComproCardActivity.this));
    }

    private ArrayList<Cartao> criarPopularprodutosArray() {
        _cartoesArray = null;
        _cartoesArray = new ArrayList<Cartao>();
        _cartoesArray = _carCTRL.getListaCartaoCtrl();
        return _cartoesArray;
    }

    private String getCartaoCript(String _numeroCartao) {
        String encrypted = "";
        try {
            MCrypt mcrypt = new MCrypt();
            encrypted = mcrypt.bytesToHex(mcrypt.encrypt(_numeroCartao));
        } catch (Exception e) {
            Log.d("Erro", e.getMessage());
        }
        return encrypted;
    }

    public boolean isConnected()
    {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(meuCartaoActivity.CONNECTIVITY_SERVICE);
        NetworkInfo net = cm.getActiveNetworkInfo();
        if (net!=null && net.isAvailable() && net.isConnected()) {
            Global.getInstance().isConected = true;
            return true;
        } else {
            Global.getInstance().isConected = false;
            Toast.makeText(getApplicationContext(), "Por favor conecte-se a internet", Toast.LENGTH_LONG).show();
            return false;
        }
    }

}
