package br.com.comprocard.meucomprocard.DataSync;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by andre.cardoso on 07/12/2016.
 */

public class resServer {
    //private String origem;
    private int httpStatus;
    private JSONObject jsonBody;
    private JSONArray jsonArray;


    /*public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }*/

    public JSONArray getJsonArray() {
        return jsonArray;
    }

    public void setJsonArray(JSONArray jsonArray) {
        this.jsonArray = jsonArray;
    }

    public int getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(int httpStatus) {
        this.httpStatus = httpStatus;
    }

    public JSONObject getJsonBody() {
        return jsonBody;
    }

    public void setJsonBody(JSONObject jsonBody) {
        this.jsonBody = jsonBody;
    }
}
