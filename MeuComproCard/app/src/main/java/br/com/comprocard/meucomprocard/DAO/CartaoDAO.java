package br.com.comprocard.meucomprocard.DAO;

import java.math.BigDecimal;
import java.util.ArrayList;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.com.comprocard.meucomprocard.dbHelper.ConexaoSQLite;
import br.com.comprocard.meucomprocard.Model.Cartao;
import br.com.comprocard.meucomprocard.Util.Data;

public class CartaoDAO {
	private ConexaoSQLite sqliteDataBase;

	private final String[] COLUNAS_ARRAY = { "Id", "Bandeira", "Produto", "Empresa", "Nome", "Numero", "Validade", "CVV", "Saldo", "Limite", "DiaVencimento", "Situacao", "Senha", "Token", "UltimaConsulta", "CartaoId", "CartaoNumFmt", "ImagemCartao", "versaoImagemCartao" };
	private Data datas;

	public CartaoDAO(ConexaoSQLite pSqliteDataBase) {
		this.sqliteDataBase = pSqliteDataBase;
	}

	public long salvarCartaoDAO(Cartao cartao, String CartaoId) {
		long Id = 0;
		SQLiteDatabase db = this.sqliteDataBase.getWritableDatabase();
		try {
			ContentValues values = new ContentValues();
			values.put("Bandeira", cartao.getBandeira());
			values.put("Produto", cartao.getProduto());
			values.put("Empresa", cartao.getEmpresa());
			values.put("Nome", cartao.getNome());
			values.put("Numero", cartao.getNumero());
			values.put("Validade", cartao.getValidade());
			values.put("CVV", cartao.getCVV());
			values.put("Saldo", String.valueOf(cartao.getSaldo()));
			values.put("Limite", String.valueOf(cartao.getLimite()));
			values.put("DiaVencimento", cartao.getDiaVencimento());
			values.put("Situacao", cartao.getSituacao());
			values.put("Senha", cartao.getSenha());
			values.put("Token", cartao.getToken());
			values.put("UltimaConsulta", cartao.getUltimaConsulta());
			values.put("CartaoId", cartao.getCartaoId());
			values.put("CartaoNumFmt", cartao.getCartaoNumFmt());

			if (null != cartao.getImagemCartao()) {
				if (!cartao.getImagemCartao().equals("")) {
					values.put("ImagemCartao", cartao.getImagemCartao());
				}
			}

			if (null != cartao.getVersaoImagemCartao()) {
				if (!cartao.getVersaoImagemCartao().equals("")) {
					values.put("versaoImagemCartao", cartao.getVersaoImagemCartao());
				}
			}

			if (CartaoId != "" && CartaoId != "-1") {
				Id = db.update("Cartao", values, "CartaoId="+CartaoId, null );
			}
			else {
				Id = db.insert("Cartao", null, values);
			}
		} catch (Exception e) {
			Log.d("DB_ERROR", "NAO FOI POSSIVEL ATUALIZAR CARTAO");
		} finally {
			db.close();
		}
		return Id;
	}

	public Cartao getCartaoDAOByCartaoId(int pId) {
		Cartao cartao = new Cartao();
		SQLiteDatabase db = this.sqliteDataBase.getReadableDatabase();
		Cursor cursor = null;
		try {
			cursor = db.query("Cartao", COLUNAS_ARRAY, " CartaoId = ?", new String[] { String.valueOf(pId) }, null,null,null,null);
			if (cursor != null) {
				cursor.moveToFirst();
			}
			cartao.setID(Integer.parseInt(cursor.getString(0)));
			cartao.setBandeira(cursor.getString(1));
			cartao.setProduto(cursor.getString(2));
			cartao.setEmpresa(cursor.getString(3));
			cartao.setNome(cursor.getString(4));
			cartao.setNumero(cursor.getString(5));
			cartao.setValidade(cursor.getString(6));
			cartao.setCVV(cursor.getString(7));
			cartao.setSaldo(new BigDecimal(cursor.getString(8)));
			cartao.setLimite(new BigDecimal(cursor.getString(9)));
			cartao.setDiaVencimento(Integer.parseInt(cursor.getString(10)));
			cartao.setSituacao(cursor.getString(11));
			cartao.setSenha(cursor.getString(12));
			cartao.setToken(cursor.getString(13));
			cartao.setUltimaConsulta(cursor.getString(14));
			cartao.setCartaoId(Integer.parseInt(cursor.getString(15)));
			cartao.setCartaoNumFmt(cursor.getString(16));
			cartao.setImagemCartao(cursor.getString(17));
			cartao.setVersaoImagemCartao(cursor.getString(18));
		} catch (Exception e) {
			Log.d("DB_ERROR", "NAO FOI POSSIVEL RECUPERAR O CARTAO");
			cartao = null;
		} finally {
			cursor.close();
			db.close();
		}
		return cartao;
	}

	public ArrayList<Cartao> getListaCartaoDAO() {
		Cartao cartao = new Cartao();
		SQLiteDatabase db = null;
		Cursor cursor = null;
		ArrayList<Cartao> listaCartao = new ArrayList<Cartao>();
		//String query = "SELECT * FROM Cartao ORDER BY Nome ";
		try {
			db = this.sqliteDataBase.getReadableDatabase();
			cursor = db.query("Cartao", COLUNAS_ARRAY, null, null, null,null,null,null);
			if (cursor.moveToFirst()) {
			do {
				cartao = new Cartao();
				cartao.setID(Integer.parseInt(cursor.getString(0)));
				cartao.setBandeira(cursor.getString(1));
				cartao.setProduto(cursor.getString(2));
				cartao.setEmpresa(cursor.getString(3));
				cartao.setNome(cursor.getString(4));
				cartao.setNumero(cursor.getString(5));
				cartao.setValidade(cursor.getString(6));
				cartao.setCVV(cursor.getString(7));
				cartao.setSaldo(new BigDecimal(cursor.getString(8)));
				cartao.setLimite(new BigDecimal(cursor.getString(9)));
				cartao.setDiaVencimento(Integer.parseInt(cursor.getString(10)));
				cartao.setSituacao(cursor.getString(11));
				cartao.setSenha(cursor.getString(12));
				cartao.setToken(cursor.getString(13));
				cartao.setUltimaConsulta(cursor.getString(14));
				cartao.setCartaoId(Integer.parseInt(cursor.getString(15)));
				cartao.setCartaoNumFmt(cursor.getString(16));
				cartao.setImagemCartao(cursor.getString(17));
				cartao.setVersaoImagemCartao(cursor.getString(18));
				listaCartao.add(cartao);
			} while (cursor.moveToNext());
		}
		} catch (Exception e) {
			Log.d("DB_ERROR", "NAO FOI POSSIVEL LISTAR CARTAO");
		} finally {
			cursor.close();
			db.close();
		}
			return listaCartao;
	}

	public boolean excluirCartaoDAO(long pId) {
		SQLiteDatabase db = null;
		try {
		db = this.sqliteDataBase.getWritableDatabase();
		db.delete(
			"Cartao",
			"CartaoId = ?",
			new String[] { String.valueOf(pId) });
		} catch (Exception e) {
			Log.d("DB_ERROR", "NAO FOI POSSIVEL DELETAR CARTAO");
			return false;
		} finally {
			db.close();
		}
		return true;
	}
}