package br.com.comprocard.meucomprocard.DataSync;

/**
 * Created by Andre on 06/12/2016.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import br.com.comprocard.meucomprocard.Model.Indicacao;
import br.com.comprocard.meucomprocard.Util.Data;
import br.com.comprocard.meucomprocard.Util.GPSTracker;
import br.com.comprocard.meucomprocard.Util.Global;
import br.com.comprocard.meucomprocard.Util.MeuComproCardProgressDialog;

public class postAccountIndicacao extends AsyncTask<String, Void, resServer> {

    private String _localURL;
    private Indicacao _indicacaoPost;

    private Context mContext;
    private Data datas;
    protected ProgressDialog mProgressDialog;
    ResultsListener listener;

    public  postAccountIndicacao(String _url, Indicacao _indicacao, Context context) {
        this.mContext = context;
        this._localURL = _url;
        this._indicacaoPost = _indicacao;
        this.datas = new Data();
    }

    public void setOnResultsListener(ResultsListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mProgressDialog = MeuComproCardProgressDialog.ctor(mContext);
        mProgressDialog.show();
    }

    @Override
    protected void onPostExecute(resServer result) {
        if (mProgressDialog != null && mProgressDialog.isShowing() && mProgressDialog.getWindow() != null) {
            try {
                mProgressDialog.hide();
            } catch ( IllegalArgumentException ignore ) { ; }
        }
        listener.onResultsSucceeded(result);
        super.onPostExecute(result);
    }

    protected resServer doInBackground(final String... args) {
        Map<String, String> mapPerfil = new HashMap<String, String>();
        mapPerfil.put("cpf", String.valueOf(_indicacaoPost.getCPF()));
        mapPerfil.put("nome", String.valueOf(_indicacaoPost.getNome()));
        mapPerfil.put("telefone", String.valueOf(_indicacaoPost.getTelefone()));
        mapPerfil.put("nomeContato", String.valueOf(_indicacaoPost.getNomeContato()));

        if ( String.valueOf(_indicacaoPost.getEmailContato ()).trim().length() <= 0 ) {
            mapPerfil.put("emailContato", null);
        }
        else {
            mapPerfil.put("emailContato", String.valueOf(_indicacaoPost.getEmailContato ()));
        }

        mapPerfil.put("latitude", String.valueOf(_indicacaoPost.getLatitude()));
        mapPerfil.put("longitude", String.valueOf(_indicacaoPost.getLongitude()));
        String _jsonIndicacao = new GsonBuilder().create().toJson(mapPerfil, Map.class);
        resServer _res;
        try {
            _res = uploadToServer( Global.getInstance().apiURLIndicacao, _jsonIndicacao);
        } catch (IOException e) {
            e.printStackTrace();
            return new resServer();
        } catch (JSONException e) {
            e.printStackTrace();
            return new resServer();
        }
        return _res;
    }

    private resServer uploadToServer(String query, String json ) throws IOException, JSONException {
        JSONObject jsonObj = null;
        URL url = new URL(query);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        try {
            conn.setConnectTimeout(5000);
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");
            OutputStream os = conn.getOutputStream();
            os.write(json.getBytes("UTF-8"));
            os.close();

            InputStream in = new BufferedInputStream(conn.getInputStream());
            byte[] bytes = new byte[1000];

            StringBuilder _reader = new StringBuilder();
            int numRead = 0;
            while ((numRead = in.read(bytes)) >= 0) {
                _reader.append(new String(bytes, 0, numRead));
            }
            jsonObj = new JSONObject(_reader.toString());
            in.close();
            conn.disconnect();

        } catch (Exception e) {
            e.getMessage();
        } finally {
            resServer _resServer = new resServer();
            _resServer.setHttpStatus(conn.getResponseCode());

            InputStream is = null;
            if (_resServer.getHttpStatus() != 200) {
                if (_resServer.getHttpStatus() > 200 && _resServer.getHttpStatus() < 400) {
                    is = conn.getInputStream();
                } else {
                    is = conn.getErrorStream();
                }

                try {
                    jsonObj = new JSONObject(Global.getInstance().convertStreamToString(is));
                } catch (Exception e) {
                    jsonObj = null;
                }

                if (null == jsonObj) {
                    jsonObj = new JSONObject("{\n" +
                            "  \"codigo\": -1,\n" +
                            "  \"mensagem\": \"Erro de autenticação.\"\n" +
                            "}");
                }
            }
            _resServer.setJsonBody( jsonObj );
            return _resServer;
        }
    }

    private class Coordenadas{
        private String Latitude;
        private String Longitude;

        public String getLatitude() {
            return Latitude;
        }

        public void setLatitude(String latitude) {
            Latitude = latitude;
        }

        public String getLongitude() {
            return Longitude;
        }

        public void setLongitude(String longitude) {
            Longitude = longitude;
        }
    }


}
