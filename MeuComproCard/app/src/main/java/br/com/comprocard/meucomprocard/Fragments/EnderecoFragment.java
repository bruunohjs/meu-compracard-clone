package br.com.comprocard.meucomprocard.Fragments;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;

import br.com.comprocard.meucomprocard.DataSync.ResultsListener;
import br.com.comprocard.meucomprocard.DataSync.getGuiaCompras;
import br.com.comprocard.meucomprocard.DataSync.resServer;
import br.com.comprocard.meucomprocard.EstabelecimentosActivity;
import br.com.comprocard.meucomprocard.Model.guiaCompras;
import br.com.comprocard.meucomprocard.R;
import br.com.comprocard.meucomprocard.Util.GPSTracker;
import br.com.comprocard.meucomprocard.Util.Global;
import br.com.comprocard.meucomprocard.filtroActivity;

import static android.widget.Toast.LENGTH_LONG;

/**
 * A simple {@link Fragment} subclass.
 */
public class EnderecoFragment extends Fragment implements ResultsListener {

    private Button btnMyLocation,
            btnSelectLocation,
            btnAlmoco,
            btnCompras,
            btnCombustivel,
            btnFarmacia,
            btnLanches,
            btnMercado,
            btnTodos;
    ArrayList<guiaCompras> _resultadoPesquisa;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_endereco, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnMyLocation = view.findViewById(R.id.btn_my_location);
        btnMyLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isGPSAtivado()) {
                    double latitude = getActualLatitude();
                    double longitude = getActualLongitude();

                    getLojasByLocation(latitude, longitude);
                }
            }
        });
        btnLanches = view.findViewById(R.id.btn_lanches);
        btnLanches.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getLojas("-200");
            }
        });
        btnCompras = view.findViewById(R.id.btn_compras);
        btnCompras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getLojas("-100");
            }
        });
        btnAlmoco = view.findViewById(R.id.btn_almoco);
        btnAlmoco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getLojas("100");
            }
        });
        btnMercado = view.findViewById(R.id.btn_mercado);
        btnMercado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getLojas("200");
            }
        });
        btnCombustivel = view.findViewById(R.id.btn_compustivel);
        btnCombustivel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getLojas("300");
            }
        });
        btnFarmacia = view.findViewById(R.id.btn_farmacia);
        btnFarmacia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getLojas("400");
            }
        });
        btnTodos = view.findViewById(R.id.btn_todos);
        btnTodos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), filtroActivity.class);
                startActivity(intent);
            }
        });

        btnSelectLocation = view.findViewById(R.id.btn_select_location);
        btnSelectLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), filtroActivity.class);
                startActivity(intent);
            }
        });
    }

    private boolean isGPSAtivado() {
        if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET}, 10);
            }
            return false;
        } else {
            final LocationManager manager = (LocationManager) requireActivity().getSystemService(Context.LOCATION_SERVICE);
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(requireContext());
                LayoutInflater inflater = (LayoutInflater) requireActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View titleView = inflater.inflate(R.layout.custom_dialog, null);
                ((TextView) titleView.findViewById(R.id.partName)).setText(" Atenção");
                builder.setCustomTitle(titleView);
                builder.setMessage("O GPS não está ativo, é necessário ativar para continuar, ativar agora?");
                builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        int RESULT_GPS = 0;
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivityForResult(intent, RESULT_GPS);
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                android.support.v7.app.AlertDialog alert = builder.create();
                alert.show();
            } else {
                return true;
            }
        }
        return false;
    }

    public void getLojasByLocation(double latitude, double longitude) {
        String _estado = "";
        String _cidade = "";
        String _bairro = "";
        String _ramo = "";
        String _loja = "";

        String _JsonPesquisaFinal = "{ \"uf\": \"" + _estado + "\", \"cidade\" : \"" + _cidade + "\", \"bairro\":\"" + _bairro + "\", \"ramo\" : \"" + _ramo + "\", \"latitude\": \"" + latitude + "\", \"longitude\": \"" + longitude + "\" }";

        if (isConnected()) {
            getGuiaCompras _taskgetGuia = new getGuiaCompras(Global.getInstance().apiURLguiaCompras + "/estabelecimentos", _JsonPesquisaFinal, requireContext());
            _taskgetGuia.setOnResultsListener(this);
            _taskgetGuia.execute();
        }
    }

    public boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) requireActivity().getSystemService(filtroActivity.CONNECTIVITY_SERVICE);
        NetworkInfo net = cm.getActiveNetworkInfo();
        if (net != null && net.isAvailable() && net.isConnected()) {
            Global.getInstance().isConected = true;
            return true;
        } else {
            Global.getInstance().isConected = false;
            Toast.makeText(requireContext(), "Por favor conecte-se a internet", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    @Override
    public void onResultsSucceeded(resServer result) {
        _resultadoPesquisa = listaResultadoFinal(result);
        Global.getInstance().guiaComprasList = _resultadoPesquisa;
        Intent myIntent = new Intent(requireContext(), EstabelecimentosActivity.class);
        startActivityForResult(myIntent, 0);
    }

    private ArrayList<guiaCompras> listaResultadoFinal(resServer result) {
        int httpStatus;
        JSONArray _jsonResp;
        try {
            httpStatus = result.getHttpStatus();
            _jsonResp = result.getJsonArray();

            ArrayList<guiaCompras> _guiaCompras = new ArrayList<guiaCompras>();

            if (httpStatus == HttpURLConnection.HTTP_OK) {
                try {
                    JSONArray array = _jsonResp;
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject _jsonItem = array.getJSONObject(i);
                        guiaCompras _itemGuia = new guiaCompras();
                        _itemGuia.setEstabelecimento(_jsonItem.getString("Estabelecimento"));
                        _itemGuia.setAtividade(_jsonItem.getString("Atividade"));
                        _itemGuia.setBairro(_jsonItem.getString("Bairro"));
                        _itemGuia.setCidade(_jsonItem.getString("Cidade"));
                        _itemGuia.setUF(_jsonItem.getString("UF"));
                        _itemGuia.setTeletone(_jsonItem.getString("Telefone"));
                        _itemGuia.setGeo_Latitude(_jsonItem.getString("geo_Latitude"));
                        _itemGuia.setGeo_Longitude(_jsonItem.getString("geo_Longitude"));
                        _guiaCompras.add(_itemGuia);
                    }
                    _resultadoPesquisa = _guiaCompras;
                } catch (JSONException e) {
                    e.printStackTrace();
                    _resultadoPesquisa = null;
                }

            } else {
                Toast.makeText(requireActivity(), "Falha de conexão, tente novamente mais tarde.", LENGTH_LONG).show();
                System.gc();
                requireActivity().finish();
            }
        } catch (Exception e) {
            Toast.makeText(requireActivity(), "Falha na conexão de rede.", LENGTH_LONG).show(); //+ e.getMessage()
            System.gc();
            _resultadoPesquisa = null;
        }
        return _resultadoPesquisa;
    }

    public void getLojas(String _codigo) {

        if (isGPSAtivado()) {
            double latitude = getActualLatitude();
            double longitude = getActualLongitude();

            String _JsonPesquisaFinal = "{ \"codigo\": \""
                    + _codigo + "\", \"latitude\" : \""
                    + latitude + "\", \"longitude\":\""
                    + longitude + "\" }";

            if (isConnected()) {
                getGuiaCompras _taskgetGuia = new getGuiaCompras(Global.getInstance().apiURLguiaCompras + "/tipocartao", _JsonPesquisaFinal, requireActivity());
                _taskgetGuia.setOnResultsListener(EnderecoFragment.this);
                _taskgetGuia.execute();
            }
        }
    }

    public double getActualLatitude() {
        GPSTracker gps = new GPSTracker(requireContext());
        return gps.getLatitude();
    }

    public double getActualLongitude() {
        GPSTracker gps = new GPSTracker(requireContext());
        return gps.getLongitude();
    }
}
