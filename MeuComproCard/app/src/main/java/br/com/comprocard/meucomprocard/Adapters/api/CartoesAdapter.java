package br.com.comprocard.meucomprocard.Adapters.api;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.List;

import br.com.comprocard.meucomprocard.DataSync.postCartaoLoginWait;
import br.com.comprocard.meucomprocard.ExtratoActivity;
import br.com.comprocard.meucomprocard.HomeActivity;
import br.com.comprocard.meucomprocard.Model.Cartao;
import br.com.comprocard.meucomprocard.R;
import br.com.comprocard.meucomprocard.Util.Global;
import br.com.comprocard.meucomprocard.Util.MCrypt;
import br.com.comprocard.meucomprocard.Util.uiUtils;
import br.com.comprocard.meucomprocard.cartaoDetalhePopup;
import br.com.comprocard.meucomprocard.mainComproCardActivity;

public class CartoesAdapter extends PagerAdapter {

    List<Cartao> cartaoList;
    Context context;
    LayoutInflater layoutInflater;

    public CartoesAdapter(List<Cartao> cartaoList, Context context) {
        this.cartaoList = cartaoList;
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return cartaoList == null ? 0 : cartaoList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return false;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView( (View) object );
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        Cartao _cartao = cartaoList.get(position);

        View itemView = layoutInflater.inflate(R.layout.cartoes_list_row, container, false);

        TextView title;
        TextView txtPrimeiro;
        TextView txtSegundo;
        ImageButton btnDetalhes;
        ImageButton btnExtrato;

        TextView _CartaoNumero;
        TextView _Situacao;
        TextView _Saldo;

        TextView _Consulta;
        TextView _card_titular;
        ImageView _profile_pic;

        txtSegundo = (TextView) itemView.findViewById(R.id.label);
        _card_titular = (TextView) itemView.findViewById(R.id.card_titular);
        btnDetalhes = (ImageButton) itemView.findViewById(R.id.buttonDetalhe);
        btnExtrato = (ImageButton) itemView.findViewById(R.id.buttonExtrato);

        _CartaoNumero = (TextView) itemView.findViewById(R.id.lblNumeroCartao);
        _Situacao = (TextView) itemView.findViewById(R.id.lblSituacao);
        _Saldo = (TextView) itemView.findViewById(R.id.lblSaldo);
        //_Limite = (TextView) itemView.findViewById(R.id.lblLimite);
        _Consulta = (TextView) itemView.findViewById(R.id.lblDataConsulta);

        TextView _IdCartao = (TextView) itemView.findViewById(R.id.lblCartaoID);
        _profile_pic = (ImageView) itemView.findViewById(R.id.card_photo);

        _CartaoNumero.setText(uiUtils.replaceNull(String.valueOf(_cartao.getCartaoNumFmt())));
        _card_titular.setText(uiUtils.replaceNull(String.valueOf(_cartao.getNome())));
        _IdCartao.setText(String.valueOf(_cartao.getCartaoId()));
        _Situacao.setText("Situação: " + uiUtils.replaceNull(String.valueOf(_cartao.getSituacao())));
        DecimalFormat df = new DecimalFormat("#,###,##0.00");

        if (Global.getInstance().isConected == true) {
            _Saldo.setText("Saldo R$ " + df.format(_cartao.getSaldo()));
            _Saldo.setPaintFlags(0);
        } else {
            _Saldo.setText("Saldo R$ " + df.format(_cartao.getSaldo()));
            _Saldo.setPaintFlags(_Saldo.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
        _Consulta.setText( uiUtils.replaceNull(String.valueOf(_cartao.getUltimaConsulta())));
        //viewHolder._Limite.setText("Limite: R$ " + df.format(_cartao.getLimite()));
//
//        btnDetalhes.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (context instanceof mainComproCardActivity) {
//                    Intent intent = new Intent(((mainComproCardActivity) context), cartaoDetalhePopup.class);
//                    intent.setFlags(Intent.FLAG_FROM_BACKGROUND);
//                    intent.putExtra("ID_CARTAO", String.valueOf(_IdCartao.getText()));
//                    ((mainComproCardActivity) context).startActivity(intent);
//                }
//            }
//        });
//
//        _profile_pic.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (context instanceof mainComproCardActivity) {
//                    Intent intent = new Intent(((mainComproCardActivity) context), cartaoDetalhePopup.class);
//                    intent.setFlags(Intent.FLAG_FROM_BACKGROUND);
//                    intent.putExtra("ID_CARTAO", String.valueOf(_IdCartao.getText()));
//                    ((mainComproCardActivity) context).startActivity(intent);
//                }
//            }
//        });
//
//        btnExtrato.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (context instanceof mainComproCardActivity) {
//                    try {
//                        String _cartaoNumero = getCartaoDecript(String.valueOf(_cartao.getNumero()));
//                        synchronized (new postCartaoLoginWait(Global.getInstance().apiURLLogin, _cartaoNumero, ((mainComproCardActivity) mContext)).execute().get()) {
//                        }
//                    } catch (Exception e) {
//
//                    }
//
//                    Intent intent = new Intent(((mainComproCardActivity) context), ExtratoActivity.class);
//                    intent.setFlags(Intent.FLAG_FROM_BACKGROUND);
//                    intent.putExtra("ID_CARTAO", String.valueOf(_IdCartao.getText()));
//                    ((mainComproCardActivity) context).startActivity(intent);
//                }
//            }
//        });

        /*String URLbase  = _cartao.getImagemCartao().substring(0, _cartao.getImagemCartao().lastIndexOf("/") + 1 );
        String fileName = _cartao.getImagemCartao().substring(_cartao.getImagemCartao().lastIndexOf("/"), _cartao.getImagemCartao().length());
        String versaoImagem  = mContext.getResources().getString(R.string.versaoImg);
        URLbase += versaoImagem + fileName;*/

        try {
            try {
                _profile_pic.setImageResource ( R.drawable.ultimohome);
                //Picasso.with(this.context).load( "https://meucomprocardimg.blob.core.windows.net/cards/cartao_compras_side.png").placeholder(R.drawable.cartao_default).fit().into(_profile_pic);
                //Picasso.with(this.context).load("file://" + _cartao.getImagemCartao()).placeholder(R.drawable.cartao_default).fit().into(_profile_pic);
                //viewHolder._profile_pic.getBackground().setAlpha(50);
                //viewHolder._profile_pic.setAlpha(50);
                //Picasso.with(this.mContext).load("file://" + URLbase ).placeholder(R.drawable.cartao_default).fit().into(viewHolder._profile_pic);
            } catch (Exception e) {

            }
            //decodedBMP.recycle();
            //decodedBMP = null;
            System.gc();
        } catch (Exception e) {

        }

        return itemView;

    }


    private String getCartaoDecript(String _numeroCartaoCrip) {
        String result = "";
        try {
            MCrypt mcrypt = new MCrypt();
            result = new String(mcrypt.decrypt(_numeroCartaoCrip));
        } catch (Exception e) {
            Log.d("Erro", e.getMessage());
        }
        return result;
    }

}
