package br.com.comprocard.meucomprocard.Model.api;

import java.io.Serializable;

public class CartaoApi implements Serializable{
    private int CartaoId;
    private String CartaoNumero;
    private String CartaoNome;
    private String ClienteNome;
    private String ClienteCPF;
    private float CartaoSituacao;
    private String SituacaoDescr;
    private float CartaoLimite;
    private float CartaoSaldo;
    private int CartaoVencto;
    private float CartaoProduto;
    private String ProdutoDesc;
    private String CartaoNumFmt;
    private String PessoaEndereco;
    private String PessoaCompl;
    private String PessoaBairro;
    private String PessoaCidade;
    private String PessoaCEP;
    private String EmpresaNome;
    private String ImagemCartao;
    private String dataUltimaConsulta;
    private boolean exigeSenha;

    public int getCartaoId() {
        return CartaoId;
    }

    public void setCartaoId(int cartaoId) {
        CartaoId = cartaoId;
    }

    public String getCartaoNumero() {
        return CartaoNumero;
    }

    public void setCartaoNumero(String cartaoNumero) {
        CartaoNumero = cartaoNumero;
    }

    public String getCartaoNome() {
        return CartaoNome;
    }

    public void setCartaoNome(String cartaoNome) {
        CartaoNome = cartaoNome;
    }

    public String getClienteNome() {
        return ClienteNome;
    }

    public void setClienteNome(String clienteNome) {
        ClienteNome = clienteNome;
    }

    public String getClienteCPF() {
        return ClienteCPF;
    }

    public void setClienteCPF(String clienteCPF) {
        ClienteCPF = clienteCPF;
    }

    public float getCartaoSituacao() {
        return CartaoSituacao;
    }

    public void setCartaoSituacao(float cartaoSituacao) {
        CartaoSituacao = cartaoSituacao;
    }

    public String getSituacaoDescr() {
        return SituacaoDescr;
    }

    public void setSituacaoDescr(String situacaoDescr) {
        SituacaoDescr = situacaoDescr;
    }

    public float getCartaoLimite() {
        return CartaoLimite;
    }

    public void setCartaoLimite(float cartaoLimite) {
        CartaoLimite = cartaoLimite;
    }

    public float getCartaoSaldo() {
        return CartaoSaldo;
    }

    public void setCartaoSaldo(float cartaoSaldo) {
        CartaoSaldo = cartaoSaldo;
    }

    public int getCartaoVencto() {
        return CartaoVencto;
    }

    public void setCartaoVencto(int cartaoVencto) {
        CartaoVencto = cartaoVencto;
    }

    public float getCartaoProduto() {
        return CartaoProduto;
    }

    public void setCartaoProduto(float cartaoProduto) {
        CartaoProduto = cartaoProduto;
    }

    public String getProdutoDesc() {
        return ProdutoDesc;
    }

    public void setProdutoDesc(String produtoDesc) {
        ProdutoDesc = produtoDesc;
    }

    public String getCartaoNumFmt() {
        return CartaoNumFmt;
    }

    public void setCartaoNumFmt(String cartaoNumFmt) {
        CartaoNumFmt = cartaoNumFmt;
    }

    public String getPessoaEndereco() {
        return PessoaEndereco;
    }

    public void setPessoaEndereco(String pessoaEndereco) {
        PessoaEndereco = pessoaEndereco;
    }

    public String getPessoaCompl() {
        return PessoaCompl;
    }

    public void setPessoaCompl(String pessoaCompl) {
        PessoaCompl = pessoaCompl;
    }

    public String getPessoaBairro() {
        return PessoaBairro;
    }

    public void setPessoaBairro(String pessoaBairro) {
        PessoaBairro = pessoaBairro;
    }

    public String getPessoaCidade() {
        return PessoaCidade;
    }

    public void setPessoaCidade(String pessoaCidade) {
        PessoaCidade = pessoaCidade;
    }

    public String getPessoaCEP() {
        return PessoaCEP;
    }

    public void setPessoaCEP(String pessoaCEP) {
        PessoaCEP = pessoaCEP;
    }

    public String getEmpresaNome() {
        return EmpresaNome;
    }

    public void setEmpresaNome(String empresaNome) {
        EmpresaNome = empresaNome;
    }

    public String getImagemCartao() {
        return ImagemCartao;
    }

    public void setImagemCartao(String imagemCartao) {
        ImagemCartao = imagemCartao;
    }

    public String getDataUltimaConsulta() {
        return dataUltimaConsulta;
    }

    public void setDataUltimaConsulta(String dataUltimaConsulta) {
        this.dataUltimaConsulta = dataUltimaConsulta;
    }

    public boolean isExigeSenha() {
        return exigeSenha;
    }

    public void setExigeSenha(boolean exigeSenha) {
        this.exigeSenha = exigeSenha;
    }
}
