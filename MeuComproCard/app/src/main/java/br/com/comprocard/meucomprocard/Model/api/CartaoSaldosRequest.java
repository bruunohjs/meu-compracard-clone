package br.com.comprocard.meucomprocard.Model.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CartaoSaldosRequest implements Serializable {
    private List<CartaoSaldo> cartaoSaldos = new ArrayList<>();

    public List<CartaoSaldo> getCartaoSaldos() {
        return cartaoSaldos;
    }

    public void setCartaoSaldos(List<CartaoSaldo> cartaoSaldos) {
        this.cartaoSaldos = cartaoSaldos;
    }
}
