package br.com.comprocard.meucomprocard.Util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import br.com.comprocard.meucomprocard.Model.guiaCompras;
import br.com.comprocard.meucomprocard.R;

/**
 * Created by andre.cardoso on 01/12/2016.
 */

public class Global {
    private static Global mInstance = null;
    public Context mContext = null;

    public String tokenPerfil = "";
    public String tokenCartao = "";

    //Variaveis de funcionamento do sistema
    public Boolean isEditPerfil = false;
    public Boolean isConected   = true;

    //URLs
    public String apiURLHostCC = "";
    public String apiURLAutenticacao = "";
    public String apiURLRegister = "";
    public String apiURLLogin = "";
    public String apiURLSaldo = "";
    public String apiURLSaldos = "";
    public String apiURLHistoricos = "";
    public String apiURLFaturas = "";
    public String apiURLIndicacao = "";
    public String apiURLBloquear = "";
    public String apiURLDesbloquear = "";
    public String apiURLguiaCompras = "";
    public String apiURLPushToken = "";

    //Tokens
    public String apiTokenPerfil = "";
    public String apiTokenCartao = "";
    public String apiDeviceID = "";
    public int idSelected = 0;

    public int qtdExtratoTemp = 0;
    public ArrayList<guiaCompras> guiaComprasList;

    private static final String CAMINHO_EXPORT = Environment
            .getExternalStorageDirectory()
            + File.separator
            + "MeuComproCard"
            + File.separator
            + "Cards";

    public static synchronized Global getInstance() {
        if (null == mInstance) {
            mInstance = new Global();
        }
        return mInstance;
    }

    public static void Mensagem(Context paramContext, String paramMensagem) {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(paramContext);
        localBuilder.setTitle("ComproCard");
        localBuilder.setMessage(paramMensagem);
        localBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
            }
        });
        localBuilder.create().show();
    }

    public static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    public static float pxFromDp(float dp, Context mContext) {
        return dp * mContext.getResources().getDisplayMetrics().density;
    }

    public static boolean escreverArquivo(String pNomeDoArquivo, String pTexto) {
        FileOutputStream fos = null;

        // verifico se o cartão sd esta montado
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {

            try {
                final File dir = new File(CAMINHO_EXPORT);

                if (!dir.exists()) {
                    dir.mkdirs();
                }

                final File myFile = new File(dir, pNomeDoArquivo);

                if (!myFile.exists()) {
                    myFile.createNewFile();
                }

                fos = new FileOutputStream(myFile);

                fos.write(pTexto.getBytes());
                fos.close();
            } catch (IOException e) {
                Log.d("ERRO", e.getMessage());
                return false;
            }
        } else {
            return false;
        }

        return true;
    }

    public static boolean existeArquivo(String pNomeDoArquivo) {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            try {
                final File myFile = new File(pNomeDoArquivo);
                return myFile.exists();
            } catch (Exception e) {
                Log.d("ERRO", e.getMessage());
                return false;
            }
        } else {
            return false;
        }
    }

}
