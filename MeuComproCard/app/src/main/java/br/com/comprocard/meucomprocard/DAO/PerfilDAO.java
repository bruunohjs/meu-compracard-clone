package br.com.comprocard.meucomprocard.DAO;

import java.util.ArrayList;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import br.com.comprocard.meucomprocard.dbHelper.ConexaoSQLite;
import br.com.comprocard.meucomprocard.Model.Perfil;
import br.com.comprocard.meucomprocard.Util.Data;

public class PerfilDAO {
	private ConexaoSQLite sqliteDataBase;

	private final String[] COLUNAS_ARRAY = { "Id", "Nome", "Email", "CPF", "DataNascimento", "IMEI", "Senha", "Ativo", "Token", "Telefone", "DataHora" };
	private Data datas;

	public PerfilDAO(ConexaoSQLite pSqliteDataBase) {
		this.sqliteDataBase = pSqliteDataBase;
	}

	public long salvarPerfilDAO(Perfil perfil, Boolean atualizar) {
		long Id = 0;
		SQLiteDatabase db = this.sqliteDataBase.getWritableDatabase();
		try {
			ContentValues values = new ContentValues();
			values.put("Id", perfil.getID());
			values.put("Nome", perfil.getNome());
			values.put("Email", perfil.getEmail());
			values.put("CPF", perfil.getCPF());
			values.put("DataNascimento", perfil.getDataNascimento());
			values.put("IMEI", perfil.getIMEI());
			values.put("Senha", perfil.getSenha());
			values.put("Ativo", perfil.getAtivo());
			values.put("Token", perfil.getToken());
			values.put("Telefone", perfil.getTelefone());
			values.put("DataHora", perfil.getDataHora() );
			if (atualizar == true) {
				Id = db.update("Perfil", values, "Id = 1", null);
			} else {
				Id = db.insert("Perfil", null, values);
			}
		} catch (Exception e) {
			Log.d("DB_ERROR", "NAO FOI POSSIVEL INSERIR PERFIL");
		} finally {
			db.close();
		}
		return Id;
	}

	public Perfil getPerfilDAO(int pId) {
		Perfil perfil = new Perfil();
		SQLiteDatabase db = this.sqliteDataBase.getReadableDatabase();
		Cursor cursor = null;
		try {
			cursor = db.query("Perfil", COLUNAS_ARRAY, " Id = ?", new String[] { String.valueOf(pId) }, null,null,null,null);
			if (cursor != null) {
				cursor.moveToFirst();
			}
			perfil.setID(Integer.parseInt(cursor.getString(0)));
			perfil.setNome(cursor.getString(1));
			perfil.setEmail(cursor.getString(2));
			perfil.setCPF(cursor.getString(3));
			perfil.setDataNascimento(cursor.getString(4));
			perfil.setIMEI(cursor.getString(5));
			perfil.setSenha(cursor.getString(6));
			perfil.setAtivo(Integer.parseInt(cursor.getString(7)));
			perfil.setToken(cursor.getString(8));
			perfil.setTelefone(cursor.getString(9));
			perfil.setDataHora(cursor.getString(10));
		} catch (Exception e) {
			Log.d("DB_ERROR", "NAO FOI POSSIVEL RECUPERAR O PERFIL");
		} finally {
			cursor.close();
				db.close();
		}
		return perfil;
	}

	public ArrayList<Perfil> getListaPerfilDAO() {
		Perfil perfil = null;
		ArrayList<Perfil> listaPerfil = new ArrayList<Perfil>();
		SQLiteDatabase db = null;
		Cursor cursor = null;
		String query = "SELECT * FROM Perfil  ORDER BY Nome ";
		try {
			db = this.sqliteDataBase.getReadableDatabase();
			cursor = db.rawQuery(query, null);
			if (cursor.moveToFirst()) {
			do {
			perfil = new Perfil();
			perfil.setID(Integer.parseInt(cursor.getString(0)));
			perfil.setNome(cursor.getString(1));
			perfil.setEmail(cursor.getString(2));
			perfil.setCPF(cursor.getString(3));
			perfil.setDataNascimento(cursor.getString(4));
			perfil.setIMEI(cursor.getString(5));
			perfil.setSenha(cursor.getString(6));
			perfil.setAtivo(Integer.parseInt(cursor.getString(7)));
			perfil.setToken(cursor.getString(8));
			perfil.setTelefone(cursor.getString(9));
			perfil.setDataHora(cursor.getString(10));
			listaPerfil.add(perfil);
			} while (cursor.moveToNext());
		}
		} catch (Exception e) {
			Log.d("DB_ERROR", "NAO FOI POSSIVEL LISTAR PERFIL");
		} finally {
			cursor.close();
			db.close();
		}
			return listaPerfil;
	}

	public boolean excluirPerfilDAO(long pId) {
		SQLiteDatabase db = null;
		try {
		db = this.sqliteDataBase.getWritableDatabase();
		db.delete(
			"Perfil",
			"Id = ?",
			new String[] { String.valueOf(pId) });
		} catch (Exception e) {
			Log.d("DB_ERROR", "NAO FOI POSSIVEL DELETAR PERFIL");
			return false;
		} finally {
			db.close();
		}
		return true;
	}

	/*public void updateStatusPerfil( Perfil perfil ) {

		SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		String DataHoraAtual = localSimpleDateFormat.format(new Date());

		SQLiteDatabase db = this.sqliteDataBase.getWritableDatabase();
		try {
			String _sqlInsTabPro = "";
			String _idVend = String.valueOf( _config.getConfigVendedorIDDefault() );
			if (_idVend.length() <=0 ){
				_idVend = "0";
			}

			String _idTabela = String.valueOf( _config.getConfigTabelaIDDefault() );
			if (_idVend.length() <=0 ){
				_idTabela = "0";
			}

			_sqlInsTabPro += " update Configuracoes " +
					" set configDataCadastro =  " + uiUtils.setdoubleQuote( DataHoraAtual ) + ", " +
					"   configHostZuma = "+ uiUtils.setdoubleQuote( _config.getConfigHostZuma()  ) + ", " +
					"   configVendedorIDDefault = "+ String.valueOf( _idVend ) + ", " +
					"   configTabelaIDDefault = " + String.valueOf( _idTabela ) + ", " +
					"   configDeviceIMEI = " + uiUtils.setdoubleQuote( _config.getConfigDeviceIMEI() ) + ", "+
					"   configCPF = "+ uiUtils.setdoubleQuote( _config.getConfigCPF()  ) + ", " +
					"   configCNPJ = "+ uiUtils.setdoubleQuote( _config.getConfigCNPJ()  ) + " "+
					" where " +
					"  configID = " + String.valueOf( _config.getConfigID() );
			db.beginTransaction();
			SQLiteStatement stmt = db.compileStatement( _sqlInsTabPro );
			stmt.execute();
			stmt.clearBindings();
			db.setTransactionSuccessful();
			db.endTransaction();
			setSystemConfigDefault( _config );
		} catch (Exception e){
			db.endTransaction();
			Log.d("sql",e.getMessage());
		}
	}*/

}